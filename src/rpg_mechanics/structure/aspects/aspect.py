from typing import Any


class Aspect:

    def __init__(self, name: str):
        self.name = name

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        try:
            assert self.name == other.name
            return True
        except AssertionError:
            return False

    def __repr__(self):
        return '{self.__class__.__name__}(name="{self.name}")'.format(self=self)

    def __hash__(self):
        return hash(str(self))
