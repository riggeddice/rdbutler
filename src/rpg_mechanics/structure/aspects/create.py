import re
from typing import List

from src.rpg_mechanics.structure.aspects.aspect import Aspect


def create_aspects(comma_separated_aspect_list: str)->List[Aspect]:
    return create_aspects_from_list(comma_separated_aspect_list.split(","))


def create_aspects_from_list(aspect_list: List[str]) -> List[Aspect]:

    aspects = []
    if len(aspect_list) > 0:
        stripped_list = [s.strip() for s in aspect_list]
        for aspect_name in stripped_list:
            if aspect_name and re.search("\w+", aspect_name):
                aspects.append(Aspect(name=str(aspect_name.replace("\"", "").lower())))

    return aspects
