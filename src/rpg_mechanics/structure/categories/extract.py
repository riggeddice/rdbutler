from typing import List

from src.application.entities import config
from src.rpg_mechanics.invasion.extract_mechanics_structure import extract_mechanics_structure
from src.technical.extract_from_text import extract_headerized_section_body


def extract_category_section_body(potential_headers: List[str], actor_text: str) -> str:

    for header in potential_headers:
        result = extract_headerized_section_body(name=header, header_depth=3, finishing_header_scope=3,
                                                 source=actor_text, default=config.default_body())
        if result:
            return result

    return config.default_body()


def extract_category_tuples_from_section(section_text: str, actor_mechanics_version) -> List:
    return extract_mechanics_structure(section_text, [], actor_mechanics_version)
