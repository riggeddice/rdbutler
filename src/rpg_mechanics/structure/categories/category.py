from typing import List, Any

from src.rpg_mechanics.structure.aspects.aspect import Aspect


class Category:

    def __init__(self, name: str, aspects: List[Aspect]):
        self.name = name
        self.aspects = aspects

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        try:
            for field in ("name", "aspects"):
                assert getattr(self, field) == getattr(other, field)
            return True
        except AssertionError:
            return False

    def __repr__(self):
        return '{self.__class__.__name__}(name="{self.name}", aspects="{self.aspects}")'.format(self=self)

    def __hash__(self):
        return hash(str(self))
