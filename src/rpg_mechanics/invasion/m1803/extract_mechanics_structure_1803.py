import re
from typing import Tuple, List

from src.technical.extract_from_text import extract_headerized_section_body, extract_list_of_tuples_from_table


def extract_mechanics_structure_1803(source: str, default: List[Tuple[str,  List[str]]]) \
        -> List[Tuple[str,  List[str]]]:

    # Step 1: extract the relevant General section containing Categories and Aspects.
    body = extract_headerized_section_body(name="Kategorie i Aspekty",
                                           source=source,
                                           header_depth=4,
                                           finishing_header_scope=4,
                                           default="")
    table_values = extract_list_of_tuples_from_table(source=body,
                                                     default=[])

    # Step 2: First table tuple is headers
    headerless_tuples = table_values[1:]

    # Step 3: Delimited by ';' and having Too Many Spaces.
    cleaned_tuples = [_adapt_to_format(x) for x in headerless_tuples]
    result = [x for x in cleaned_tuples if x and x[0]]

    if not result:
        return default

    return result


def _adapt_to_format(record: Tuple[str, str]) -> Tuple[str, List[str]]:
    category_candidate = record[0].strip().lower()
    aspect_candidates = record[1].strip().lower().split(";")

    if re.search(r"\w+", category_candidate, re.IGNORECASE | re.DOTALL | re.MULTILINE):
        category = category_candidate
    else:
        category = ""

    aspects = []
    if category:
        for aspect_candidate in aspect_candidates:
            if re.search(r"\w+", aspect_candidate):
                aspects.append(aspect_candidate.strip())

    return category, aspects
