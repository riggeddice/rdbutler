import re
from typing import List, Tuple


def extract_mechanics_structure_default(source: str, default: List[Tuple[str,  List[str]]]) \
        -> List[Tuple[str,  List[str]]]:

    # Split the structure by the first bolded, dominant list:
    sections = re.compile(r"^\* ", re.MULTILINE).split(source)
    formatless_sections = [fs.replace("*", "").replace("_", "") for fs in sections if fs.strip()]

    mechanics_structures = []
    for section in formatless_sections:
        category_candidates = re.search(r"^(.+?)$", section, re.IGNORECASE | re.MULTILINE)
        category_string = category_candidates.group(0) if category_candidates else ""
        category = category_string.replace(":", "").strip()

        aspect_candidates = re.findall(r"\s*aspekty:(.+)", section, re.IGNORECASE)
        aspect_string = aspect_candidates[0] if aspect_candidates else ""
        dirty_aspects = aspect_string.split(",")
        aspects = [x.strip() for x in dirty_aspects if x and x.strip()]

        # VALIDATION: Motivation, Skills, Magic. TODO -> MOVE IT SOMEWHERE ELSE.
        if category and category not in("MET", "MRZ", "FIL", "BÓL", "KLT", "??", "?"):
            mechanics_structures.append((category, aspects))

    if not mechanics_structures:
        return default

    return mechanics_structures
