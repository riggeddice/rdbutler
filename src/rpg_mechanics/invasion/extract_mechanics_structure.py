from typing import Tuple, List

from src.rpg_mechanics.invasion.default.extract_mechanics_structure_default import extract_mechanics_structure_default
from src.rpg_mechanics.invasion.m1803.extract_mechanics_structure_1803 import extract_mechanics_structure_1803


def extract_mechanics_structure(source: str, default: List[Tuple[str,  List[str]]], actor_mechver: str) \
        -> List[Tuple[str,  List[str]]]:

    if int(actor_mechver) < 1803:
        return map_extract_structure["default"](source=source, default=default)
    else:
        if actor_mechver not in map_extract_structure:
            raise ValueError("Unknown actor mechanics version: " + actor_mechver)

        return map_extract_structure[actor_mechver](source=source, default=default)


map_extract_structure = {
    "1806": extract_mechanics_structure_1803,
    "1805": extract_mechanics_structure_1803,
    "1803": extract_mechanics_structure_1803,
    "default": extract_mechanics_structure_default
                         }
