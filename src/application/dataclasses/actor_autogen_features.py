import re
from typing import List, Dict, Tuple

from src.application.dataclasses.emotions import EmotionWheel
from src.technical.file_ops import EntityTextWithPath


# TODO: Change it into dataclass when Python 3.7 comes
class ActorAutogenFeature:

    def __init__(self, categorized_names: Dict[str, List[Tuple[str, int]]],
                 professions: List[Tuple[str, str]],
                 emotions: EmotionWheel,
                 traits: List[str]):
        self.categorized_names = categorized_names
        self.professions = professions
        self.emotions = emotions
        self.traits = traits


def create_actor_autogen_feature_with_defaults(*,
                                               categorized_names={},
                                               professions=[],
                                               emotions=EmotionWheel([], []),
                                               traits=[]
                                               ) -> ActorAutogenFeature:
    return ActorAutogenFeature(categorized_names=categorized_names, professions=professions, emotions=emotions,
                               traits=traits)


def create_actor_autogen_feature(entities: List[EntityTextWithPath], config) -> ActorAutogenFeature:
    categorized_names = extract_categorized_names(entities, config.file_matches_actor_names())
    professions = extract_list_of_codes_with_professions(entities, config.file_matches_professions(), 6)
    emotions = extract_list_of_emotions(entities, config.file_matches_emotions())
    traits = extract_list_of_traits(entities, config.file_matches_traits())

    return create_actor_autogen_feature_with_defaults(categorized_names=categorized_names,
                                                      professions=professions, emotions=emotions, traits=traits)


def extract_categorized_names(entities: List[EntityTextWithPath], matching_regex: str) -> Dict[str, List[Tuple[str, int]]]:
    names_text = [en for en in entities if re.search(matching_regex, en.path)][0].entity_text
    categories = re.findall(r"^## (.+?)$", names_text, re.MULTILINE | re.DOTALL)

    name_entity = {}
    for category in categories:
        # Because of string.format I have to apply double {{ }} at 1, 2 to escape {, }.
        relevant_names_section = re.search(r"^## {category}\s(.+?)(?:\Z|^#{{1,2}})".format(category=category),
                                           names_text, re.IGNORECASE | re.MULTILINE | re.DOTALL)[0]
        categorized_names = re.findall(r"^(\w+)\s+(\d+)$", relevant_names_section, re.MULTILINE | re.DOTALL)
        name_entity[category] = [(x[0], int(x[1])) for x in categorized_names]

    return name_entity


def extract_list_of_codes_with_professions(entities: List[EntityTextWithPath], matching_regex: str, depth: int):

    professions_entity = [en for en in entities if re.search(matching_regex, en.path)][0]

    regex = r"\b(?P<code>\d{%s})\b (?P<profession>.+)" % str(depth)
    professions = re.findall(regex, professions_entity.entity_text, re.MULTILINE | re.IGNORECASE)
    assert len(professions) > 0
    return professions


def extract_list_of_emotions(entities: List[EntityTextWithPath], matching_regex: str) -> EmotionWheel:
    emotion_entity = [en for en in entities if re.search(matching_regex, en.path)][0]

    base_emotion_text = re.search(r"## \bUczucia podstawowe\b(.+?)#{1,2}",
                                   emotion_entity.entity_text, re.IGNORECASE | re.MULTILINE | re.DOTALL).group(1)
    base_emotions = re.findall(r"^(\w+)\s+(\d+)\s+(\w+)\s+(\d+)\s+(\w+)\s+(\d+)\s+",
                                      base_emotion_text, re.MULTILINE | re.DOTALL)

    dyad_text = re.search(r"## \bStruktura\b(.+)(?:\Z|^#{1,2})",
                          emotion_entity.entity_text, re.IGNORECASE | re.MULTILINE | re.DOTALL).group(1)
    dyads = re.findall(r"^(\w+)\s+(\w+)\s+(\w+)\s+(\d+)\s*",
                       dyad_text, re.MULTILINE | re.DOTALL)

    return EmotionWheel(base_emotions=base_emotions, dyads=dyads)


def extract_list_of_traits(entities: List[EntityTextWithPath], matching_regex: str) -> List[str]:
    trait_entity_text = [en for en in entities if re.search(matching_regex, en.path)][0].entity_text
    base_trait_text = re.search(r"#.+?$.+?(^.+)$",
                                trait_entity_text, re.IGNORECASE | re.MULTILINE | re.DOTALL).group(1)
    traits = [x for x in base_trait_text.split("\n") if x]
    return traits
