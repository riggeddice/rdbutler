from typing import Any


class Merit:

    def __init__(self, originating_story: str, actor: str, qualifier: str, actual_merit: str):
        self.originating_story = originating_story
        self.actor = actor
        self.qualifier = qualifier
        self.actual_merit = actual_merit

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        return self.originating_story == other.originating_story and self.actor == other.actor and \
               self.qualifier == other.qualifier and self.actual_merit == other.actual_merit

    def __repr__(self):
        return '{self.__class__.__name__}(originating_story="{self.originating_story}", actor="{self.actor}", ' \
               'qualifier="{self.qualifier}", actual_merit="{self.actual_merit}")'.format(self=self)
