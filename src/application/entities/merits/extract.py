import re

from src.application.entities import config
from src.technical.extract_from_text import extract_headerized_section_body


def extract_merit_section_body(story_text: str) -> str:

    return extract_headerized_section_body(name="Zasługi", header_depth=1, finishing_header_scope=1,
                                           source=story_text, default=config.default_body())


def extract_merit_record_tuples(merit_section_body: str) -> list:

    # Seek the initial structure of '- ' or '* '. Then capture qualifier 'mag' or '???'.
    # Then capture the name until (, or 'jako') and in the end capture it all as a deed body.
    extracting_regex = r"^[-\*]\s(...):\s(.+?)(?:,|\s*jako)\s*(.+?)$"
    merits = re.findall(extracting_regex, merit_section_body, re.DOTALL | re.MULTILINE | re.IGNORECASE)

    if not merits:
        return []

    return merits
