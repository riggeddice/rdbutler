from src.application.entities.merits.extract import extract_merit_section_body
from src.application.entities.merits.extract import extract_merit_record_tuples
from src.application.entities.stories.story import Story
from .merit import Merit
from typing import List


def create_merits(entities: object) -> List[Merit]:

    if isinstance(entities, list):
        result = create_merits_from_stories(entities)
    elif isinstance(entities, Story):
        result = create_merits_from_story(entities)
    else:
        raise ValueError("Expected either list or Story, got: " + str(type(entities)))

    return result


def create_merits_from_stories(stories: List[Story]) -> List[Merit]:

    merits = []
    for story in stories:
        merits.extend(create_merits_from_story(story))

    return merits


def create_merits_from_story(story: Story) -> List[Merit]:

    merits_section = extract_merit_section_body(story.body)
    merit_records = extract_merit_record_tuples(merits_section)

    merits = []
    for record in merit_records:
        merits.append(create_merit(story.unique_id, record))

    return merits


def create_merit_with_default_params(*, story_uid="101214-default", actor="default-actor", qualifier="???",
                                     actual_merit="single-line text"):
    return Merit(originating_story=story_uid, actor=actor, qualifier=qualifier, actual_merit=actual_merit)


def create_merit(story_uid: str, merit_tuple: tuple) -> Merit:
    return Merit(story_uid, merit_tuple[1], merit_tuple[0], merit_tuple[2])
