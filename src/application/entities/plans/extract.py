from typing import List, Tuple

from src.application.entities import config
from src.technical.extract_from_text import extract_headerized_section_body, extract_list_of_unnamed_key_values


def extract_actor_plan_section_body(story_text: str) -> str:
    return extract_headerized_section_body(name="Plany", header_depth=1, finishing_header_scope=3,
                                           source=story_text, default=config.default_body())


def extract_faction_plan_section_body(story_text: str) -> str:

    plan_section = extract_headerized_section_body(name="Plany", header_depth=1, finishing_header_scope=1,
                                                       source=story_text, default=config.default_body())
    faction_section = extract_headerized_section_body(name="Frakcji", header_depth=2, finishing_header_scope=3,
                                                      source=plan_section, default=config.default_body())

    return faction_section


def extract_plan_record_tuples(progression_section_body: str) -> List[Tuple[str, str]]:
    return extract_list_of_unnamed_key_values(source=progression_section_body, default=[])
