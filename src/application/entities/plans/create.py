from src.application.entities.plans.extract import extract_actor_plan_section_body, extract_plan_record_tuples, \
    extract_faction_plan_section_body
from src.application.entities.stories.story import Story
from .plan import Plan
from typing import List


def create_plans_from_story(story: Story) -> List[Plan]:

    plan_section = extract_actor_plan_section_body(story.body)
    actor_plan_records = extract_plan_record_tuples(plan_section)
    faction_plan_section = extract_faction_plan_section_body(story.body)
    faction_plan_records = extract_plan_record_tuples(faction_plan_section)

    plans = []
    for record in actor_plan_records:
        plans.append(create_plan(story.unique_id, record))

    for record in faction_plan_records:
        plans.append(create_plan(story.unique_id, record, True))

    return plans


def create_plan_with_default_params(*, story_uid="101214-default", principal="default-actor",
                                    actual_plan="single-line text", is_faction=False):
    return Plan(originating_story=story_uid, principal=principal, actual_plan=actual_plan, is_faction=is_faction)


def create_plan(story_uid: str, plan_tuple: tuple, is_faction=False) -> Plan:
    return Plan(story_uid, plan_tuple[0], plan_tuple[1], is_faction)
