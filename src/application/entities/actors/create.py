from typing import List

from .actor import Actor
from .extract import extract_name_factions_type_and_text_body_from_text
from .extract import extract_mechanics_description_owner_from_text
from src.application.entities import config


def create_actor(path: str, actor_text: str) -> Actor:

    uid_prototype = path.split('.', 1)[0]
    mechanics_version = uid_prototype[0:4]
    uid = uid_prototype
    name, factions, actor_type, body = extract_name_factions_type_and_text_body_from_text(actor_text)
    mechanics, description, owner = extract_mechanics_description_owner_from_text(actor_text)

    actor = Actor(uid=uid, mechanics_version=mechanics_version, name=name, actor_type=actor_type,
                  factions=factions, mechanics=mechanics, description=description, body=body, owner=owner)
    return actor


def create_actor_with_defaults(*, uid=config.default_actor_uid(), mechanics_version=config.default_actor_mech_version(),
                               name=config.default_actor_name(), factions=config.default_actor_factions(),
                               actor_type=config.default_actor_type(), body=config.default_body(),
                               mechanics=config.default_actor_mechanics(),
                               description=config.default_actor_description(), owner=config.default_owner()) -> Actor:

    actor = Actor(uid=uid, mechanics_version=mechanics_version, name=name, actor_type=actor_type,
                  factions=factions, mechanics=mechanics, description=description, body=body, owner=owner)
    return actor


def create_actors(actors_with_paths: list) -> List[Actor]:

    actors = []
    for awp in actors_with_paths:
        path = awp.path
        actor_text = awp.entity_text

        actors.append(create_actor(path, actor_text))

    return actors
