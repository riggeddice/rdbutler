from typing import List, Tuple, Union, Any


class Actor:

    def __init__(self, uid: str, mechanics_version: str, name: str, factions: List[str], actor_type: str,
                 mechanics: str, description: str, body: str, owner: str):
        self.uid = uid
        self.mechanics_version = mechanics_version
        self.name = name
        self.body = body
        self.factions = factions
        self.mechanics = mechanics
        self.description = description
        self.actor_type = actor_type
        self.owner = owner

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        try:
            for field in ("uid", "mechanics_version", "name", "body", "factions", "mechanics", "description",
                          "actor_type", "owner"):
                assert getattr(self, field) == getattr(other, field)
            return True
        except AssertionError:
            return False

    def __repr__(self):
        return '{self.__class__.__name__}(uid="{self.uid}", mechanics_version="{self.mechanics_version}", ' \
               'name="{self.name}", body="{self.body}", factions={self.factions}, mechanics="{self.mechanics}", ' \
               'description="{self.description}", actor_type="{self.actor_type}", ' \
               'owner="{self.owner}")'.format(self=self)
