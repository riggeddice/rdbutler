from typing import List

from src.application.entities import config
from src.technical.extract_from_text import extract_body__no_front_matter_no_autogen__from_text, \
    extract_single_param_from_front_matter, extract_headerized_section_body, extract_param_list_from_front_matter


def extract_name_factions_type_and_text_body_from_text(actor_text: str) -> tuple:

    name = extract_actor_name_from_text(actor_text)
    factions = extract_factions_from_text(actor_text)
    actor_type = extract_type_from_text(actor_text)
    body = extract_body__no_front_matter_no_autogen__from_text(actor_text, "Historia")

    return name, factions, actor_type, body


def extract_mechanics_description_owner_from_text(actor_text: str) -> tuple:

    mechanics = extract_mechanics_section_body(actor_text)
    description = extract_description_section_body(actor_text)
    owner = extract_owner_from_text(actor_text)

    return mechanics, description, owner


def extract_description_section_body(actor_text: str) -> str:
    return extract_headerized_section_body("Opis", 1, 1, actor_text, config.default_actor_description())


def extract_actor_name_from_text(actor_text: str) -> str:
    return extract_single_param_from_front_matter(name="title", source=actor_text, default=config.default_actor_name())


def extract_owner_from_text(actor_text: str) -> str:
    return extract_single_param_from_front_matter(name="owner", source=actor_text, default=config.default_owner())


def extract_type_from_text(actor_text: str) -> str:
    return extract_single_param_from_front_matter(name="type", source=actor_text, default=config.default_actor_type())


def extract_mechanics_section_body(actor_text: str) -> str:

    mechanics_section = extract_headerized_section_body("Postać", 2, 1, actor_text, "")

    if not mechanics_section:
        mechanics_section = extract_headerized_section_body("Mechanika", 1, 1,
                                                            actor_text, config.default_actor_mechanics())

    return mechanics_section


def extract_factions_from_text(actor_text: str) -> List[str]:
    return extract_param_list_from_front_matter(name="factions", source=actor_text, list_separator=",",
                                                default=config.default_actor_factions())
