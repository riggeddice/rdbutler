from typing import List
from src.application.entities.stories.time_record import TimeRecord


# Story, Campaign


def default_location_in_story() -> str:
    """This Location is applied to every Story if the proper record is not present in the Story"""
    return "1. Świat"


def default_summary_in_story() -> str:
    """This Summary is applied to every Story if the proper record is not present in the Story"""
    return ""


def default_gms_in_story() -> List[str]:
    """This list of gamemasters is applied to every Story if the proper record is not present in the Story"""
    return ["Nieznane"]


def default_players_in_story() -> List[str]:
    """This list of players is applied to every Story if the proper record is not present in the Story"""
    return ["Nieznane"]


def default_story_title_in_story() -> str:
    """This Story name is applied to every Story if the proper record is not present in the Story"""
    return "Bez nazwy"


def default_story_duration()-> int:
    """Default duration if not specified is 1"""
    return 1


def default_story_delay()-> int:
    """Default delay if not specified is 0"""
    return 0


def default_story_time_record()-> TimeRecord:
    return TimeRecord(duration=default_story_duration(), delay=default_story_delay(), is_default=True)


def default_campaign_title() -> str:
    """This Campaign name is applied if the proper record is not present"""
    return "Nie przydzielone"


def default_campaign_uid() -> str:
    """This Campaign UID is applied to a campaign if proper record is not present. Unlikely, but..."""
    return "nie-przydzielone"


def default_campaign_description() -> str:
    """This Campaign description is applied if description not present at all"""
    return ""

# Actor


def default_actor_type() -> str:
    """This type is applied to every actor that has no type specified. Type is usually PC or NPC."""
    return "???"


def default_actor_factions() -> List[str]:
    """This Faction is applied to every actor if there is no faction present in the Actor"""
    return ["Nieznany"]


def default_owner() -> str:
    """This Owner is applied to every actor if there is no Owner present in the Actor"""
    return "Publiczny"


def default_actor_name() -> str:
    """This uid is applied to any actor with no uid specified"""
    return "Nieznany Aktor"


def default_faction_name() -> str:
    """This uid is applied to any faction with no uid specified"""
    return "Nieznana Frakcja"


def default_faction_uid() -> str:
    """This uid is applied to any faction with no uid specified"""
    return "nieznana-frakcja"


def default_actor_mech_version() -> str:
    return "9999"


def default_actor_uid() -> str:
    return default_actor_mech_version() + "-" + default_actor_name()


def default_actor_description() -> str:
    return ""


def default_actor_mechanics() -> str:
    return ""


# Common


def default_body()->str:
    """This body is a default body for every entity out there, unless specified otherwise"""
    return ""

