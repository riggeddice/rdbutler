from src.application.entities import config
from src.technical.extract_from_text import extract_body__no_front_matter_no_autogen__from_text, \
    extract_single_param_from_front_matter


def extract_name_and_text_body_from_text(faction_text: str) -> tuple:

    name = extract_faction_name_from_text(faction_text)
    body = extract_body__no_front_matter_no_autogen__from_text(faction_text, "Historia")

    return name, body


def extract_faction_name_from_text(faction_text: str) -> str:
    return extract_single_param_from_front_matter(name="title", source=faction_text,
                                                  default=config.default_faction_name())
