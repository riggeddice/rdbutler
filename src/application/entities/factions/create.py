from typing import List

from src.application.entities.factions.extract import extract_name_and_text_body_from_text
from .faction import Faction
from src.application.entities import config


def create_faction(path: str, actor_text: str) -> Faction:

    uid_prototype = path.split('.', 1)[0]
    name, body = extract_name_and_text_body_from_text(actor_text)

    faction = _create_faction_naked(uid=uid_prototype, name=name, body=body)
    return faction


def create_faction_with_defaults(*, uid=config.default_faction_uid(),
                                 name=config.default_faction_name(),
                                 body=config.default_body()) -> Faction:

    faction = _create_faction_naked(uid=uid, name=name, body=body)
    return faction


def create_factions(factions_with_paths: list) -> List[Faction]:

    factions = []
    for fwp in factions_with_paths:
        path = fwp.path
        faction_text = fwp.entity_text

        factions.append(create_faction(path, faction_text))

    return factions


def _create_faction_naked(*, uid=config.default_faction_uid(),
                          name=config.default_faction_name(),
                          body=config.default_body()) -> Faction:

    uid_1 = uid.replace("frakcja-", "") if uid else config.default_faction_uid()

    faction = Faction(uid=uid_1, name=name, body=body)
    return faction
