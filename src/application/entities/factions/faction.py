from typing import Any


class Faction:

    def __init__(self, uid: str, name: str, body: str):
        self.uid = uid
        self.name = name
        self.body = body

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        try:
            for field in ("uid", "name", "body"):
                assert getattr(self, field) == getattr(other, field)
            return True
        except AssertionError:
            return False

    def __repr__(self):
        return '{self.__class__.__name__}(uid="{self.uid}", name="{self.name}", body="{self.body}")'.format(self=self)
