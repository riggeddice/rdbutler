from typing import Any


class Campaign:
    def __init__(self, uid: str, title: str, prev_campaign_uid: str, body: str, description:str):
        self.uid = uid
        self.title = title
        self.prev_campaign_uid = prev_campaign_uid
        self.body = body
        self.description = description

        """Populated data is something added to Campaign after building it - for example, 'campaign number'.
                It is a PUBLICLY ACCESSIBLE dict, mutable one and it is NOT checked in all_fields."""
        self.populated_data = {}

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        return self.uid == other.uid and self.title == other.title and \
               self.prev_campaign_uid == other.prev_campaign_uid and self.body == other.body and \
               self.description == other.description

    def __repr__(self):
        return '{self.__class__.__name__}(uid="{self.uid}", title="{self.title}", ' \
               'prev_campaign_uid="{self.prev_campaign_uid}", body="{self.body}", ' \
               'description="{self.description}")'.format(self=self)


KEY_campaign_position_in_index = "campaign_position_in_index"
KEY_campaign_distance_from_root = "campaign_distance_from_root"
