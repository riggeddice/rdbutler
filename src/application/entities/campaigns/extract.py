from typing import Tuple

from src.application.entities import config
from src.technical.extract_from_text import extract_body__no_front_matter_no_autogen__from_text, \
    extract_single_param_from_front_matter, extract_headerized_section_body, extract_list_of_links


def extract_all_campaign_params_from_text(entity_text: str, campaign_uid: str) -> Tuple[str, str, str, str]:

    title = extract_title_from_text(entity_text)
    body = extract_body__no_front_matter_no_autogen__from_text(entity_text, "Historia")
    previous_campaign_uid = extract_previous_campaign_uid_from_text(entity_text, campaign_uid)
    description = extract_description_section_body(entity_text)

    return title, body, previous_campaign_uid, description


def extract_title_from_text(story_text: str) -> str:
    return extract_single_param_from_front_matter(name="title", source=story_text, default=config.default_campaign_title())


def extract_description_section_body(campaign_text: str) -> str:

    return extract_headerized_section_body(name="Opis", header_depth=2, finishing_header_scope=1,
                                           source=campaign_text, default=config.default_campaign_description())


def extract_previous_campaign_uid_from_text(entity_text: str, campaign_uid: str) -> str:

    section = extract_headerized_section_body(name="Kontynuacja", header_depth=2, finishing_header_scope=2,
                                           source=entity_text, default="")

    prev_campaign_uid = campaign_uid

    if section:
        links = extract_list_of_links(source=section, default=[("Irrelevant", campaign_uid)])
        link_target = links[0][1]
        prev_campaign_uid = link_target.split(".")[0]

    return prev_campaign_uid
