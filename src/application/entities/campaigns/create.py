from typing import List

from src.application.entities.campaigns.extract import extract_all_campaign_params_from_text
from src.application.entities.campaigns.campaign import Campaign
from src.application.entities import config


def create_campaigns(entities_with_paths: list) -> List[Campaign]:

    campaigns = []
    for ewp in entities_with_paths:
        path = ewp.path
        entity_text = ewp.entity_text

        campaigns.append(create_campaign(path, entity_text))

    return campaigns


def create_campaign_with_default_params(*, uid=config.default_campaign_uid(), title=config.default_campaign_title(),
                                        prev_campaign_uid=None, body=config.default_body(),
                                        description= config.default_campaign_description()) -> Campaign:

    # To make sure prev campaign UID is self UID if not present
    prev_campaign_uid2 = prev_campaign_uid if prev_campaign_uid else uid

    campaign = _create_campaign_naked(uid=uid,
                                      title=title,
                                      prev_campaign_uid=prev_campaign_uid2,
                                      body=body,
                                      description=description)
    return campaign


def create_campaign(path: str, entity_text: str) -> Campaign:

    uid = path.split('.', 1)[0]
    title, body, previous_campaign_uid, description = extract_all_campaign_params_from_text(entity_text, uid)

    campaign = _create_campaign_naked(uid=uid,
                                      title=title,
                                      prev_campaign_uid=previous_campaign_uid,
                                      body=body,
                                      description=description)
    return campaign


def _create_campaign_naked(*, uid: str, title: str, prev_campaign_uid: str, body: str, description:str):
    actual_uid = uid.replace("kampania-", "")
    actual_prev_camp = prev_campaign_uid.replace("kampania-", "")

    return Campaign(uid=actual_uid,
                    title=title,
                    prev_campaign_uid=actual_prev_camp,
                    body=body,
                    description=description)

