import re
from typing import List, Tuple, Dict

from src.application.entities import config
from src.technical.extract_from_text import extract_headerized_section_body, \
    extract_body__no_front_matter_no_autogen__from_text, extract_single_param_from_front_matter, \
    extract_param_list_from_front_matter, extract_list_of_links, extract_dict_of_potentially_emphasised_keys
from .time_record import TimeRecord


def extract_all_story_params_from_text(story_text: str, story_uid: str) -> \
        Tuple[str, str, List[str], str, List[str], List[str], str, str, str, TimeRecord]:

    try:
        title = extract_story_title_from_text(story_text)
        campaign_name = extract_campaign_from_text(story_text)
        body = extract_body__no_front_matter_no_autogen__from_text(story_text, "Historia")
        prev_chrono_story_ids = extract_previous_chrono_story_ids_from_text(story_text, story_uid)
        prev_campaign_story_ids = extract_previous_campaign_story_ids_from_text(story_text, story_uid)
        gms = extract_gms_from_text(story_text)
        players = extract_players_from_text(story_text)
        summary = extract_summary_section_body(story_text)
        location = extract_location_section_body(story_text)
        time_record = extract_time_record_from_text(story_text)
        return title, campaign_name, prev_chrono_story_ids, prev_campaign_story_ids, gms, \
               players, body, summary, location, time_record
    except IndexError:
        print(story_uid)
        raise Exception("Error with %s " % story_uid)


def extract_location_section_body(story_text: str) -> str:
    return extract_headerized_section_body(name="Lokalizacje", header_depth=1, finishing_header_scope=1,
                                           source=story_text, default=config.default_location_in_story())


def extract_summary_section_body(story_text: str) -> str:
    return extract_headerized_section_body(name="Streszczenie", header_depth=1, finishing_header_scope=1,
                                           source=story_text, default=config.default_summary_in_story())


def extract_story_title_from_text(story_text: str) -> str:
    return extract_single_param_from_front_matter(name="title", source=story_text,
                                                  default=config.default_story_title_in_story())


def extract_campaign_from_text(story_text: str) -> str:
    return extract_single_param_from_front_matter(name="campaign", source=story_text,
                                                  default=config.default_campaign_title())


def extract_players_from_text(story_text: str) -> List[str]:
    return extract_param_list_from_front_matter(name="players", source=story_text, list_separator=",",
                                                default=config.default_players_in_story())


def extract_gms_from_text(story_text: str) -> List[str]:
    return extract_param_list_from_front_matter(name="gm", source=story_text, list_separator=",",
                                                default=config.default_gms_in_story())


def extract_previous_campaign_story_ids_from_text(story_text: str, story_uid: str) -> str:

    # Records do not exist; we assume it is the START of the campaign as default
    prev_story_id = story_uid

    previous_stories_section = extract_headerized_section_body(name="Kontynuacja", header_depth=2, default="",
                                                               finishing_header_scope=2, source=story_text)

    if previous_stories_section:
        campaign_section = extract_headerized_section_body(name="Kampanijna", header_depth=3, finishing_header_scope=3,
                                                           default="", source=previous_stories_section)
        if campaign_section:
            links = extract_list_of_links(campaign_section, [])
            if links:
                prev_story_link_target = links[0][1]
                prev_story_id = prev_story_link_target.split(".")[0]

    return prev_story_id


def extract_previous_chrono_story_ids_from_text(story_text: str, story_uid: str) -> List[str]:

    # Records do not exist; we assume it is the START of the campaign as default
    prev_story_ids = [story_uid]

    previous_stories_section = extract_headerized_section_body(name="Kontynuacja", header_depth=2, default="",
                                                               finishing_header_scope=2, source=story_text)
    if previous_stories_section:
        chrono_section = extract_headerized_section_body(name="Chronologiczna", header_depth=3, default="",
                                                         finishing_header_scope=3, source=previous_stories_section)
        if chrono_section:
            links = extract_list_of_links(chrono_section, [])
            if links:
                link_targets = [lt[1] for lt in links]
                prev_story_ids = [si.split('.', 1)[0] for si in link_targets]

    return prev_story_ids


def extract_time_record_from_text(story_text: str) -> TimeRecord:

    delay_key = "opóźnienie"
    duration_key = "dni"

    time_section = extract_headerized_section_body(name="Czas", header_depth=1, finishing_header_scope=1,
                                                   default="", source=story_text)
    if time_section:

        default_time = {delay_key: str(config.default_story_delay()),
                        duration_key: str(config.default_story_duration())}
        actual_time = extract_dict_of_potentially_emphasised_keys(source=time_section, default=default_time)

        delay = _extract_number_from_record(actual_time, delay_key, config.default_story_delay())
        duration = _extract_number_from_record(actual_time, duration_key, config.default_story_duration())
        time_record = TimeRecord(duration=duration, delay=delay, is_default=False)

    else:
        # Section does not exist.
        time_record = config.default_story_time_record()

    return time_record


def _extract_number_from_record(source: Dict[str, str], key: str, default: int) -> int:
    if key in source:
        number = re.findall("(\d+)", source[key])[0]
        return int(number)
    else:
        return default

