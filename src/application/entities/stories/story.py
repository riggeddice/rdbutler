from typing import List, Tuple, Union, Any
from .time_record import TimeRecord


class Story:

    def __init__(
            self,
            uid: str,
            title: str,
            campaign: str,
            prev_story_ids_chrono: Union[List[str], Tuple[str, ...]],
            prev_story_id_campgn: str,
            gms: Union[List[str], Tuple[str, ...]],
            players: Union[List[str] or Tuple[str, ...]],
            body: str,
            summary: str,
            location: str,
            time_record: TimeRecord
    ):
        self.unique_id = uid
        self.title = title
        self.campaign = campaign
        self.previous_story_ids_chrono = tuple(prev_story_ids_chrono)
        self.previous_story_id_campaign = prev_story_id_campgn
        self.gms = tuple(gms)
        self.players = tuple(players)
        self.body = body
        self.summary = summary
        self.location = location
        self.time_record = time_record

        """Populated data is something added to Story after building it - for example, 'absolute story number'.
        It is a PUBLICLY ACCESSIBLE dict, mutable one and it is NOT checked in all_fields."""
        self.populated_data = {}

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        try:
            for field in ("unique_id", "title", "campaign", "body", "previous_story_ids_chrono",
                          "previous_story_id_campaign", "gms", "players", "summary", "location", "time_record"):
                assert getattr(self, field) == getattr(other, field)
            return True
        except AssertionError:
            return False

    def __repr__(self):
        return '{self.__class__.__name__}(unique_id="{self.unique_id}", title="{self.title}", ' \
               'campaign="{self.campaign}", body="{self.body}", ' \
               'previous_story_ids_chrono={self.previous_story_ids_chrono}, ' \
               'previous_story_id_campaign="{self.previous_story_id_campaign}", gms={self.gms}, ' \
               'players={self.players}, summary="{self.summary}", location="{self.location}", ' \
               'time_record={self.time_record}, populated_data={self.populated_data})'.format(self=self)


KEY_story_position_in_campaign = "story_position_in_campaign"
KEY_story_absolute_position = "story_absolute_position_in_index"
KEY_story_start_date = "story_start_date"
KEY_story_end_date = "story_end_date"
