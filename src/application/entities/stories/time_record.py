from typing import Any


class TimeRecord:

    def __init__(self, *, duration: int, delay: int, is_default: bool):
        self.duration = duration
        self.delay = delay
        self.is_default=is_default

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        return self.delay == other.delay and self.duration == other.duration and self.is_default == other.is_default

    def __repr__(self):
        return '{self.__class__.__name__}(duration={self.duration}, delay={self.delay}, ' \
               'default={self.is_default})'.format(self=self)