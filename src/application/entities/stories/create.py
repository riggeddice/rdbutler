from typing import List

from src.application.entities.stories.story import Story
from src.application.entities.stories.extract import extract_all_story_params_from_text
from src.application.entities import config


def create_story_with_default_params(*, uid="101214-default", title="default_title", campaign="default_campaign",
                                     body="default \n multiline body", previous_story_ids_chrono=None,
                                     prev_story_id_campgn = None, gms=None, players=None,
                                     summary=config.default_summary_in_story(),
                                     location=config.default_location_in_story(),
                                     time_record=None):

    # Trick, as default function arguments NEED to be immutable because of Python's design.
    time_record2 = time_record if time_record else config.default_story_time_record()
    previous_story_ids2 = previous_story_ids_chrono if previous_story_ids_chrono else [uid]
    gms2 = gms if gms else config.default_gms_in_story()
    players2 = players if players else config.default_players_in_story()
    prev_campgn_story_id2 = prev_story_id_campgn if prev_story_id_campgn else uid

    story = Story(uid=uid, title=title, campaign=campaign, body=body, prev_story_ids_chrono=previous_story_ids2,
                  prev_story_id_campgn=prev_campgn_story_id2, gms=gms2, players=players2, summary=summary,
                  location=location, time_record=time_record2)
    return story


def create_story(path: str, story_text: str) -> Story:

    uid = path.split('.', 1)[0]
    title, campaign, previous_story_ids, prev_campgn_story_id, gms, players, body, summary, \
        location, time_record = extract_all_story_params_from_text(story_text, uid)

    story = Story(uid=uid, title=title, campaign=campaign, body=body, prev_story_ids_chrono=previous_story_ids,
                  prev_story_id_campgn=prev_campgn_story_id, gms=gms, players=players, summary=summary,
                  location=location, time_record=time_record)
    return story


def create_stories(stories_with_paths: list) -> List[Story]:

    stories = []
    for swp in stories_with_paths:
        path = swp.path
        story_text = swp.entity_text

        stories.append(create_story(path, story_text))

    return stories
