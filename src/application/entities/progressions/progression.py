from typing import Any


class Progression:

    def __init__(self, originating_story: str, principal: str, actual_progression: str, is_faction=False):
        self.originating_story = originating_story
        self.principal = principal
        self.actual_progression = actual_progression
        self.is_faction = is_faction

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        return self.originating_story == other.originating_story and self.principal == other.principal and \
               self.actual_progression == other.actual_progression and self.is_faction == other.is_faction

    def __repr__(self):
        return '{self.__class__.__name__}(originating_story="{self.originating_story}", principal="{self.principal}", ' \
               'actual_progression="{self.actual_progression}") is_faction="{self.is_faction}"'.format(self=self)

# NOTE: "Principal" is a name of a Faction or an Actor
# TODO: Change "Principal" into something more clear
