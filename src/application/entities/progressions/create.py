from src.application.entities.progressions.extract import extract_actor_progression_section_body, \
    extract_faction_progression_section_body
from src.application.entities.progressions.extract import extract_progression_record_tuples
from src.application.entities.stories.story import Story
from .progression import Progression
from typing import List


def create_progressions_from_story(story: Story) -> List[Progression]:

    progression_section = extract_actor_progression_section_body(story.body)
    actor_progression_records = extract_progression_record_tuples(progression_section)
    faction_progression_section = extract_faction_progression_section_body(story.body)
    faction_progression_records = extract_progression_record_tuples(faction_progression_section)

    progressions = []
    for record in actor_progression_records:
        progressions.append(create_progression(story.unique_id, record))

    for record in faction_progression_records:
        progressions.append(create_progression(story.unique_id, record, True))

    return progressions


def create_progression_with_default_params(*, story_uid="101214-default", principal="default-actor",
                                           actual_progression="single-line text", is_faction=False):
    return Progression(originating_story=story_uid, principal=principal, actual_progression=actual_progression,
                       is_faction=is_faction)


def create_progression(story_uid: str, progression_tuple: tuple, is_faction=False) -> Progression:
    return Progression(story_uid, progression_tuple[0], progression_tuple[1], is_faction)
