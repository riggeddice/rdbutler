from typing import Any
"""
This class represents changes in the world that happened after a Story.
In Story text those are denoted as 'Progresja'
"""


class Consequence:

    def __init__(self, originating_story: str, actor: str, actual_consequence:str):
        self.originating_story = originating_story
        self.actor = actor
        self.actual_consequence = actual_consequence

    def __eq__(self, other: Any):
        if type(self) != type(other):
            return False
        return self.originating_story == other.originating_story and self.actor == other.actor and \
               self.actual_consequence == other.actual_consequence

    def __repr__(self):
        return '{self.__class__.__name__}(originating_story="{self.originating_story}", actor="{self.actor}", ' \
               'actual_consequence="{self.actual_consequence}")'.format(self=self)