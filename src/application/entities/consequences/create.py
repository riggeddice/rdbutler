from src.application.entities.stories.story import Story
from .consequence import Consequence
from .extract import extract_consequence_record_tuples
from .extract import extract_consequence_section_body
from typing import List


def create_consequences(entities: object) -> List[Consequence]:

    if isinstance(entities, list):
        result = create_consequences_from_stories(entities)
    elif isinstance(entities, Story):
        result = create_consequences_from_story(entities)
    else:
        raise ValueError("Expected either list or Story, got: " + str(type(entities)))

    return result


def create_consequences_from_stories(stories: List[Story]) -> List[Consequence]:

    consequences = []
    for story in stories:
        consequences.extend(create_consequences_from_story(story))
    return consequences


def create_consequences_from_story(story: Story) -> List[Consequence]:

    consequences_section = extract_consequence_section_body(story.body)
    consequence_records = extract_consequence_record_tuples(consequences_section)

    consequences = []
    for record in consequence_records:
        consequences.append(create_consequence(story.unique_id, record))

    return consequences


def create_consequence(story_uid: str, consequence_tuple: tuple) -> Consequence:
    return Consequence(story_uid, consequence_tuple[0], consequence_tuple[1] )
