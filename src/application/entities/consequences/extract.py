from typing import Tuple, List

from src.application.entities import config
from src.technical.extract_from_text import extract_headerized_section_body, extract_list_of_unnamed_key_values


def extract_consequence_section_body(story_text: str) -> str:

    return extract_headerized_section_body(name="Progresja", header_depth=1, finishing_header_scope=1,
                                           source=story_text, default=config.default_campaign_description())


def extract_consequence_record_tuples(consequence_section_body: str) -> List[Tuple[str, str]]:
    return extract_list_of_unnamed_key_values(source=consequence_section_body, default=[])
