from src.application.entities import config
from src.application.entities.actors.actor import Actor
from src.application.entities.config import default_campaign_uid
from src.application.entities.factions.faction import Faction
from src.external.neo4j.queries.db_query_structures import CampaignInfo
from src.technical.sanitize import strip_polish_letters


# TODO: Consider retrieve_uid_of_actor__for_db method. Where should it go, how to name it, if this should exist?
def retrieve_uid_of_actor__for_db(actor: Actor) -> str:
    if actor.uid:
        uid = actor.uid
    else:
        name = actor.name.strip().lower().replace(" ", "-").replace('"', '')
        name = strip_polish_letters(name)
        uid = "9999-" + name

    return uid


# TODO: Consider retrieve_uid_of_campaign, retrieve_uid_of_campaign_info. Look at retrieve_uid_of_actor__for_db. Same issue.
def retrieve_uid_of_campaign_info__for_file(campaign_info: CampaignInfo) -> str:
    campaign_prefix = "kampania-"
    if campaign_info.campaign_uid and campaign_info.campaign_uid != default_campaign_uid():
        if campaign_info.campaign_uid.startswith(campaign_prefix):
            return campaign_info.campaign_uid
        else:
            return campaign_prefix + campaign_info.campaign_uid
    elif campaign_info.campaign_title:
        title = campaign_info.campaign_title.strip().lower().replace(" ", "-").replace('"', '')
        title = strip_polish_letters(title)
        return campaign_prefix + title
    else:
        return campaign_prefix + default_campaign_uid()


def retrieve_uid_of_faction__for_db(faction: Faction) -> str:
    """We take the faction uid if present, and use faction name as id if not available"""
    if faction.uid and faction.uid != config.default_faction_uid():
        uid = faction.uid
    else:
        name = faction.name.strip().lower().replace(" ", "-").replace('"', '')
        name = strip_polish_letters(name)
        uid = name

    return uid


def retrieve_uid_of_faction__for_file(faction: Faction) -> str:
    """File requires "frakcja-" phrase. It is not in the DB, so we add it."""
    faction_prefix = "frakcja-"
    if faction.uid and faction.uid != config.default_faction_uid():
        if faction.uid.startswith(faction_prefix):
            return faction.uid
        else:
            return faction_prefix + faction.uid
    else:
        name = faction.name.strip().lower().replace(" ", "-").replace('"', '')
        name = strip_polish_letters(name)
        uid = faction_prefix + name

    return uid