import random
from typing import List

from src.external.neo4j.queries.db_query_structures import AspectWithItem
from src.rpg_mechanics.structure.aspects.aspect import Aspect


def generate_items_with_aspects(items: List, requested_items: int,
                                aspects: List[AspectWithItem], requested_aspects: int) -> List:
    random_items = select_random_from_list(items, requested_items)

    for item in random_items:
        add_aspects_for_item(item, aspects, requested_aspects)

    return random_items


def add_aspects_for_item(item, aspects: List[AspectWithItem], target_no_of_aspects: int):

    item_aspects = [a for a in aspects if a.item == item.name]
    item_aspects = [n for n in item_aspects if n not in item.aspects]
    aspect_names = []

    if item_aspects:
        random_aspects = select_random_from_list(item_aspects, target_no_of_aspects - len(item.aspects))
        aspect_names = [a.aspect for a in random_aspects]

    item.aspects.extend(list(map(Aspect, aspect_names)))

    return item


def select_random_from_list(items: List, requested_items: int) -> List:
    random_items = []
    if len(items) > requested_items:
        random_items = random.sample(items, requested_items)
    else:
        random_items.extend(items)
    return random_items


def extend_list_with_random_items(initial_items: List, items: List, requested_items: int) -> List:
    if len(initial_items) < requested_items:
        random_items = select_random_from_list(items, requested_items - len(initial_items))
        items = []
        items.extend(initial_items)
        items.extend(random_items)
        return items
    else:
        return initial_items
