import random
import re
from typing import List

from src.application.dataclasses.actor_autogen_features import ActorAutogenFeature
from src.technical.random_distributions import perform_roulette_selection


def pick_category(aaf: ActorAutogenFeature) -> str:
    categories = list(aaf.categorized_names.keys())
    return random.choice(categories)


def pick_name(aaf: ActorAutogenFeature, category: str) -> str:

    # For test purposes, make it possible to inject controllable randint to pick_name function
    try:
        randint = pick_name.test_injected_randint
    except:
        randint = random.randint

    # Select the list from which to use the name according to category
    lst = aaf.categorized_names[category]

    # To make sure less common names are pickable, we want to add 10% of most common name to all
    normalized_occurences = [(x[0], x[1] + int(lst[0][1]/10)) for x in lst]
    selected_name = perform_roulette_selection(normalized_occurences, randint)

    return selected_name


def pick_professions(aaf: ActorAutogenFeature) -> List[str]:

    # For test purposes, make it possible to inject controllable randint to pick_name function
    try:
        choice = pick_professions.test_injected_choice
    except:
        choice = random.choice

    professions_only = [x[1] for x in aaf.professions if not re.search("pozosta[lł][aeiy]", x[1], re.IGNORECASE)]
    selected_choices = [choice(professions_only) for i in range(2)]
    return selected_choices


def pick_emotion(aaf: ActorAutogenFeature) -> str:
    # For test purposes, make it possible to inject controllable randint to pick_name function
    try:
        randint = pick_emotion.test_injected_randint
    except:
        randint = random.randint

    # First, roulette the dyads:
    dyads = aaf.emotions.dyads

    roulette_friendly_dyads = [(x[0], int(x[3])) for x in dyads]
    selected_dyad = perform_roulette_selection(roulette_friendly_dyads, randint)

    # having the selected dyad, we have to see what emotions does it correspond to and with what intensity
    selected_complete_dyad = [x for x in dyads if x[0] == selected_dyad][0]
    base_emotions = aaf.emotions.base_emotions

    base_emotion_1 = [e for e in base_emotions if selected_complete_dyad[1] == e[0]][0]
    roulette_be_1 = [(base_emotion_1[i+0], int(base_emotion_1[i+1])) for i in range(0, 6, 2)]
    selected_be_1 = perform_roulette_selection(roulette_be_1, randint)

    base_emotion_2 = [e for e in base_emotions if selected_complete_dyad[2] == e[0]][0]
    roulette_be_2 = [(base_emotion_2[i + 0], int(base_emotion_2[i + 1])) for i in range(0, 6, 2)]
    selected_be_2 = perform_roulette_selection(roulette_be_2, randint)

    # Now, format it
    emotion = r"{dyad} ({first} + {second}), {first} -> {actual_1}, {second} -> {actual_2}".format(
        dyad=selected_dyad, first=base_emotion_1[0], second=base_emotion_2[0],
        actual_1=selected_be_1, actual_2=selected_be_2
    )
    return emotion


def pick_traits(aaf: ActorAutogenFeature) -> List[str]:
    traits = [random.choice(aaf.traits), random.choice(aaf.traits)]
    return traits
