from src.application.usecases.generate.helpers.generation_helper import front_matter_delimiter, front_matter_title


faction_front_matter_static = "layout: default\ncategories: faction\n"
faction_front_matter = front_matter_delimiter + faction_front_matter_static + front_matter_title + front_matter_delimiter
