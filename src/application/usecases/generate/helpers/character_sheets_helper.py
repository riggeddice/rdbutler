from src.application.usecases.generate.helpers.generation_helper import front_matter_delimiter, front_matter_title

front_matter_static = "layout: inwazja-karta-postaci\ncategories: profile\n"
front_matter_factions = """factions: \"%s\"\n"""
front_matter_type = """type: \"%s\"\n"""
front_matter_owner = """owner: \"%s\"\n"""
front_matter_player = """player: \"s\"\n"""

history_header = """\n\n# Historia:\n"""

merits_header = """\n## Dokonania:\n\n"""
merit_table_header = """|Data|Dokonanie|Misja|Początek|Koniec|Kampania|\n|-----|------|------|-----|------|------|\n"""
merit_table_row = "|%s|%s|[%s](%s)|%s|%s|[%s](%s)|\n"

met_actors_header = """\n\n## Relacje z postaciami:\n\n"""
met_actors_table_header = """|Z kim|Intensywność|Na misjach|\n|-----|------|------|\n"""
met_actors_table_row = """|[%s](%s)|%s|%s|\n"""

actor_front_matter = front_matter_delimiter + front_matter_static + "%s" + front_matter_title + front_matter_delimiter
