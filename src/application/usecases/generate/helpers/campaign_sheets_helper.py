from src.application.usecases.generate.helpers.generation_helper import front_matter_delimiter, front_matter_title

_front_matter_static = "layout: default\ncategories: inwazja, campaign\n"

campaign_title = "\n# Kampania: {{ page.title }}\n"

campaign_previous_section = "\n## Kontynuacja\n\n* [%s](%s.html)\n\n"

campaign_description_section = "## Opis\n\n%s\n\n"

history_header = "# Historia\n\n"

campaign_front_matter = front_matter_delimiter + _front_matter_static + front_matter_title + front_matter_delimiter
