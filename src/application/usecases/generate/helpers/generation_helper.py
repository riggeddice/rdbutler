front_matter_delimiter = "---\n"
front_matter_title = """title: \"%s\"\n"""
link_format = """[%s](%s)"""
h2 = """## %s\n"""

body_title = """# {{ page.title }}"""

progression_header = """## Progresja\n\n"""
progression_table_header_for_profile = """|Misja|Progresja|Kampania|\n|-----|------|------|\n"""
progression_table_row_for_profile = """|[%s](%s)|%s|%s|\n"""
progression_table_header_for_campaign = """|Misja|Progresja|\n|------|------|\n"""
progression_table_row_for_campaign = """|[%s](%s)|[%s](%s) %s|\n"""
progression_table_row_for_campaign_no_link = """|[%s](%s)|%s %s|\n"""

plan_header = """## Plany\n\n"""
plan_table_header_for_profile = """|Misja|Plan|Kampania|\n|-----|------|------|\n"""
plan_table_row_for_profile = """|[%s](%s)|%s|%s|\n"""
plan_table_header_for_campaign = """|Misja|Plan|\n|-----|-----|\n"""
plan_table_row_for_campaign = """|[%s](%s)|[%s](%s) %s|\n"""
plan_table_row_for_campaign_no_link = """|[%s](%s)|%s %s|\n"""
