from typing import List

from src.application.entities import config
from src.application.entities.actors.actor import Actor
from src.application.usecases.generate.helpers.campaign_sheets_helper import *
from src.application.usecases.generate.helpers.generation_helper import *
from src.external.jekyll import config as defaults
from src.external.jekyll.config import full_story_url, full_actor_url
from src.external.neo4j.queries.db_query_structures import CampaignInfo, StoryDataWithCampaignLink, \
    ActualProgressionInContext, ActualPlanInContext


def generate_campaign_sheet(campaign_info: CampaignInfo,
                            story_data_with_campaigns: List[StoryDataWithCampaignLink],
                            all_progressions: List[ActualProgressionInContext],
                            all_plans: List[ActualPlanInContext],
                            all_actors: List[Actor]) -> str:

    campaign_text = build_front_matter_for_campaign(campaign_info)
    campaign_text += campaign_title
    campaign_text += generate_previous_section(campaign_info)
    campaign_text += generate_campaign_description(campaign_info)

    campaign_stories = [s for s in story_data_with_campaigns if s.campaign_uid == campaign_info.campaign_uid]
    campaign_text += generate_campaign_history(campaign_stories)

    campaign_story_ids = [s.story_uid for s in campaign_stories]
    campaign_progressions = [p for p in all_progressions if p.story_uid in campaign_story_ids]
    campaign_text += generate_campaign_progressions(campaign_progressions, all_actors)
    campaign_plans = [p for p in all_plans if p.story_uid in campaign_story_ids]
    campaign_text += generate_campaign_plans(campaign_plans, all_actors)

    return campaign_text


def generate_campaign_progressions(progressions: List[ActualProgressionInContext], all_actors: List[Actor]) -> str:

    text = ""
    if progressions:
        actor_names = [a.name for a in all_actors]
        progressions.reverse()

        text = progression_header
        text += progression_table_header_for_campaign

        for progression in progressions:
            if progression.principal_name in actor_names:
                actor_uid = progression.principal_uid
                text += progression_table_row_for_campaign % (
                    progression.story_title, full_story_url(progression.story_uid), progression.principal_name,
                    full_actor_url(actor_uid), progression.actual_progression)
            else:
                text += progression_table_row_for_campaign_no_link % (
                    progression.story_title, full_story_url(progression.story_uid), progression.principal_name,
                    progression.actual_progression)

        text += "\n"

    return text


def generate_campaign_plans(plans: List[ActualPlanInContext], all_actors: List[Actor]) -> str:

    text = ""
    if plans:
        actor_names = [a.name for a in all_actors]
        plans.reverse()

        text = plan_header
        text += plan_table_header_for_campaign

        for plan in plans:
            if plan.principal_name in actor_names:
                actor_uid = plan.principal_uid
                text += plan_table_row_for_campaign % (
                    plan.story_title, full_story_url(plan.story_uid), plan.principal_name,
                    full_actor_url(actor_uid), plan.actual_plan)
            else:
                text += plan_table_row_for_campaign_no_link % (
                    plan.story_title, full_story_url(plan.story_uid), plan.principal_name,
                    plan.actual_plan)

        text += "\n"

    return text



def generate_previous_section(campaign: CampaignInfo) -> str:
    text = campaign_previous_section % (campaign.prev_campaign_title, "kampania-" + campaign.prev_campaign_uid)
    return text


def generate_campaign_description(campaign: CampaignInfo) -> str:
    if campaign.campaign_description and campaign.campaign_description != config.default_campaign_description():
        text = campaign_description_section % campaign.campaign_description
    else:
        text = campaign_description_section % ""
    return text


def generate_campaign_history(entries: List[StoryDataWithCampaignLink]) -> str:
    text = history_header
    for entry in entries:
        text += "1. " + link_format % (entry.story_title, defaults.full_story_url(entry.story_uid)) \
                + ": " + entry.story_start_date.strftime("%y/%m/%d") + " - " + entry.story_end_date.strftime("%y/%m/%d") \
                + "\n(" + entry.story_uid + ")\n" \
                + "\n" + entry.story_summary + "\n\n"
    return text


def build_front_matter_for_campaign(info: CampaignInfo) -> str:

    front_matter = campaign_front_matter % info.campaign_title
    return front_matter
