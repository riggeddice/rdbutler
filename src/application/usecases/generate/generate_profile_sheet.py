from typing import List

from src.application.entities.actors.actor import Actor
from src.application.usecases.generate.helpers.character_sheets_helper import *
from src.application.usecases.generate.helpers.generation_helper import *
from src.external.jekyll.config import full_story_url, full_campagin_url, full_actor_url
from src.external.neo4j.queries.db_query_structures import MeetingOf2Actors, ActorToFactionLink, ExtendedMeritInfo, \
    OwnershipInfo, ActualProgressionInContext, ActualPlanInContext
from src.rpg_domain.invasion.bloodlines import actor_is_from_bloodline, faction_is_actors_bloodline


def generate_profile_sheet(all_actor_to_faction_links: List[ActorToFactionLink], all_meetings: List[MeetingOf2Actors],
                           all_merit_infos: List[ExtendedMeritInfo], current_actor: Actor,
                           non_default_ownerships: List[OwnershipInfo],
                           all_progressions: List[ActualProgressionInContext],
                           all_plans: List[ActualPlanInContext]) -> str:

    profile_text = build_front_matter_for_actor(current_actor, all_actor_to_faction_links, non_default_ownerships)

    # Use already existing body (from DB)
    if current_actor.body:
        profile_text += current_actor.body
    else:
        profile_text += body_title

    # Start History section
    profile_text += history_header

    # Progressions
    progressions = [p for p in all_progressions if p.principal_name == current_actor.name]
    if progressions:
        profile_text += progression_header
        profile_text += progression_table_header_for_profile
        for progression in progressions:
            profile_text += progression_table_row_for_profile % (progression.story_title,
                                                                 full_story_url(progression.story_uid),
                                                                 progression.actual_progression,
                                                                 progression.campaign_name)
        profile_text += "\n"

    # Plans
    plans = [p for p in all_plans if p.principal_name == current_actor.name]
    if plans:
        profile_text += plan_header
        profile_text += plan_table_header_for_profile
        for plan in plans:
            profile_text += plan_table_row_for_profile % (plan.story_title,
                                                          full_story_url(plan.story_uid),
                                                          plan.actual_plan,
                                                          plan.campaign_name)

    # Merits
    merits = [m for m in all_merit_infos if m.actor_name == current_actor.name]
    if merits:
        profile_text += merits_header
        profile_text += merit_table_header
        merits.sort(key=lambda x: (x.story_start_date, x.story_uid), reverse=True)
        for merit in merits:
            profile_text += generate_merit_text(merit)

    # Meetings
    meetings_for_current_actor = [m for m in all_meetings if m.actor_1 == current_actor.name]
    if meetings_for_current_actor:
        profile_text += met_actors_header
        profile_text += met_actors_table_header
        sorted_met_actors = extract_actors_ordered_by_intensity(meetings_for_current_actor)
        for met_actor in sorted_met_actors:
            all_stories_when_they_met = [m for m in meetings_for_current_actor if m.actor_2 == met_actor]
            all_stories_when_they_met.sort(key=lambda x: (x.story_start_date, x.story_uid), reverse=True)
            intensity = len(all_stories_when_they_met)

            actor_meeting_cell_value = ""
            for story in all_stories_when_they_met:
                actor_meeting_cell_value += link_format % (story.story_uid[0:6],
                                                           full_story_url(story.story_uid)) + ", "
            met_actor_uid = [x.actor_2_uid for x in meetings_for_current_actor if x.actor_2 == met_actor][0]
            profile_text += met_actors_table_row % (met_actor,
                                                    full_actor_url(met_actor_uid),
                                                    intensity,
                                                    actor_meeting_cell_value[:-2])

    return profile_text


def generate_merit_text(merit: ExtendedMeritInfo):
    merit_text = (merit_table_row % (merit.story_uid[:6],
                                     merit.actual_merit,
                                     merit.story_title,
                                     full_story_url(merit.story_uid),
                                     merit.story_start_date.strftime("%y/%m/%d"),
                                     merit.story_end_date.strftime("%y/%m/%d"),
                                     merit.campaign_name,
                                     full_campagin_url(merit.campaign_uid)))
    return merit_text


def build_front_matter_for_actor(actor: Actor, all_faction_links: List[ActorToFactionLink],
                                 non_default_ownerships: List[OwnershipInfo]) -> str:

    dynamic_front_matter = ""
    actor_factions = [l for l in all_faction_links if actor.name == l.actor_name]
    actor_factions.sort(key=lambda x: x.faction_name)
    ownerships = [o for o in non_default_ownerships if actor.name == o.actor_name]

    if actor_factions:
        faction_text = ""
        for actor_faction in actor_factions:
            is_bloodline_member = actor_is_from_bloodline(actor)
            is_faction_his_bloodline = faction_is_actors_bloodline(actor, actor_faction.faction_name)

            # If we have a Diakon we will not put it into the faction. Because he *is* Diakon.
            if not (is_bloodline_member and is_faction_his_bloodline):
                faction_text += actor_faction.faction_name + ", "

        if not (len(actor_factions) == 1 and actor_is_from_bloodline(actor)):
            dynamic_front_matter += front_matter_factions % faction_text[:-2]

    if actor.actor_type:
        dynamic_front_matter += front_matter_type % actor.actor_type

    if len(ownerships) == 1:
        dynamic_front_matter += front_matter_owner % ownerships[0].owner_name

    final_actor_front_matter = actor_front_matter % (dynamic_front_matter, actor.name)

    return final_actor_front_matter


def extract_actors_ordered_by_intensity(actors: List[MeetingOf2Actors]) -> List[str]:

    actors.sort(key=lambda x: x.actor_2, reverse=True)
    actors_with_intensity = []
    while len(actors) > 0:
        actor = actors[0].actor_2
        meetings = [m for m in actors if m.actor_2 == actor]
        intensity = len(meetings)
        actors_with_intensity.append([intensity, actor])
        actors = [a for a in actors if a.actor_2 != actor]

    actors_with_intensity.sort(reverse=True)
    sorted_list = []
    for a in actors_with_intensity:
        sorted_list.append(a[1])
    return sorted_list

