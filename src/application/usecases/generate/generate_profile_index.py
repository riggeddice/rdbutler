from typing import List

from src.application.entities import config
from src.application.entities.actors.actor import Actor
from src.application.usecases.generate.helpers.character_index_helper import *
from src.external.neo4j.queries.db_query_structures import ActorToFactionLink, OwnershipInfo
from src.rpg_domain.invasion.bloodlines import actor_is_from_bloodline, faction_is_actors_bloodline


def generate_profile_index(all_actors: List[Actor], all_actor_to_faction_links: List[ActorToFactionLink],
                           non_default_ownerships: List[OwnershipInfo]):

    index_chars_with_sheets = ""
    index_chars_only_history = ""

    for actor in all_actors:

        if actor.uid and not actor.uid.startswith("9999-"):

            # We are dealing with a full-fledged Actor.
            index_chars_with_sheets += index_entry % (actor.name, actor.uid)
            actor_to_faction_links = [f for f in all_actor_to_faction_links if f.actor_name == actor.name]
            ownerships = [o for o in non_default_ownerships if actor.name == o.actor_name]

            if len(ownerships) == 1:
                # There exists an Owner for this Actor
                index_chars_with_sheets += ", " + ownerships[0].owner_name.capitalize()

            if len(actor_to_faction_links) > 0:
                actor_to_faction_links.sort(key=lambda x: x.faction_name)
                for atf_link in actor_to_faction_links:
                    is_faction_known = atf_link.faction_name != config.default_actor_factions()[0]
                    is_bloodline_member = actor_is_from_bloodline(actor)
                    is_faction_his_bloodline = faction_is_actors_bloodline(actor, atf_link.faction_name)

                    # If we have a Diakon we will not put it into the faction. Because he *is* Diakon.
                    if is_faction_known and not (is_bloodline_member and is_faction_his_bloodline):
                        index_chars_with_sheets += ", " + atf_link.faction_name

            index_chars_with_sheets += "\n"

        else:

            # This is a History-only Profile. Add required row.
            uid = actor.uid
            index_chars_only_history += index_entry % (actor.name, uid)
            index_chars_only_history += "\n"

    index_text = index_front_matter
    index_text += index_char_sheets_header
    index_text += index_chars_with_sheets
    index_text += index_history_only_header
    index_text += index_chars_only_history

    return index_text
