from typing import List

from src.application.entities.factions.faction import Faction
from src.application.usecases.generate.helpers.character_sheets_helper import *
from src.application.usecases.generate.helpers.faction_sheets_helper import faction_front_matter
from src.application.usecases.generate.helpers.generation_helper import *
from src.external.jekyll.config import full_story_url
from src.external.neo4j.queries.db_query_structures import ActualProgressionInContext, ActualPlanInContext


def generate_faction_sheet(current_faction: Faction,
                           all_progressions: List[ActualProgressionInContext],
                           all_plans: List[ActualPlanInContext]) -> str:

    faction_text = build_front_matter_for_faction(current_faction)

    # Use already existing body (from DB)
    if current_faction.body:
        faction_text += current_faction.body
    else:
        faction_text += body_title

    # Start History section
    faction_text += history_header

    # Progressions
    progressions = [p for p in all_progressions if p.principal_name == current_faction.name]
    if progressions:
        faction_text += progression_header
        faction_text += progression_table_header_for_profile
        for progression in progressions:
            faction_text += progression_table_row_for_profile % (progression.story_title,
                                                                 full_story_url(progression.story_uid),
                                                                 progression.actual_progression,
                                                                 progression.campaign_name)

        faction_text += "\n"

    # Plans
    plans = [p for p in all_plans if p.principal_name == current_faction.name]
    if plans:
        faction_text += plan_header
        faction_text += plan_table_header_for_profile
        for plan in plans:
            faction_text += plan_table_row_for_profile % (plan.story_title,
                                                          full_story_url(plan.story_uid),
                                                          plan.actual_plan,
                                                          plan.campaign_name)

        faction_text += "\n"

    return faction_text


def build_front_matter_for_faction(faction: Faction) -> str:
    return faction_front_matter % faction.name
