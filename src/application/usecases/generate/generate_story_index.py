from typing import List

from src.application.usecases.generate.helpers.campaign_index_helper import *
from src.application.usecases.generate.helpers.generation_helper import link_format, h2
from src.external.jekyll import config as defaults
from src.external.neo4j.queries.db_query_structures import StoryDataWithCampaignLink, CampaignInfo


def generate_story_index(all_campaigns: List[CampaignInfo],
                         all_stories_with_campaigns: List[StoryDataWithCampaignLink]):

    index_text = story_index_front_matter
    i = 1
    for campaign in all_campaigns:
        campaign_stories = [s for s in all_stories_with_campaigns if s.campaign_uid == campaign.campaign_uid]

        index_text += h2 % (str(i) + ". " + campaign.campaign_title + "\n")
        index_text += _generate_campaign_dates(campaign_stories)
        campaign_link = link_format % (campaign_link_title, defaults.full_campagin_url(campaign.campaign_uid)) + "\n\n"
        index_text += campaign_link
        index_text += _generate_campaign_history(campaign_stories) + "\n"
        i += 1

    return index_text


def _generate_campaign_dates(entries: List[StoryDataWithCampaignLink]) -> str:
    if len(entries) == 0:
        return ""

    campaign_start_date = entries[0].story_start_date
    campaign_end_date = entries[len(entries)-1].story_end_date

    return campaign_start_date.strftime("%y/%m/%d") + " - " + campaign_end_date.strftime("%y/%m/%d") + "\n"


def _generate_campaign_history(entries: List[StoryDataWithCampaignLink]) -> str:
    text = ""
    for entry in entries:
        link_name = entry.story_uid[0:6] + " - " + entry.story_title
        text += "1. " + link_format % (link_name, defaults.full_story_url(entry.story_uid)) \
                + ": " + entry.story_start_date.strftime("%y/%m/%d") + " - " \
                + entry.story_end_date.strftime("%y/%m/%d") + "\n"

    return text
