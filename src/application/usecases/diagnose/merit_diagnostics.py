import logging
from typing import List

from src.application.entities.actors.create import create_actors
from src.application.entities.merits.create import create_merits_from_stories
from src.application.entities.merits.merit import Merit
from src.application.entities.stories.create import create_stories
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_stories, read_all_actors
from src.technical.file_ops import place_file_in_outputs_folder

logger = logging.getLogger("merit_diagnostics")
handler = logging.FileHandler(place_file_in_outputs_folder('merit_diagnostics.log'), mode="w", encoding="UTF-8")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def merit_diagnostics():
    read_stories_with_paths = read_all_stories(config)
    stories = create_stories(read_stories_with_paths)
    merits = create_merits_from_stories(stories)
    check_merits(merits)
    return


def check_merits(merits: List[Merit]):
    logger.info("Checking merits...")
    read_actors_with_paths = read_all_actors(config)
    actors = create_actors(read_actors_with_paths)
    actor_names = [actor.name for actor in actors]
    incorrect_merits = []
    logger.info("=============================")
    logger.info("Checking merits for actors...")
    logger.info("=============================")
    for merit in merits:
        if merit.actor not in actor_names:
            logger.warning("Actor:   " + merit.actor + "   in story " + merit.originating_story + " does not have a Profile")
            incorrect_merits.append(merit)
    logger.info("===================================")
    logger.info("Checking merits for descriptions...")
    logger.info("===================================")
    for merit in merits:
        if merit.actual_merit.strip() == "" or merit.actual_merit.strip() == "TODO":
            logger.warning("Actual merit missing for : " + merit.actor +
                           "'s merit in story " + merit.originating_story)
            incorrect_merits.append(merit)

    logger.info("Checking merits done")
    if len(incorrect_merits) == 0:
        logger.info("No incorrect merits found...")

    return

