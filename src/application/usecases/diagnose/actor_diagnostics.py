import logging
from typing import List

from src.application.entities import config as defaults
from src.application.entities.actors.actor import Actor
from src.application.entities.actors.create import create_actors
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_actors
from src.technical.file_ops import place_file_in_outputs_folder

logger = logging.getLogger("actor_diagnostics")
handler = logging.FileHandler(place_file_in_outputs_folder('actor_diagnostics.log'), mode="w", encoding="UTF-8")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def actor_diagnostics():
    read_actors_with_paths = read_all_actors(config)
    all_actors = create_actors(read_actors_with_paths)
    actors_with_sheets = [actor for actor in all_actors if not actor.uid.startswith("9999")]
    diagnose_name(actors_with_sheets)
    diagnose_factions(actors_with_sheets)
    diagnose_description(actors_with_sheets)
    diagnose_actor_type(actors_with_sheets)
    diagnose_owner(actors_with_sheets)
    return


def diagnose_name(actors: List[Actor]):
    logger.info("=================")
    logger.info("Checking names...")
    logger.info("=================")
    missing_name = [actor for actor in actors if len(actor.name) == 0 or actor.name == defaults.default_actor_name()]
    for actor in missing_name:
        logger.warning("Missing name for actor " + actor.uid + ", version: " + actor.mechanics_version)

    logger.info("Checking names done")
    if len(missing_name) == 0:
        logger.info("No incorrect names found...")
    return


def diagnose_factions(actors: List[Actor]):
    logger.info("=================")
    logger.info("Checking factions...")
    logger.info("=================")
    missing_factions = [actor for actor in actors if
                        len(actor.factions) == 0 or
                        actor.factions == defaults.default_actor_factions() or
                        actor.factions == "none"]
    for actor in missing_factions:
        logger.warning("Missing factions for actor " + actor.uid + ", version: " + actor.mechanics_version)

    logger.info("Checking factions done")
    if len(missing_factions) == 0:
        logger.info("No incorrect factions found")
    return


def diagnose_description(actors: List[Actor]):
    logger.info("=======================")
    logger.info("Checking description...")
    logger.info("=======================")
    missing_description = [actor for actor in actors if not actor.description]
    for actor in missing_description:
        logger.warning("Missing description for actor " + actor.uid + ", version: " + actor.mechanics_version)

    logger.info("Checking description done")
    if len(missing_description) == 0:
        logger.info("No incorrect descriptions found")
    return


def diagnose_actor_type(actors: List[Actor]):
    logger.info("=======================")
    logger.info("Checking actor types...")
    logger.info("=======================")
    missing_actor_types = [actor for actor in actors if len(actor.actor_type) == 0 or
                           actor.actor_type == defaults.default_actor_type()]
    for actor in missing_actor_types:
        logger.warning("Missing actor type for actor " + actor.uid + ", version: " + actor.mechanics_version)

    logger.info("Checking actor types done")
    if len(missing_actor_types) == 0:
        logger.info("No incorrect actor types found")
    return


def diagnose_owner(actors: List[Actor]):
    logger.info("==================")
    logger.info("Checking owners...")
    logger.info("==================")
    missing_owners = [actor for actor in actors if (actor.owner == defaults.default_owner() and actor.actor_type == "PC")]
    for actor in missing_owners:
        logger.warning("Missing owner information for actor " + actor.uid + ", version: " + actor.mechanics_version)

    logger.info("Checking actor owners done")
    if len(missing_owners) == 0:
        logger.info("No incorrect actor owners found")
    return
