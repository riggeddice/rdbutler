import logging
from typing import List

from src.application.entities import config as defaults
from src.application.entities.stories.create import create_stories
from src.application.entities.stories.story import Story
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_stories
from src.technical.file_ops import place_file_in_outputs_folder

logger = logging.getLogger("story_diagnostics")
handler = logging.FileHandler(place_file_in_outputs_folder('story_diagnostics.log'), mode="w", encoding="UTF-8")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)


def story_diagnostics():
    read_stories_with_paths = read_all_stories(config)
    stories = create_stories(read_stories_with_paths)
    diagnose_chronology(stories)
    diagnose_players(stories)
    diagnose_gms(stories)
    diagnose_summary(stories)
    diagnose_campaign(stories)
    diagnose_title(stories)
    diagnose_time(stories)
    return


def diagnose_chronology(stories: List[Story]):
    logger.info("======================")
    logger.info("Checking chronology...")
    logger.info("======================")
    logger.info("============================")
    logger.info("Checking for missing info...")
    logger.info("============================")
    missing_chronology = [story for story in stories if len(story.previous_story_ids_chrono) == 0]
    if len(missing_chronology) > 0:
        logger.info("Missing chronology for " + str(len(missing_chronology)) + " stories")
    for story in missing_chronology:
        logger.warning("Missing chronology info in story " + story.unique_id)

    present_chronology=[story for story in stories if story not in missing_chronology]
    all_story_uids=[story.unique_id for story in stories]
    logger.info("============================")
    logger.info("Checking for broken links...")
    logger.info("============================")
    for story in present_chronology:
        if story.previous_story_ids_chrono not in all_story_uids:
            for uid in story.previous_story_ids_chrono:
                if uid not in all_story_uids:
                    logger.warning("Incorrect chronology link: "+uid+" in story " + story.unique_id)

    logger.info("Checking chronology done")
    if len(missing_chronology) == 0:
        logger.info("No incorrect chronologies found")
    return


def diagnose_summary(stories: List[Story]):
    logger.info("=====================")
    logger.info("Checking summaries...")
    logger.info("=====================")
    missing_summaries = [ story for story in stories if len(story.summary) == 0]
    if len(missing_summaries) > 0:
        logger.info("Missing summaries for " + str(len(missing_summaries)) + " stories")
    for story in missing_summaries:
        logger.warning("Missing summary in story " + story.unique_id)
    logger.info("Checking summaries done")
    if len(missing_summaries) == 0:
        logger.info("No incorrect summaries found")
    return


def diagnose_players(stories: List[Story]):
    logger.info("===================")
    logger.info("Checking players...")
    logger.info("===================")
    missing_players = [story for story in stories if
                       len(story.players) == 0 or story.players==defaults.default_players_in_story()]
    if len(missing_players) > 0:
        logger.info("Missing or default player data for " + str(len(missing_players)) + " stories")
    for story in missing_players:
        if len(story.players) == 0:
            logger.warning("Missing player info in story " + story.unique_id)
        if story.players == defaults.default_players_in_story():
            logger.warning("Default player info in story " + story.unique_id)
    logger.info("Checking players done")
    if len(missing_players) == 0:
        logger.info("No incorrect player info found")
    return


def diagnose_gms(stories: List[Story]):
    logger.info("===============")
    logger.info("Checking gms...")
    logger.info("===============")
    missing_gms = [story for story in stories if
                        (len(story.gms) == 0 or story.gms == defaults.default_gms_in_story())]
    if len(missing_gms) > 0:
        logger.info("Missing or default gm data for " + str(len(missing_gms)) + " stories")
    for story in missing_gms:
        if len(story.gms) == 0:
            logger.warning("Missing gm info in story " + story.unique_id)
        if story.gms == defaults.default_gms_in_story():
            logger.warning("Default gm info in story " + story.unique_id)
    logger.info("Checking gms done")
    if len(missing_gms) == 0:
        logger.info("No incorrect gm info found")
    return


def diagnose_campaign(stories: List[Story]):
    logger.info("=====================")
    logger.info("Checking campaigns...")
    logger.info("=====================")
    missing_campaigns = [story for story in stories if
                         (len(story.campaign) == 0 or story.campaign == defaults.default_campaign_title())]
    if len(missing_campaigns) > 0:
        logger.info("Missing or default campaign for " + str(len(missing_campaigns)) + " stories")
    for story in missing_campaigns:
        if len(story.campaign) == 0:
            logger.warning("Missing campaign info in story " + story.unique_id)
        if story.campaign == defaults.default_campaign_title():
            logger.warning("Default campaign info in story " + story.unique_id)
    logger.info("Checking campaigns done")
    if len(missing_campaigns) == 0:
        logger.info("No incorrect campaign info found")
    return


def diagnose_title(stories: List[Story]):
    logger.info("==================")
    logger.info("Checking titles...")
    logger.info("==================")
    missing_title = [story for story in stories if
                        (len(story.title) == 0 or story.title == defaults.default_story_title_in_story())]
    if len(missing_title) > 0:
        logger.info("Missing or default title for " + str(len(missing_title)) + " stories")
    for story in missing_title:
        if len(story.title) == 0:
            logger.warning("Missing title in story " + story.unique_id)
        if story.title == defaults.default_story_title_in_story():
            logger.warning("Default title in story " + story.unique_id)
    logger.info("Checking titles done")
    if len(missing_title) == 0:
        logger.info("No incorrect titles found")
    return


def diagnose_time(stories: List[Story]):
    logger.info("==================")
    logger.info("Checking time...")
    logger.info("==================")
    missing_time = [story for story in stories if story.time_record.is_default]
    if len(missing_time) > 0:
        logger.info("Missing time data for " + str(len(missing_time)) + " stories")
    for story in missing_time:
        logger.warning("Default time data in " + story.unique_id)
    logger.info("Checking times done")
    if len(missing_time) == 0:
        logger.info("No incorrect times found")
    return
