from typing import List
from collections import namedtuple
from copy import deepcopy

from src.application.entities import config


class LocationRecord:
    """Auxillary class, to make playing with tuples and tests more readable"""

    def __init__(self, *, header, path_to_self, path_to_parent):
        self.header = header
        self.path_to_self = path_to_self
        self.path_to_parent = path_to_parent

    def all_fields(self) -> tuple:
        return self.header, self.path_to_self, self.path_to_parent


def merge_locations_to_common_tree(location_sections: List[str]) -> str:
    """Main function: gets a list of location sections and returns a merged string"""

    master_tree = config.default_location_in_story()

    for section in location_sections:
        master_tree = merge_locations_only_headers(master_tree, section)

    return master_tree


def merge_locations_only_headers(tree_1: str, tree_2: str) -> str:

    # Trees need to be split into separate lines
    tree_1_lines = tree_1.split("\n")
    tree_2_lines = tree_2.split("\n")

    # Eliminate data; only headers remain
    headers_1 = extract_headers_from_lines(tree_1_lines)
    headers_2 = extract_headers_from_lines(tree_2_lines)

    # Create records which specifically contain paths_to_self and paths_to_parent
    records_1 = create_location_records_from_headers(headers_1)
    records_2 = create_location_records_from_headers(headers_2)

    # Merge records into record_tree:
    merged_records = merge_location_records(records_1, records_2)

    # Rebuild location string from records:
    merged_tree = location_records_to_string(merged_records)
    return merged_tree


def location_records_to_string(records: List[LocationRecord]) -> str:

    # We have a list of location records. Their headers are accessible though.
    merged_location = ""

    for record in records:
        merged_location += record.header + "\n"

    final_location = merged_location.rstrip()
    return final_location


def merge_location_records(records_1: List[LocationRecord], records_2: List[LocationRecord]) -> List[LocationRecord]:

    # Deepcopy left list. This is our operating list we will append to. Not strictly needed, but it is a good habit.
    merged_records = deepcopy(records_1)

    # Now, extend the list to get two different lists which are NOT unique
    merged_records.extend(records_2)

    # Sort them by path_to_self to get all the 'same' components close to each other
    merged_records.sort(key=lambda x: x.path_to_self)

    # Now we have non-unique records in the merged tree, but they are next to each other.
    # We cannot use a simple list(set(list)) here, as the objects have different reference. So, manual work
    unique_records = []
    previously_considered_path = "OUTSIDE_ROOT"
    for record in merged_records:
        if record.path_to_self != previously_considered_path:
            unique_records.append(record)
            previously_considered_path = record.path_to_self

    # Done
    return unique_records


def create_location_records_from_headers(headers: List[str]) -> List[LocationRecord]:

    # First step - scan list of strings to determine their depth needed in further classification

    HeaderWithDepth = namedtuple("HeaderWithDepth", "header depth")
    headers_with_depth = []
    for header in headers:
        headers_with_depth.append(HeaderWithDepth(header, len(header) - len(header.lstrip(" "))))

    # Second step - build a list of records with paths up to the point of root
    # We need two paths - a path to self and a path to parent only.

    records = []
    for i in range(0, len(headers_with_depth)):

        if i == 0:

            # Root is a special case; we have to build a very special record as it is shared and has no parent.
            # The 'path to parent' is set as 'path to self' due to lack of sensible 'what to put there'. Otherwise
            # I would have to make some kind of virtual root like '*' or so.
            records.append(LocationRecord(header=config.default_location_in_story(),
                                          path_to_parent=config.default_location_in_story(),
                                          path_to_self=config.default_location_in_story()))
        else:

            # Normal, understandable case.
            # Being at a particular point, we want to backtrack and find the first parent node,
            # then copy parent path_to_self as path_to_parent. Reason: as we do it from up to down
            # we are guaranteed there exists a parent there. So moving down to up we can do it in the
            # least movements needed.
            # Thus we need both lists: 'records' for paths and 'headers_with_depth' (for indices).

            current_depth = headers_with_depth[i].depth
            current_header = headers_with_depth[i].header.rstrip().title()

            for j in range(i, -1, -1):
                if current_depth > headers_with_depth[j].depth:
                    record_to_add = LocationRecord(header=current_header,
                                                   path_to_parent=records[j].path_to_self,
                                                   path_to_self=records[j].path_to_self + current_header.strip())
                    records.append(record_to_add)
                    break

    return records


def extract_headers_from_lines(tree_lines: List[str]) -> List[str]:

    headers = []
    for line in tree_lines:
        if "," in line:
            headers.append(line[0: line.find(",")])
        else:
            headers.append(line)

    return headers

