from typing import List, Tuple

from src.application.entities.campaigns.campaign import Campaign, KEY_campaign_distance_from_root, KEY_campaign_position_in_index
from src.application.entities.stories.story import Story, KEY_story_position_in_campaign, KEY_story_absolute_position


def populate_stories_with_campaign_data(stories: List[Story], campaigns: List[Campaign]) -> \
        Tuple[List[Story], List[Campaign]]:

    # Phase 1:
    # Link campaigns sequentially and re-sort them using the populated_data.
    # There can be two or more alternate campaigno-chronologies.
    all_campaigns = _populate_and_sort_campaigns(campaigns)

    # Phase 2: Set absolute campaign position in index AND sort them
    for index, campaign in enumerate(all_campaigns):
        campaign.populated_data[KEY_campaign_position_in_index] = index + 1

    all_stories = []

    # Phase 3: Set the populated_data for stories and sort them according to campaigns.
    for campaign in all_campaigns:

        # Extract only Stories relevant to this campaign:
        stories_from_this_campaign = [s for s in stories if s.campaign == campaign.uid]

        unique_ids = [s.unique_id for s in stories_from_this_campaign]

        stories_to_process = stories_from_this_campaign[:]
        processed_stories = {}

        root_stories = [story for story in stories_to_process if
                        story.previous_story_id_campaign == story.unique_id or
                        story.previous_story_id_campaign not in unique_ids]
        root_stories.sort(key=lambda s: s.unique_id)

        for index, root_story in enumerate(root_stories):
            root_story.populated_data[KEY_story_position_in_campaign] = index*len(unique_ids) + 1
            processed_stories[root_story.unique_id] = root_story
            stories_to_process.remove(root_story)

        while stories_to_process:
            for linked_story in [story for story in stories_to_process if
                                 story.previous_story_id_campaign in processed_stories]:

                previous_story_id = linked_story.previous_story_id_campaign
                pos = processed_stories[previous_story_id].populated_data[KEY_story_position_in_campaign] + 1

                linked_story.populated_data[KEY_story_position_in_campaign] = pos
                processed_stories[linked_story.unique_id] = linked_story
                stories_to_process.remove(linked_story)

        stories_from_this_campaign.sort(key=lambda x: x.populated_data[KEY_story_position_in_campaign])

        all_stories.extend(stories_from_this_campaign)

    # Phase 4: Set the absolute story sequence number for each story
    for index, story in enumerate(all_stories):
        story.populated_data[KEY_story_absolute_position] = index + 1

    return all_stories, all_campaigns


def _populate_and_sort_campaigns(campaigns):

    all_campaigns = []
    starting_campaigns = [c for c in campaigns if c.prev_campaign_uid == c.uid]
    starting_campaigns.sort(key=lambda c: c.uid)
    # Select starting campaigns and their direct descendants. This will create short chronological LINES.
    for starting_campaign in starting_campaigns:

        starting_campaign.populated_data[KEY_campaign_distance_from_root] = 0
        all_campaigns.append(starting_campaign)

        campaigns_derived_from_starting = [cc for cc in campaigns if
                                           cc.prev_campaign_uid == starting_campaign.uid and cc.uid != starting_campaign.uid]
        campaigns_derived_from_starting.sort(key=lambda c: c.uid)

        for every_campaign in campaigns_derived_from_starting:
            every_campaign.populated_data[KEY_campaign_distance_from_root] = 1
            all_campaigns.append(every_campaign)

    # Deal with other campaigns - now we want a WIDE sort, not a DEEP one.
    second_and_more_level_campaigns = [smlc for smlc in campaigns if smlc not in all_campaigns]
    second_and_more_level_campaigns.sort(key=lambda c: c.uid)
    distance_from_root_smlc = 2
    for smlc in second_and_more_level_campaigns:

        current_camp = smlc
        counter = 0
        match = False
        while not match:
            prev_camps = [pc for pc in all_campaigns if pc.uid == current_camp.prev_campaign_uid]
            if prev_camps:
                # Match
                match = True
                smlc.populated_data[KEY_campaign_distance_from_root] = distance_from_root_smlc + counter
            else:
                counter += 1
                current_camp = \
                [pc for pc in second_and_more_level_campaigns if pc.uid == current_camp.prev_campaign_uid][0]

                if counter > 500:
                    raise RecursionError("Potential infinite loop detected at Campaign: " + current_camp.uid)

    second_and_more_level_campaigns.sort(key=lambda x: x.prev_campaign_uid)
    second_and_more_level_campaigns.sort(key=lambda x: x.populated_data[KEY_campaign_distance_from_root])

    all_campaigns.extend(second_and_more_level_campaigns)

    return all_campaigns
