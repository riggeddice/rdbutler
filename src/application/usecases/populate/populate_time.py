from datetime import timedelta, date
from src.application.entities.stories.story import *


def populate_time_in_stories_by_chrono_order(stories:List[Story], starting_date:date) -> List[Story]:
    """This method can only accept complete set of stories that are correctly linked.
    Only then resulting assignments will make sense. If some error in links are found,
    method will print out message specifying which stories are corrupt, but then all the date assignments may be off.
    Method will take starting date from passed config, so make sure it is available."""

    starting_stories = [s for s in stories if
                        ((len(s.previous_story_ids_chrono) > 0
                          and s.unique_id in s.previous_story_ids_chrono))]

    for starting_story in starting_stories:
        delay = starting_story.time_record.delay
        starting_story.populated_data[KEY_story_start_date] = starting_date + timedelta(days=delay)
        duration = starting_story.time_record.duration
        starting_story.populated_data[KEY_story_end_date] = starting_story.populated_data[KEY_story_start_date] \
                                                            + timedelta(days=duration)

    valid_stories = [s for s in stories if (len(s.previous_story_ids_chrono) > 0 and s not in starting_stories)]
    invalid_stories = [s for s in stories if s not in valid_stories and s not in starting_stories]

    processed_stories = []
    processed_stories.extend(starting_stories)

    while len(valid_stories) > 0:

        for story in valid_stories:
            # If there are 2 chronological links, we take the first one - it doesn't matter, as delta needs
            # to come from SOMETHING. Thus, index [0].
            previous_stories = [s for s in stories if s.unique_id == story.previous_story_ids_chrono[0]]
            previous_story = _guarded_extract_previous_story(story, previous_stories)

            if previous_story in invalid_stories:
                valid_stories.remove(story)
                invalid_stories.append(story)
            elif previous_story in processed_stories:
                delay = story.time_record.delay
                duration = story.time_record.duration
                story.populated_data[KEY_story_start_date] = previous_story.populated_data[KEY_story_end_date] + \
                                                                timedelta(days=delay+1)
                story.populated_data[KEY_story_end_date] = story.populated_data[KEY_story_start_date] + \
                                                                timedelta(days=duration)
                valid_stories.remove(story)
                processed_stories.append(story)

    for s in invalid_stories:

        error = ""
        next_story_id = "none"
        if len(s.previous_story_ids_chrono) > 0:
            next_story_id = s.previous_story_ids_chrono[0]

        error += "Found invalid story: " + s.unique_id + " -> " + next_story_id + "\n"
        raise ValueError(error)

    return processed_stories


def _guarded_extract_previous_story(story: Story, previous_stories: List[Story]):

    if previous_stories:
        previous_story = previous_stories[0]
    else:
        raise ValueError("Errorenous data in Story: " + story.unique_id + " at Previous Stories section.")

    return previous_story
