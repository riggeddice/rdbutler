import re

from src.application.entities import config


def normalize_story_location_section(location_section: str) -> str:

    # Eliminate leading and trailing whitespaces
    result = location_section.strip()

    # If no structure of type '1. xxx', return default common root.
    if not re.match("^1\. Świat", result, re.MULTILINE):
        return config.default_location_in_story()

    return result

