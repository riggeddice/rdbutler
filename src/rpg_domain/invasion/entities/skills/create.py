from typing import List

from src.rpg_domain.invasion.entities.skills.skill import Skill
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.aspects.create import create_aspects_from_list
from src.rpg_mechanics.structure.categories.extract import extract_category_tuples_from_section


def create_skills_from_actor_text(actor_body: str, mechver: str) -> List[Skill]:

    section = Skill.extract_section_body(actor_body)
    tuples = extract_category_tuples_from_section(section, mechver)

    entities = [create_skill(name=name, aspects=create_aspects_from_list(value)) for name, value in tuples]

    return entities


def create_skill(name: str, aspects: List[Aspect]) -> Skill:
    return Skill(name=name.lower(), aspects=aspects)

