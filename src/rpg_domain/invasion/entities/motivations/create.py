from typing import List

from src.rpg_domain.invasion.entities.motivations.motivation import Motivation
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.aspects.create import create_aspects_from_list
from src.rpg_mechanics.structure.categories.extract import extract_category_tuples_from_section


def create_motivations_from_actor_text(actor_body: str, mechver: str) -> List[Motivation]:

    section = Motivation.extract_section_body(actor_body)
    tuples = extract_category_tuples_from_section(section, mechver)

    entities = [create_motivation(name=name, aspects=create_aspects_from_list(value)) for name, value in tuples]

    return entities


def create_motivation(name: str, aspects: List[Aspect]) -> Motivation:
    return Motivation(name=name.lower(), aspects=aspects)
