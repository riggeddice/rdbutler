from typing import List

from src.rpg_mechanics.structure.categories.category import Category
from src.rpg_mechanics.structure.categories.extract import extract_category_section_body


class Motivation(Category):

    @staticmethod
    def extract_section_body(actor_text: str) -> str:
        return extract_category_section_body(potential_headers=Motivation.headers(), actor_text=actor_text)

    @staticmethod
    def headers() -> List[str]:
        return ["Motywacje (do czego dąży)", "Motywacje"]
