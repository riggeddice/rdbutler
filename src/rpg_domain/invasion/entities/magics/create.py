from typing import List

from src.rpg_domain.invasion.entities.magics.magic import Magic
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.aspects.create import create_aspects_from_list
from src.rpg_mechanics.structure.categories.extract import extract_category_tuples_from_section


def create_magics_from_actor_text(actor_body: str, mechver: str) -> List[Magic]:

    section = Magic.extract_section_body(actor_body)
    tuples = extract_category_tuples_from_section(section, mechver)

    entities = [create_magic(name=name, aspects=create_aspects_from_list(value)) for name, value in tuples]

    return entities


def create_magic(name: str, aspects: List[Aspect]) -> Magic:
    return Magic(name=name.lower(), aspects=aspects)
