from src.application.entities.actors.actor import Actor


def faction_is_actors_bloodline(actor: Actor, faction: str) -> bool:
    factions_to_bloodlines = get_factions_to_bloodline_map()
    if faction in factions_to_bloodlines:
        surnames = factions_to_bloodlines[faction]
        for surname in surnames:
            if surname in actor.name:
                return True

    return False


def actor_is_from_bloodline(actor: Actor) -> bool:
    for bloodline in get_all_bloodlines():
        if bloodline in actor.name:
            return True

    return False


def get_bloodlines_to_factions_map() -> {str: str}:
    return {"Bankierz": "Bankierz",
            "Blakenbauer": "Blakenbauer",
            "Diakon": "Diakon",
            "Maus": "Maus",
            "Myszeczka": "Myszeczka",
            "Zajcew": "Zajcew",
            "Sowiński": "Sowińscy",
            "Sowińska": "Sowińscy",
            "Weiner": "Weiner"}


def get_factions_to_bloodline_map() -> {str: str}:
    return {"Bankierz": ["Bankierz"],
            "Blakenbauer": ["Blakenbauer"],
            "Diakon": ["Diakon"],
            "Maus": ["Maus"],
            "Myszeczka": ["Myszeczka"],
            "Zajcew": ["Zajcew"],
            "Sowińscy": ["Sowiński", "Sowińska"],
            "Weiner": ["Weiner"]}


def get_all_bloodlines() -> []:
    return ["Bankierz",
            "Blakenbauer",
            "Diakon",
            "Maus",
            "Myszeczka",
            "Zajcew",
            "Sowińska",
            "Sowiński",
            "Weiner"]
