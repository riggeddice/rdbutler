

def create_regex_extracting_text_to_numbers_table():
    """
    This is used to extract the column looking like the following:
    XXXX 1234
    AASDLJD 11111111
    It ignores comments and everything NOT of this structure.
    :return: regex string
    """

    regex = r'^\b(?P<{column_1}>\w+)\b\s+\b(?P<{column_2}>\d+)\b'.format(column_1="text", column_2="number")
    return regex


def create_regex_extracting_param_from_front_matter(name: str) -> str:

    safe_name = _sanitize_for_regex(name)
    regex = r'---.+?^{name}:\s*\"*(.+?)\"*\s*$.*---'.format(name=safe_name)
    return regex


def create_regex_extracting_header_section(header_depth: int, blocking_header_depth: int, name: str) -> str:

    corrected_name = _sanitize_for_regex(name)
    blocking_depth = str(blocking_header_depth)

    header = "#" * header_depth

    # The regex below uses '{' and '}'. This is ESCAPED, so '{{' and '}}' are read as '{' and '}'. Sorry.
    return fr"^{header}\s{corrected_name}[:|\s]*$(.+?)(\Z|^#{{1,{blocking_depth}}}\s\w+)"


def _sanitize_for_regex(word: str) -> str:
    parens = word.replace(r"(", r"\(").replace(r")", r"\)")
    return parens
