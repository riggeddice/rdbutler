

def sanitize_input_for_db(text: str) -> str:
    return text.replace(r'"', r'\"').replace("'", r"\'").replace(r"\|", r"\\|")


def strip_polish_letters(text: str) -> str:
    return text.replace("ą", "a").replace("ę", "e").replace("ć", "c").replace("ł", "l") \
        .replace("ó", "o").replace("ś", "s").replace("ż", "z").replace("ź", "z").replace("ń", "n") \
        .replace("Ą", "A").replace("Ę", "E").replace("Ć", "C").replace("Ł", "L") \
        .replace("Ó", "O").replace("Ś", "S").replace("Ż", "Z").replace("Ź", "Z").replace("Ń", "N")
