import os
import sys
from time import time

from src.external.jekyll.config import full_path_to_profiles_directory, full_path_to_stories_directory, \
    full_path_to_factions_directory


class EntityTextWithPath:
    """Auxillary class to deliver read files"""
    def __init__(self, entity_text, path: str):
        self.entity_text = entity_text
        self.path = path


def store_text_in_outputs(entity_name: str, to_store: str):

    filename = entity_name + str(time()) + ".txt"
    path = place_file_in_outputs_folder(filename)
    save_file(to_store, path)


def place_file_in_outputs_folder(filename: str) -> str:
    """This is a path to the Outputs folder - all high level commands having textual outputs
    should output there, unless specified otherwise (configs, users...)."""

    calling_command_path = os.path.dirname(sys.argv[0])
    rdbutler_path = calling_command_path[0: calling_command_path.find("src")]
    outputs_path = rdbutler_path + "/outputs"
    return outputs_path + "/" + filename


def place_file_in_rd_profiles_folder(filename: str) -> str:
    return full_path_to_profiles_directory() + "/" + filename


def place_file_in_rd_stories_folder(filename: str) -> str:
    return full_path_to_stories_directory() + "/" + filename


def place_file_in_rd_factions_folder(filename: str) -> str:
    return full_path_to_factions_directory() + "/" + filename


def read_file(file_path: str):

    file_to_read = open(file_path, mode='r', encoding='utf-8')
    read_text = file_to_read.read()
    file_to_read.close()

    return read_text


def save_file(text: str, file_path: str):
    to_write = open(file_path, 'w', encoding='utf-8')
    to_write.write(text)
    to_write.close()
