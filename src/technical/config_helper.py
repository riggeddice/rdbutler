import os
import yaml


def load(path: str, *, config_file_name='config.yml') -> dict:
    """
    :param path: path to folder (or file inside folder) containing configuration file
    :param config_file_name: configuration file name
    :return: returns a dictionary with loaded configuration
    """
    path = os.path.dirname(path)
    config_path = os.path.join(path, config_file_name)
    config = yaml.load(open(config_path))
    local_config_path = os.path.join(path, '{}.local'.format(config_file_name))
    if os.path.exists(local_config_path):
        config.update(yaml.load(open(local_config_path)))
    return config
