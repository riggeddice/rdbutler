from typing import List, Tuple


def perform_roulette_selection(lst: List[Tuple[str, int]], randint):
    roulette_list = roulette_breakpoint_list(lst)

    # roulette values are ascending. We are looking for LAST record SMALLER than the value.
    value = randint(1, roulette_list[-1][1] + 1)
    all_above = [x for x in roulette_list if x[1] >= value]
    selected = all_above[0][0] if all_above else roulette_list[-1][0]
    return selected


def roulette_breakpoint_list(lst: List[Tuple[str, int]]):
    """
    Assumption: we get a list of "(value, number)", [like ("cats", 100), ("dogs", 50)]. The goal is to return the list
    according to roulette algorithm: ("cats", 100), ("dogs", 150).
    :param lst: list to be roulette-ized, in form of [("cats", 100)]. Does not work with NEGATIVE weights.
    :return: the same list with weights
    """

    breakpoint_list = []
    previous_max = 0
    for element in lst:
        previous_max = previous_max + element[1]
        breakpoint_list.append((element[0], previous_max))

    return breakpoint_list
