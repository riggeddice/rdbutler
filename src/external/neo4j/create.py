from .neo4jdb import Neo4jDb
from src.technical import config_helper


def create_neo4jdb() -> Neo4jDb:
    config = config_helper.load(__file__)
    return Neo4jDb(url=config['url'], login=config['login'], pwd=config['pwd'])
