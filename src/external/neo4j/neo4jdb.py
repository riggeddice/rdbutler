from neo4j.v1 import GraphDatabase, basic_auth

from src.external.neo4j.convert_to_entities import *
from src.external.neo4j.queries.queries_modify import *
from src.external.neo4j.queries.queries_read import *
from src.rpg_domain.invasion.bloodlines import get_bloodlines_to_factions_map
from src.rpg_domain.invasion.entities.motivations.motivation import Motivation


class Neo4jDb:

    def __init__(self, url: str, login: str, pwd: str):
        self._driver = GraphDatabase.driver(url, auth=basic_auth(login, pwd))

    # CLEAR DATABASE

    def clear_database(self):
        query = detach_delete_all_nodes()
        self._execute_update_query(driver=self._driver, query=query)

    # CREATE OR UPDATE

    def create_actors_ecosystem(self, actors: List[Actor]):
        chunks = [actors[x:x + 1] for x in range(0, len(actors), 1)]

        queries = []
        for one_chunk in chunks:
            queries.extend([create_actor_ecosystem(one_chunk)])

        self._execute_update_queries(driver=self._driver, queries=queries)

    def create_factions_ecosystem(self, factions: List[Faction]):
        chunks = [factions[x:x + 1] for x in range(0, len(factions), 1)]

        queries = []
        for one_chunk in chunks:
            queries.extend([create_factions_ecosystem(one_chunk)])

        self._execute_update_queries(driver=self._driver, queries=queries)

    def create_campaigns_ecosystem(self, campaigns: List[Campaign]):
        chunks = [campaigns[x:x + 1] for x in range(0, len(campaigns), 1)]

        queries = []
        for one_chunk in chunks:
            queries.extend([create_campaign_ecosystem(one_chunk)])

        self._execute_update_queries(driver=self._driver, queries=queries)

    def create_stories_ecosystem(self, stories: List[Story]):
        queries = [create_story_ecosystem(story) for story in stories]
        self._execute_update_queries(driver=self._driver, queries=queries)

    def link_to_predecessors(self, stories: List[Story], campaigns: List[Campaign]):

        queries = []
        for story in stories:
            if len(story.previous_story_ids_chrono) > 0:
                queries.extend([link_single_story_node_with_chronological_predecessors(story)])

            queries.extend([link_single_story_node_with_campaign_predecessor(story)])

        for campaign in campaigns:
            queries.extend([link_campaign_with_predecessor(campaign)])

        self._execute_update_queries(self._driver, queries)

    def link_unassigned_actors_to_default_player(self):
        query = link_unlinked_actors_with_default_player()
        self._execute_update_query(driver=self._driver, query=query)

    def link_unlinked_actors(self):

        queries = []
        bloodline_faction_pairs = get_bloodlines_to_factions_map()
        for surname in bloodline_faction_pairs:
            queries.extend([link_actors_with_factions(surname, bloodline_faction_pairs[surname])])

        self._execute_update_queries(self._driver, queries)

    def set_missing_faction_uids(self):

        query = get__all_factions_without_uid()
        results = self._execute_read_query(self._driver, query)
        factions_without_uid = get_all_factions(results)

        if len(factions_without_uid) > 0:
            queries = []
            for faction in factions_without_uid:
                queries.append(update_missing_faction_uid(faction))

            self._execute_update_queries(self._driver, queries)

    def set_missing_actor_uids(self):

        query = get__all_actors_without_uid()
        results = self._execute_read_query(self._driver, query)
        actors_without_uid = get_all_actors(results)

        if len(actors_without_uid) > 0:
            queries = []
            for actor in actors_without_uid:
                queries.append(update_missing_actor_uid(actor))

            self._execute_update_queries(self._driver, queries)

    # READ

    def get_contact_names_with_intensity(self, name: str) -> List[ActorNameWithIntensity]:

        query = get__all_actor_names__met_by_named_actor(name)
        results = self._execute_read_query(self._driver, query)
        repacked = get_contact_names_for(results)
        return repacked

    def get_stories_when_actors_met(self, name1: str, name2: str) -> List[StoryUidWithTitle]:
        query = get__all_story_uids__where_two_actors_met(name1, name2)
        results = self._execute_read_query(self._driver, query)
        repacked = in_which_stories_did_two_actors_meet(results)
        return repacked

    def get_merits_in_context(self, name: str) -> List[ActualMeritInContext]:
        query = get__all_merits__for_named_actor(name)
        results = self._execute_read_query(self._driver, query)
        repacked = what_when_did_actor_do(results)
        return repacked

    def get_all_actor_to_faction_links(self) -> List[ActorToFactionLink]:
        query = get__all_faction_links()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_faction_links(results)
        return repacked

    def get_all_actors(self) -> List[Actor]:
        query = get__all_actors()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_actors(results)
        return repacked

    def get_all_factions(self) -> List[Faction]:
        query = get__all_factions()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_factions(results)
        return repacked

    def get_all_meetings(self) -> List[MeetingOf2Actors]:
        query = get__all_meetings()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_meetings(results)
        return repacked

    def get_all_merit_infos(self) -> List[ExtendedMeritInfo]:
        query = get__all_merits()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_merits(results)
        return repacked

    def get_all_progression_infos(self) -> List[ActualProgressionInContext]:
        query = get__all_progressions()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_progressions(results)
        return repacked

    def get_all_plan_infos(self) -> List[ActualPlanInContext]:
        query = get__all_plans()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_plans(results)
        return repacked

    def get_non_default_ownerships(self) -> List[OwnershipInfo]:
        query = get__non_default_ownerships()
        results = self._execute_read_query(self._driver, query)
        repacked = get_non_default_ownerships(results)
        return repacked

    def get_all_campaign_infos(self) -> List[CampaignInfo]:
        query = get__all_campaigns()
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_campaigns(results)
        return repacked

    def get_all_story_data_with_campaign_links(self) -> List[StoryDataWithCampaignLink]:
        query = get__all_stories_with_campaigns()
        results = self._execute_read_query(self._driver, query)
        repacked = get_stories_with_campaigns(results)
        return repacked

    def get_all_skills(self) -> List[Skill]:
        query = get__all_item_names("Skill")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_skills(results)
        return repacked

    def get_all_magics(self) -> List[Magic]:
        query = get__all_item_names("Magic")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_magics(results)
        return repacked

    def get_all_motivations(self) -> List[Motivation]:
        query = get__all_item_names("Motivation")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_motivations(results)
        return repacked

    def get_all_aspects_with_skills(self) -> List[AspectWithItem]:
        query = get__all_aspects_with_item("Skill")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_aspects_with_items(results)
        return repacked

    def get_all_aspects_with_magics(self) -> List[AspectWithItem]:
        query = get__all_aspects_with_item("Magic")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_aspects_with_items(results)
        return repacked

    def get_all_aspects_with_motivations(self) -> List[AspectWithItem]:
        query = get__all_aspects_with_item("Motivation")
        results = self._execute_read_query(self._driver, query)
        repacked = get_all_aspects_with_items(results)
        return repacked

    def get_items_with_relations(self, item1_type, item2_type) -> List[ItemsRelationWithIntensity]:
        query = get__item_relations_with_intensity(item1_type, item2_type)
        results = self._execute_read_query(self._driver, query)
        repacked = get_items_relations_with_intensity(results)
        return repacked

    # DRIVER

    @staticmethod
    def _execute_update_query(driver, query):
        session = driver.session()
        session.run(query)
        session.close()

    @staticmethod
    def _execute_update_queries(driver, queries):
        session = driver.session()
        for query in queries:
            session.run(query)

        session.close()

    @staticmethod
    def _execute_read_query(driver, query):
        session = driver.session()
        result = session.run(query)
        session.close()
        return result
