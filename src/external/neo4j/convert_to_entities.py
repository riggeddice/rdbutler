from datetime import date
from typing import List

from src.application.entities.actors.create import create_actor_with_defaults, Actor
from src.application.entities.factions.create import create_faction_with_defaults, Faction
from src.external.neo4j.queries.db_query_structures import *
from src.rpg_domain.invasion.entities.magics.magic import Magic
from src.rpg_domain.invasion.entities.motivations.motivation import Motivation
from src.rpg_domain.invasion.entities.skills.skill import Skill


def get_contact_names_for(results) -> List[ActorNameWithIntensity]:

    actor_names_with_intensity = []
    for result in results:
        actor_names_with_intensity.append(ActorNameWithIntensity(result['name'],
                                                                 result["times_met"]))

    return actor_names_with_intensity


def in_which_stories_did_two_actors_meet(results) -> List[StoryUidWithTitle]:

    stories_uid_with_title = []

    for result in results:
        stories_uid_with_title.append(StoryUidWithTitle(result["story_uid"],
                                                        result["story_title"]))

    return stories_uid_with_title


def what_when_did_actor_do(results) -> List[ActualMeritInContext]:

    merits = []
    for result in results:
        merits.append(ActualMeritInContext(result["story_id"],
                                           result["story_title"],
                                           result["merit"],
                                           result["campaign"]))

    return merits


def get_all_actors(results) -> List[Actor]:

    actors = []
    for result in results:
        actor_record = result['actor']
        actor = create_actor_with_defaults(uid=actor_record["uid"],
                      mechanics_version=actor_record["mechanics_version"],
                      name=actor_record["name"],
                      actor_type=actor_record["actor_type"],
                      factions=actor_record["guild"],
                      mechanics=actor_record["rpg_mechanics"],
                      description=actor_record["description"],
                      body=actor_record["body"])
        actors.append(actor)

    return actors


def get_all_factions(results) -> List[Faction]:

    factions = []
    for result in results:
        faction_record = result['faction']
        faction = create_faction_with_defaults(uid=faction_record["uid"],
                      name=faction_record["name"],
                      body=faction_record["body"])
        factions.append(faction)

    return factions


def get_all_faction_links(results) -> List[ActorToFactionLink]:

    actor_faction_links = []
    for result in results:
        actor_faction_links.append(ActorToFactionLink(result["actor_name"],
                                                      result["actor_uid"],
                                                      result["faction_name"]))
    return actor_faction_links


def get_all_meetings(results) -> List[MeetingOf2Actors]:

    meetings = []
    for result in results:
        meetings.append(MeetingOf2Actors(result["actor1"],
                                         result["actor1_uid"],
                                         result["actor2"],
                                         result["actor2_uid"],
                                         result["story_uid"],
                                         result["story_title"],
                                         __parse_date_from_str(result["story_start_date"]),
                                         result["campaign"]))
    return meetings


def get_all_merits(results) -> List[ExtendedMeritInfo]:

    merits = []
    for result in results:
        merits.append(ExtendedMeritInfo(result["actor_name"],
                                        result["story_id"],
                                        result["story_title"],
                                        __parse_date_from_str(result["story_start_date"]),
                                        __parse_date_from_str(result["story_end_date"]),
                                        result["merit"],
                                        result["campaign_uid"],
                                        result["campaign_name"]))
    return merits


def get_all_progressions(results) -> List[ActualProgressionInContext]:

    progressions = []
    for result in results:
        progressions.append(ActualProgressionInContext(result["story_uid"],
                                        result["story_title"],
                                        result["actual_progression"],
                                        result["principal_uid"],
                                        result["principal_name"],
                                        result["campaign_name"]))
    return progressions


def get_all_plans(results) -> List[ActualPlanInContext]:

    plans = []
    for result in results:
        plans.append(ActualPlanInContext(result["story_uid"],
                                        result["story_title"],
                                        result["actual_plan"],
                                        result["principal_uid"],
                                        result["principal_name"],
                                        result["campaign_name"]))
    return plans


def get_non_default_ownerships(results) -> List[OwnershipInfo]:

    ownerships = []
    for result in results:
        ownerships.append(OwnershipInfo(result["actor_name"],
                                        result["actor_uid"],
                                        result["owner_name"]))
    return ownerships


def get_stories_with_campaigns(results) -> List[StoryDataWithCampaignLink]:

    sessions_with_campaigns = []
    for result in results:
        sessions_with_campaigns.append(StoryDataWithCampaignLink(result["story_uid"],
                                                                 result["story_title"],
                                                                 result["story_summary"],
                                                                 __parse_date_from_str(result["story_start_date"]),
                                                                 __parse_date_from_str(result["story_end_date"]),
                                                                 result["campaign_uid"]))

    return sessions_with_campaigns


def get_all_campaigns(results) -> List[CampaignInfo]:
    campaigns = []
    for result in results:
        campaigns.append(CampaignInfo(result["campaign_uid"],
                                      result["campaign_title"],
                                      result["prev_campaign_uid"],
                                      result["prev_campaign_title"],
                                      result["campaign_description"]))

    return campaigns


def get_items_relations_with_intensity(results) -> List[ItemsRelationWithIntensity]:
    relations = []
    for result in results:
        relations.append(ItemsRelationWithIntensity(result["item1"],
                                                    result["item2"],
                                                    result["intensity"]))

    return relations


def get_all_skills(results) -> List[Skill]:
    skills = []
    for result in results:
        skills.append(Skill(result["item"], []))

    return skills


def get_all_magics(results) -> List[Magic]:
    magics = []
    for result in results:
        magics.append(Magic(result["item"], []))

    return magics


def get_all_motivations(results) -> List[Motivation]:
    magics = []
    for result in results:
        magics.append(Motivation(result["item"], []))

    return magics


def get_all_aspects_with_items(results) -> List[AspectWithItem]:
    aspects = []
    for result in results:
        aspects.append(AspectWithItem(result["aspect"], result["item"]))

    return aspects


def __parse_date_from_str(datestr: str) -> date:
    parts = datestr.split("-")
    return date(year=int(parts[0].strip()),month=int(parts[1].strip()),day=int(parts[2].strip()), )
