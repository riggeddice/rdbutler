import os
from typing import List

from jinja2 import FileSystemLoader, Environment

from src.application.entities.actors.actor import Actor
from src.application.entities.campaigns.campaign import Campaign, KEY_campaign_position_in_index
from src.application.entities.config import default_owner
from src.application.entities.factions.faction import Faction
from src.application.entities.merits.create import create_merits_from_story
from src.application.entities.plans.create import create_plans_from_story
from src.application.entities.progressions.create import create_progressions_from_story
from src.application.entities.stories.story import Story, KEY_story_absolute_position, KEY_story_position_in_campaign, \
    KEY_story_start_date, KEY_story_end_date
from src.application.usecases.retrieve_from_entities.retrieve_uid import retrieve_uid_of_actor__for_db, \
    retrieve_uid_of_faction__for_db
from src.rpg_domain.invasion.entities.magics.create import create_magics_from_actor_text
from src.rpg_domain.invasion.entities.motivations.create import create_motivations_from_actor_text
from src.rpg_domain.invasion.entities.skills.create import create_skills_from_actor_text
from src.technical.sanitize import sanitize_input_for_db

loader = FileSystemLoader(os.path.dirname(__file__))
env = Environment(loader=loader, line_statement_prefix=r'//', keep_trailing_newline=True)
env.globals['sanitize'] = sanitize_input_for_db
env.globals['retrieve_uid_of_faction'] = retrieve_uid_of_faction__for_db
env.globals['retrieve_uid_of_actor'] = retrieve_uid_of_actor__for_db
env.globals['KEY_campaign_position_in_index'] = KEY_campaign_position_in_index
env.globals['KEY_story_start_date'] = KEY_story_start_date
env.globals['KEY_story_end_date'] = KEY_story_end_date
env.globals['KEY_story_absolute_position'] = KEY_story_absolute_position
env.globals['KEY_story_position_in_campaign'] = KEY_story_position_in_campaign


def render_template(template_name, **kwargs):
    template = env.get_template('/'.join(['templates', '{}.cypher.template'.format(template_name)]))
    result = template.render(**kwargs)
    return result


def create_actor_ecosystem(actors: List[Actor]) -> str:
    assert actors
    for actor in actors:
        actor.skills = create_skills_from_actor_text(actor.body, actor.mechanics_version)
        actor.magics = create_magics_from_actor_text(actor.body, actor.mechanics_version)
        actor.motivations = create_motivations_from_actor_text(actor.body, actor.mechanics_version)
    return render_template('create_actor_ecosystem', actors=actors)


def create_campaign_ecosystem(campaigns: List[Campaign]) -> str:
    assert campaigns
    return render_template('create_campaign_ecosystem', campaigns=campaigns)


def create_factions_ecosystem(factions: List[Faction]) -> str:
    assert factions
    return render_template('create_factions_ecosystem', factions=factions)


def create_story_ecosystem(story: Story) -> str:
    assert story
    assert story.campaign
    return render_template('create_story_ecosystem', story=story, merits=create_merits_from_story(story),
                           progressions=create_progressions_from_story(story), plans=create_plans_from_story(story))


def link_campaign_with_predecessor(campaign: Campaign) -> str:
    assert campaign
    assert campaign.prev_campaign_uid
    return render_template('link_campaign_with_predecessor', campaign=campaign)


def link_single_story_node_with_campaign_predecessor(story: Story) -> str:
    assert story
    assert story.previous_story_id_campaign
    return render_template('link_single_story_node_with_campaign_predecessor', story=story)


def link_single_story_node_with_chronological_predecessors(story: Story) -> str:
    assert story
    assert story.previous_story_ids_chrono
    return render_template('link_single_story_node_with_chronological_predecessors', story=story)


def link_unlinked_actors_with_default_player() -> str:
    return render_template('link_unlinked_actors_with_default_player', default_owner=default_owner())


def link_actors_with_factions(surname: str, faction: str) -> str:
    return render_template('link_actors_with_factions', surname=surname, faction=faction)


def update_missing_faction_uid(faction: Faction) -> str:
    return render_template('update_missing_faction_uid', faction=faction)


def update_missing_actor_uid(actor: Actor) -> str:
    return render_template('update_missing_actor_uid', actor=actor)


def detach_delete_all_nodes() -> str:
    return render_template('detach_delete_all_nodes')
