class ActorNameWithIntensity:
    def __init__(self, actor_name, times_met):
        self.actor_name = actor_name
        self.times_met = times_met


class ActorWithIntensity:
    def __init__(self, actor, times_met):
        self.actor = actor
        self.times_met = times_met


class StoryUidWithTitle:
    def __init__(self, uid, title):
        self.uid = uid
        self.title = title


class ActualMeritInContext:
    def __init__(self, story_uid, story_title, actual_merit, campaign):
        self.story_uid = story_uid
        self.story_title = story_title
        self.actual_merit = actual_merit
        self.campaign = campaign


class ActualProgressionInContext:
    def __init__(self, story_uid, story_title, actual_progression, principal_uid, principal_name, campaign_name):
        self.story_uid = story_uid
        self.story_title = story_title
        self.actual_progression = actual_progression
        self.principal_uid = principal_uid
        self.principal_name = principal_name
        self.campaign_name = campaign_name


class ActualPlanInContext:
    def __init__(self, story_uid, story_title, actual_plan, principal_uid, principal_name, campaign_name):
        self.story_uid = story_uid
        self.story_title = story_title
        self.actual_plan = actual_plan
        self.principal_uid = principal_uid
        self.principal_name = principal_name
        self.campaign_name = campaign_name


class MeetingOf2Actors:
    def __init__(self, actor_1, actor_1_uid, actor_2, actor_2_uid, story_uid, story_title, story_start_date, campaign):
        self.actor_1 = actor_1
        self.actor_1_uid = actor_1_uid
        self.actor_2 = actor_2
        self.actor_2_uid = actor_2_uid
        self.story_uid = story_uid
        self.story_title = story_title
        self.story_start_date = story_start_date
        self.campaign = campaign


class ExtendedMeritInfo:
    def __init__(self, actor_name, story_uid, story_title, story_start_date,
                 story_end_date, actual_merit, campaign_uid, campaign_name):
        self.actor_name = actor_name
        self.story_uid = story_uid
        self.story_title = story_title
        self.story_start_date = story_start_date
        self.story_end_date = story_end_date
        self.actual_merit = actual_merit
        self.campaign_uid = campaign_uid
        self.campaign_name = campaign_name


class ActorToFactionLink:
    def __init__(self, actor_name, actor_uid, faction_name):
        self.actor_name = actor_name
        self.actor_uid = actor_uid
        self.faction_name = faction_name


class OwnershipInfo:
    def __init__(self, actor_name, actor_uid, owner_name):
        self.actor_name = actor_name
        self.actor_uid = actor_uid
        self.owner_name = owner_name


class CampaignInfo:
    def __init__(self, campaign_uid, campaign_title, prev_campaign_uid, prev_campaign_title, campaign_description):
        self.campaign_uid = campaign_uid
        self.campaign_title = campaign_title
        self.prev_campaign_uid = prev_campaign_uid
        self.prev_campaign_title = prev_campaign_title
        self.campaign_description = campaign_description


class StoryDataWithCampaignLink:
    def __init__(self, story_uid, story_title, story_summary, story_start_date, story_end_date, campaign_uid):
        self.story_uid = story_uid
        self.story_title = story_title
        self.story_summary = story_summary
        self.story_start_date = story_start_date
        self.story_end_date = story_end_date
        self.campaign_uid = campaign_uid


class AspectWithItem:
    def __init__(self, aspect, item):
        self.aspect = aspect
        self.item = item


class ItemsRelationWithIntensity:
    def __init__(self, item1, item2, intensity):
        self.item1 = item1
        self.item2 = item2
        self.intensity = intensity
