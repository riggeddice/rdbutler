from src.application.entities.config import default_owner


def get__all_actors() -> str:

    return """match (a:Actor)
    return a as actor
    order by a.name"""


def get__all_factions() -> str:

    return """match (f:Faction)
    return f as faction
    order by f.name"""


def get__all_factions_without_uid() -> str:

    return"""match (f:Faction) where NOT EXISTS(f.uid) 
    return f as faction"""


def get__all_actors_without_uid() -> str:

    return """match (a:Actor) where NOT EXISTS(a.uid) 
    return a as actor"""


def get__all_faction_links() -> str:

    return """match (a:Actor) -[:IS_IN]->(f:Faction) 
    return a.name as actor_name, a.uid as actor_uid, f.name as faction_name"""


def get__all_actor_names__met_by_named_actor(name: str) -> str:

    query = """match (a1:Actor{name:'%s'})-[d1:DID]->(m1:Merit)-[:HAPPENED_DURING]->(s:Story)<-[:HAPPENED_DURING]-(m2:Merit)<-[:DID]-(a2:Actor)
    return a2.name as name, count(m2) as times_met
    order by count(m2) DESC"""
    return query % name


def get__all_actor_ownerships() -> str:

    return """match (a1:Actor{name:'%s'})-[d1:DID]->(m1:Merit)-[:HAPPENED_DURING]->(s:Story)<-[:HAPPENED_DURING]-(m2:Merit)<-[:DID]-(a2:Actor)
    return a2.name as name, count(m2) as times_met
    order by count(m2) DESC"""


def get__all_story_uids__where_two_actors_met(name1: str, name2: str) -> str:

    query = """match (a1:Actor{name:'%s'})-[d1:DID]->(m1:Merit)-[:HAPPENED_DURING]->(s:Story)<-[:HAPPENED_DURING]-(m2:Merit)<-[:DID]-(a2:Actor{name:'%s'})
    return s.uid as story_uid, s.title as story_title
    order by s.absolute_position DESC"""
    return query % (name1, name2)


def get__all_merits__for_named_actor(name: str) -> str:

    query = """match (a:Actor{name:'%s'})-[d1:DID]->(m:Merit)-[:HAPPENED_DURING]->(s:Story)-[:BELONGS_TO]->(c:Campaign)
    return m.story_id as story_id, m.actual_merit as merit, s.title as story_title, c.name as campaign
    order by m.story_id DESC"""
    return query % name


def get__all_merits() -> str:

    return """match (a:Actor)-[d1:DID]->(m:Merit)-[:HAPPENED_DURING]->(s:Story)-[:BELONGS_TO]->(c:Campaign)
    return m.story_id as story_id, m.actual_merit as merit, s.title as story_title, s.start_date as story_start_date, s.end_date as story_end_date, c.uid as campaign_uid, c.title as campaign_name, a.name as actor_name
    order by s.absolute_position DESC"""


def get__all_meetings() -> str:
    return """match (a1:Actor)-[d1:DID]->(m1:Merit)-[:HAPPENED_DURING]->(s:Story)<-[:HAPPENED_DURING]-(m2:Merit)<-[:DID]-(a2:Actor), (s)-[:BELONGS_TO]->(c:Campaign)
    return a1.name as actor1, a1.uid as actor1_uid, a2.name as actor2, a2.uid as actor2_uid, s.title as story_title, s.uid as story_uid, s.start_date as story_start_date, c.title as campaign
    order by a1.name, s.absolute_position DESC"""


def get__non_default_ownerships() -> str:
    return """match (a:Actor), (p:Player) where (a)<-[:OWNS]-(p) and not p.name = '%s'
return a.name as actor_name, a.uid as actor_uid, p.name as owner_name""" % default_owner()


def get__all_stories_with_campaigns() -> str:
    return """match (su:Summary)<-[:SUMMARIZED_AS]-(s:Story)-[:BELONGS_TO]->(c:Campaign)
return s.uid as story_uid, s.title as story_title, c.uid as campaign_uid, s.start_date as story_start_date, s.end_date as story_end_date, su.body as story_summary
order by s.absolute_position"""


def get__all_campaigns()-> str:
    return """match (c:Campaign)-[:CAMPAIGN_IS_AFTER]->(c2)
return c.uid as campaign_uid, c.title as campaign_title, c2.uid as prev_campaign_uid, c2.title as prev_campaign_title, c.description as campaign_description
order by c.index_position"""


def get__all_progressions() ->str:
    return """match (a)-[:GAINED]->(p:Progression)<-[:RESULTED_IN]-(s:Story)-[:BELONGS_TO]-(c:Campaign)
return a.uid as principal_uid, a.name as principal_name, p.actual_progression as actual_progression, s.uid as story_uid, s.title as story_title, c.title as campaign_name order by  s.start_date desc, s.title, actual_progression, principal_uid"""


def get__all_plans() ->str:
    return """match (a)-[:HAS_PLAN]->(p:Plan)<-[:CAUSED_PLAN]-(s:Story)-[:BELONGS_TO]-(c:Campaign)
return a.uid as principal_uid, a.name as principal_name, p.actual_plan as actual_plan, s.uid as story_uid, s.title as story_title, c.title as campaign_name order by  s.start_date desc, s.title, actual_plan, principal_uid desc"""


def get__all_item_names(item_type: str)-> str:
    return """match (i:%s) return i.name as item""" % item_type


def get__all_aspects_with_item(item_type: str)-> str:
    return """match (i:%s)-[r:HAS_ASPECT]-(a:Aspect) return i.name as item, a.name as aspect""" % item_type


def get__item_relations_with_intensity(item1_type: str, item2_type: str)-> str:
    return """match (i1:%s)-[]-(a:Actor)-[]-(i2:%s) return i1.name as item1, i2.name as item2, count(a) as intensity order by intensity desc""" % (item1_type, item2_type)
