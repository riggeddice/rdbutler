import datetime

from src.application.dataclasses.actor_autogen_features import create_actor_autogen_feature
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_campaigns, read_all_factions, read_all_actor_autogens
from src.external.jekyll.read_entities_from_files import read_all_stories
from src.external.jekyll.read_entities_from_files import read_all_actors
from src.external.neo4j.create import create_neo4jdb
from src.application.entities.stories.create import create_stories
from src.application.entities.actors.create import create_actors
from src.application.entities.factions.create import create_factions
from src.application.entities.campaigns.create import create_campaigns
from src.application.usecases.populate.populate_stories import populate_stories_with_campaign_data
from src.application.usecases.populate.populate_time import populate_time_in_stories_by_chrono_order
from src.rpg_domain.invasion.invasion_config import starting_date


def import_jekyll_files_into_neo4j_opt():
    """
    This function is a high level command which should take the RD master folder (Jekyll one) and then activate
      all the parsers and readers, to finally import the data into the database. In short, this is a 'console UI'
      of sorts. A script to be ran.
    :return:
    """

    # Prepare data to be loaded
    print("Creation of entities started: " + str(datetime.datetime.now()))
    actors = create_actors(read_all_actors(config))
    campaigns = create_campaigns(read_all_campaigns(config))
    stories = create_stories(read_all_stories(config))
    factions = create_factions(read_all_factions(config))
    populate_time_in_stories_by_chrono_order(stories, starting_date)
    stories, campaigns = populate_stories_with_campaign_data(stories, campaigns)

    # Init
    db = create_neo4jdb()
    db.clear_database()
    print("Neo4j cleared: " + str(datetime.datetime.now()))

    # PHASE 1
    # Create Actors and dependents
    db.create_factions_ecosystem(factions)
    print("Factions created: " + str(datetime.datetime.now()))
    db.create_actors_ecosystem(actors)
    print("Actors created: " + str(datetime.datetime.now()))

    # PHASE 2
    # Create Stories and dependents - we have Actor ecosystem.
    db.create_campaigns_ecosystem(campaigns)
    db.create_stories_ecosystem(stories)
    print("Stories, campaigns created: " + str(datetime.datetime.now()))

    # PHASE 3
    # Perform further actions - we have Actor and Story ecosystems
    db.link_to_predecessors(stories, campaigns)
    print("Linked stories with predecessors: " + str(datetime.datetime.now()))
    db.link_unassigned_actors_to_default_player()
    print("Linked unlinked actors with default player: " + str(datetime.datetime.now()))
    db.link_unlinked_actors()
    print("Linked bloodline actors: " + str(datetime.datetime.now()))

    # PHASE 4
    # Populate missing key data in all entities
    print("Adding missing faction uids: " + str(datetime.datetime.now()))
    db.set_missing_faction_uids()
    print("Added missing faction uids: " + str(datetime.datetime.now()))
    print("Adding missing actor uids: " + str(datetime.datetime.now()))
    db.set_missing_actor_uids()
    print("Added missing actor uids: " + str(datetime.datetime.now()))


if __name__ == "__main__":
    import_jekyll_files_into_neo4j_opt()
