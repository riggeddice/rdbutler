import time
from datetime import datetime

from src.application.usecases.randomize.profile import add_aspects_for_item, \
    extend_list_with_random_items
from src.external.neo4j.create import create_neo4jdb
from src.rpg_domain.invasion.entities.skills.skill import Skill
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.technical.file_ops import store_text_in_outputs


def extend_profile():

    print("Starting profile extension: " + str(datetime.now()))

    # PHASE 1: Configure the ecosystem

    db = create_neo4jdb()

    all_skills = db.get_all_skills()
    all_skill_aspects = db.get_all_aspects_with_skills()
    all_magics = db.get_all_magics()
    all_magic_aspects = db.get_all_aspects_with_magics()
    all_motivations = db.get_all_motivations()
    all_motivation_aspects = db.get_all_aspects_with_motivations()

    # PHASE 2: User UI:
    # Define desired skills (with aspects if you want)
    desired_skills = []
    desired_skill = Skill("terminus", [Aspect("łowca magów")])
    desired_skill2 = Skill("polityk", [])
    desired_skills.append(desired_skill)
    desired_skills.append(desired_skill2)

    # Define desired magics (with aspects if you want)
    desired_magics = []

    # Define desired motivations (with aspects if you want)
    desired_motivations = []

    # PHASE 3: Command operations:
    # Process skills
    skills = extend_list_with_random_items(desired_skills, all_skills, 5)

    for skill in skills:
        add_aspects_for_item(skill, all_skill_aspects, 5)

    # Process magics
    magics = extend_list_with_random_items(desired_magics, all_magics, 3)

    for magic in magics:
        add_aspects_for_item(magic, all_magic_aspects, 5)

    # Process motivations
    motivations = extend_list_with_random_items(desired_motivations, all_motivations, 5)
    for motivation in motivations:
        add_aspects_for_item(motivation, all_motivation_aspects, 5)

    profile_text = ""
    profile_text += "\nMotivations:\n"
    for motivation in motivations:
        profile_text += motivation.__str__() + "\n"

    profile_text += "\nSkills:\n"
    for skill in skills:
        profile_text += skill.__str__() + "\n"

    profile_text += "\nMagics:\n"
    for magic in magics:
        profile_text += magic.__str__() + "\n"

    # Store single profile on HDD
    store_text_in_outputs(entity_name="extended_profile", to_store=profile_text)

    print("\nFinished profile extension generation: " + str(datetime.now()))


if __name__ == "__main__":
    extend_profile()