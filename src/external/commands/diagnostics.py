from src.application.usecases.diagnose.merit_diagnostics import merit_diagnostics
from src.application.usecases.diagnose.story_diagnostics import story_diagnostics
from src.application.usecases.diagnose.actor_diagnostics import actor_diagnostics


def full_diagnostics():
    story_diagnostics()
    actor_diagnostics()
    merit_diagnostics()
    return

full_diagnostics()
