from src.external.commands.generate_all_from_db import generate_all_from_db
from src.external.commands.import_to_db import import_jekyll_files_into_neo4j_opt

from datetime import datetime

start = datetime.now()
import_jekyll_files_into_neo4j_opt()
generate_all_from_db()
print('Total time taken: {}s'.format((datetime.now() - start).total_seconds()))
