from src.application.entities.actors.actor import Actor
from src.external.neo4j.create import create_neo4jdb


def get_contact_names_with_intensity():

    # Given data
    db = create_neo4jdb()

    # When
    name_list = db.get_contact_names_with_intensity("Paulina Tarczyńska")

    # Then
    assert (len(name_list) > 0)
    assert (isinstance(name_list[0].actor_name, str))
    assert (isinstance(name_list[0].times_met, int))


get_contact_names_with_intensity()

