import datetime

from src.application.usecases.generate.generate_profile_index import generate_profile_index
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_rd_profiles_folder
from src.technical.file_ops import save_file


def generate_profile_index_from_db():

    db = create_neo4jdb()
    all_actors = db.get_all_actors()
    all_faction_links = db.get_all_actor_to_faction_links()
    non_default_ownerships = db.get_non_default_ownerships()

    print("Generating character master list " + str(datetime.datetime.now()))

    index_text = generate_profile_index(all_actors, all_faction_links, non_default_ownerships)

    index_path = place_file_in_rd_profiles_folder("index.md")
    save_file(index_text, index_path)


if __name__ == "__main__":
    generate_profile_index_from_db()
