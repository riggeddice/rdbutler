import datetime

from src.application.usecases.generate.generate_campaign_sheet import generate_campaign_sheet
from src.application.usecases.retrieve_from_entities.retrieve_uid import retrieve_uid_of_campaign_info__for_file
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_rd_stories_folder
from src.technical.file_ops import save_file


def generate_campaign_sheets_from_db():

    db = create_neo4jdb()
    campaign_infos = db.get_all_campaign_infos()
    story_data_with_campaigns = db.get_all_story_data_with_campaign_links()
    all_progressions = db.get_all_progression_infos()
    all_plans = db.get_all_plan_infos()
    all_actors = db.get_all_actors()

    print("Generating campaign sheets " + str(datetime.datetime.now()))

    for campaign_info in campaign_infos:
        campaign_text = generate_campaign_sheet(campaign_info, story_data_with_campaigns,
                                                all_progressions,all_plans, all_actors)

        # save campaign file
        file_name = retrieve_uid_of_campaign_info__for_file(campaign_info) + ".md"
        index_path = place_file_in_rd_stories_folder(file_name)
        save_file(campaign_text, index_path)

    print("Generating campaign sheets done " + str(datetime.datetime.now()))


if __name__ == "__main__":
    generate_campaign_sheets_from_db()
