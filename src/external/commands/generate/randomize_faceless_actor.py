from datetime import datetime
from textwrap import dedent

from src.application.dataclasses.actor_autogen_features import create_actor_autogen_feature, ActorAutogenFeature
from src.application.usecases.randomize.faceless_actor import pick_category, pick_name, pick_professions, pick_emotion, \
    pick_traits
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_actor_autogens
from src.technical.file_ops import store_text_in_outputs


def randomize_faceless_npc():

    actor_autogen_features = create_actor_autogen_feature(read_all_actor_autogens(config), config)
    actors = ""
    for i in range(1, 20):
        faceless_actor = randomize_faceless_actor(actor_autogen_features)
        actors = actors + faceless_actor

    store_text_in_outputs("faceless_actor", actors)

    print("\nFinished profile extension generation: " + str(datetime.now()))


def randomize_faceless_actor(aaf: ActorAutogenFeature):
    category = pick_category(aaf)
    name = pick_name(aaf, category)
    professions = pick_professions(aaf)
    initial_emotion = pick_emotion(aaf)
    traits = pick_traits(aaf)

    faceless = format_faceless_actor(category, name, professions, initial_emotion, traits)
    return faceless


def format_faceless_actor(category, name, professions, initial_emotion, traits) -> str:
    return dedent("""
    Name:                   {name}
    Category:               {gender}
    Profession candidates:  {prof_1}, {prof_2}
    Initial emotion:        {emotion}
    Main trait:             {trait_1}
    Secondary trait:        {trait_2}
    """.format(
        name=name, gender=category, prof_1=professions[0], prof_2=professions[1], emotion=initial_emotion,
        trait_1=traits[0], trait_2=traits[1]
    ))

if __name__ == "__main__":
    randomize_faceless_npc()
