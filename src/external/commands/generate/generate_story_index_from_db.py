import datetime

from src.application.usecases.generate.generate_story_index import generate_story_index
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_rd_stories_folder
from src.technical.file_ops import save_file


def generate_story_index_from_db():

    db = create_neo4jdb()
    all_campaigns = db.get_all_campaign_infos()
    all_stories_with_camps = db.get_all_story_data_with_campaign_links()

    print("Generating story index " + str(datetime.datetime.now()))

    index_text = generate_story_index(all_campaigns, all_stories_with_camps)

    file_name = "index.md"
    index_path = place_file_in_rd_stories_folder(file_name)
    save_file(index_text, index_path)

    print("Generating story index done " + str(datetime.datetime.now()))


if __name__ == "__main__":
    generate_story_index_from_db()
