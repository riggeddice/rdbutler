import datetime

from src.application.usecases.generate.generate_faction_sheet import generate_faction_sheet
from src.application.usecases.retrieve_from_entities.retrieve_uid import retrieve_uid_of_faction__for_file
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_rd_factions_folder
from src.technical.file_ops import save_file


def generate_factions_from_db():

    db = create_neo4jdb()
    print("Starting faction sheet generation: " + str(datetime.datetime.now()))

    all_factions_in_db = db.get_all_factions()
    all_progressions = db.get_all_progression_infos()
    all_plans = db.get_all_plan_infos()

    for current_faction in all_factions_in_db:
        faction_text = generate_faction_sheet(current_faction, all_progressions, all_plans)

        filename = retrieve_uid_of_faction__for_file(current_faction) + ".md"
        path = place_file_in_rd_factions_folder(filename)
        save_file(faction_text, path)

    print("Finished faction sheet generation: " + str(datetime.datetime.now()))


if __name__ == "__main__":
    generate_factions_from_db()
