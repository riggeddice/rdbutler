import datetime

from src.application.usecases.generate.generate_profile_sheet import generate_profile_sheet
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_rd_profiles_folder
from src.technical.file_ops import save_file


def generate_profiles_from_db():

    db = create_neo4jdb()
    print("Starting character sheet generation: " + str(datetime.datetime.now()))

    # Read from DB
    all_actors_in_db = db.get_all_actors()
    all_meetings = db.get_all_meetings()
    all_merit_infos = db.get_all_merit_infos()
    all_actor_to_faction_links = db.get_all_actor_to_faction_links()
    non_default_ownerships = db.get_non_default_ownerships()
    all_progressions = db.get_all_progression_infos()
    all_plans = db.get_all_plan_infos()

    # For each actor build a profile text:
    for current_actor in all_actors_in_db:
        profile_text = generate_profile_sheet(all_actor_to_faction_links, all_meetings, all_merit_infos, current_actor,
                                              non_default_ownerships, all_progressions, all_plans)

        # Store single profile on HDD
        filename = current_actor.uid + ".md"
        path = place_file_in_rd_profiles_folder(filename)
        save_file(profile_text, path)

    print("Finished character sheet generation: " + str(datetime.datetime.now()))


if __name__ == "__main__":
    generate_profiles_from_db()
