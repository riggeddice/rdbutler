from src.external.commands.generate.generate_campaign_sheets_from_db import generate_campaign_sheets_from_db
from src.external.commands.generate.generate_faction_sheets_from_db import generate_factions_from_db
from src.external.commands.generate.generate_profile_index_from_db import generate_profile_index_from_db
from src.external.commands.generate.generate_profile_sheets_from_db import generate_profiles_from_db
from src.external.commands.generate.generate_story_index_from_db import generate_story_index_from_db


def generate_all_from_db():
    generate_profile_index_from_db()
    generate_profiles_from_db()
    generate_story_index_from_db()
    generate_campaign_sheets_from_db()
    generate_factions_from_db()


if __name__ == "__main__":
    generate_all_from_db()
