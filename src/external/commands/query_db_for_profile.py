from src.external.neo4j.create import create_neo4jdb


def query_db_for_profile():

    db = create_neo4jdb()

    profile_name = "Paulina Tarczyńska"
    second_profile_name = "Maria Newa"

    her_contacts = db.get_contact_names_with_intensity(profile_name)
    for contact in her_contacts:
        print(str(contact.times_met) + ": " + contact.actor_name)

    print("======================================================================")

    meetings = db.get_stories_when_actors_met(profile_name, second_profile_name)
    for meeting in meetings:
        print(meeting.uid + ": " + meeting.title)
    print("======================================================================")

    deeds = db.get_merits_in_context(profile_name)
    for deed in deeds:
        print(deed.story_uid + ": " + deed.actual_merit)


query_db_for_profile()
