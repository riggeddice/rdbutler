from datetime import datetime
import time

from src.application.usecases.randomize.profile import generate_items_with_aspects
from src.external.neo4j.create import create_neo4jdb
from src.technical.file_ops import place_file_in_outputs_folder, save_file


def generate_random_profile_from_db():

    db = create_neo4jdb()
    print("Starting random profile generation: " + str(datetime.now()))

    all_skills = db.get_all_skills()
    all_skill_aspects = db.get_all_aspects_with_skills()
    all_magics = db.get_all_magics()
    all_magic_aspects = db.get_all_aspects_with_magics()
    all_motivations = db.get_all_motivations()
    all_motivation_aspects = db.get_all_aspects_with_motivations()

    skills = generate_items_with_aspects(all_skills, 4, all_skill_aspects, 3)
    magics = generate_items_with_aspects(all_magics, 3, all_magic_aspects, 5)
    motivations = generate_items_with_aspects(all_motivations, 5, all_motivation_aspects, 5)

    profile_text = ""
    profile_text += "\nSelected random motivations:\n"
    for motivation in motivations:
        profile_text += motivation.__str__() + "\n"

    profile_text += "\nSelected random skills:\n"
    for skill in skills:
        profile_text += skill.__str__() + "\n"

    profile_text += "\nSelected random magics:\n"
    for magic in magics:
        profile_text += magic.__str__() + "\n"

    # Store single profile on HDD
    filename = "profile" + str(time.time()) + ".txt"
    path = place_file_in_outputs_folder(filename)
    save_file(profile_text, path)

    print("\nFinished random profile generation: " + str(datetime.now()))


if __name__ == "__main__":
    generate_random_profile_from_db()