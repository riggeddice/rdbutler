
def analize_token_mechanics():

    # Levels:

    lv_zero = 0
    lv_competent = 4
    lv_good = 8
    lv_authority = 12

    red_reduction_step = 4

    # Trivial:   trivial = [80, 20, 0]
    # 3G, 2Y, 2R
    # Every 4 by characters: -1R

    print("===============================")
    print("Trivial: [80, 20, 0]")

    base_g = 3

    # Base
    tg = base_g + lv_zero
    ty = 2
    tr = 2 - int(base_g / red_reduction_step)
    print("Zero (0) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Competent: +4
    tg = base_g + lv_competent
    ty = 2
    tr = 1
    print("Competent (4) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Very good: +8
    tg = base_g + lv_good
    ty = 2
    tr = 0
    print("Expert (8) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Authority: +12
    tg = base_g + lv_authority
    ty = 2
    tr = 0
    print("Authority (12) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Easy = [50, 30, 20]
    # 2G, 3Y, 3R
    # Every 4 by characters: -1R

    print("===============================")
    print("Easy = [50, 30, 20]")

    base_g = 2

    # Base
    tg = base_g + lv_zero
    ty = 3
    tr = 4
    print("Zero (0) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Competent: +4
    tg = base_g + lv_competent
    ty = 3
    tr = 3
    print("Competent (4) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Very good: +8
    tg = base_g + lv_good
    ty = 3
    tr = 2
    print("Expert (8) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Authority: +12
    tg = base_g + lv_authority
    ty = 3
    tr = 1
    print("Authority (12) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Typical = [30, 40, 30]
    # 0G, 2Y, 5R
    # Every 4 by characters: -1R

    print("===============================")
    print("Typical = [30, 40, 30]")

    base_g = 0

    # Base
    tg = base_g + lv_zero
    ty = 2
    tr = 5
    print("Zero (0) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Competent: +4
    tg = base_g + lv_competent
    ty = 2
    tr = 4
    print("Competent (4) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Very good: +8
    tg = base_g + lv_good
    ty = 2
    tr = 3
    print("Expert (8) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Authority: +12
    tg = base_g + lv_authority
    ty = 2
    tr = 2
    print("Authority (12) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # hard = [20, 30, 50]
    # 0G, 4Y, 8R
    # Every 4 by characters: -1R

    print("===============================")
    print("Hard = [20, 30, 50]")

    base_g = -2

    # Base
    tg = 0
    ty = 4
    tr = 8
    print("Zero (0) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Competent: +4
    tg = base_g + lv_competent
    ty = 4
    tr = 7
    print("Competent (4) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Very good: +8
    tg = base_g + lv_good
    ty = 4
    tr = 6
    print("Expert (8) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Authority: +12
    tg = base_g + lv_authority
    ty = 4
    tr = 5
    print("Authority (12) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Heroic = [0, 20, 80]
    # -3G, 4Y, 9R
    # Every 4 by characters: -1R

    print("===============================")
    print("Heroic = [0, 20, 80]")

    base_g = -3

    # Base
    tg = 0
    ty = 4
    tr = 9
    print("Zero (0) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Competent: +4
    tg = base_g + lv_competent
    ty = 4
    tr = 8
    print("Competent (4) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Very good: +8
    tg = base_g + lv_good
    ty = 4
    tr = 7
    print("Expert (8) level: " + str(analyse_mechanics_2(tg, ty, tr)))

    # Authority: +12
    tg = base_g + lv_authority
    ty = 4
    tr = 6
    print("Authority (12) level: " + str(analyse_mechanics_2(tg, ty, tr)))


def analyse_mechanics_2(tg, tr, ty):
    successes = [[1, 1], [1, 0], [0, 1]]
    conflicted = [[0, 0], [1, -1], [-1, 1]]
    failures = [[-1, -1], [-1, 0], [0, -1]]

    p_s = events_2(successes, tg, tr, ty)
    p_c = events_2(conflicted, tg, tr, ty)
    p_f = events_2(failures, tg, tr, ty)

    probabilizer = {"Success": p_s, "Conflicted": p_c, "Failure": p_f}

    return probabilizer


def events_2(sequences, green, yellow, red):
    p_event = 0
    for sequence in sequences:

        pool = green, yellow, red
        p_prev_event = 1
        for color in sequence:
            picked = pick_1(color, *pool)
            pool = picked[1:]
            p_prev_event = p_prev_event * picked[0]

        p_event = p_event + p_prev_event

    return p_event


def pick_1(color, green, yellow, red):
    if color == 1:
        return green / (green + yellow + red), green - 1, yellow, red
    if color == 0:
        return yellow / (green + yellow + red), green, yellow - 1, red
    if color == -1:
        return red / (green + yellow + red), green, yellow, red - 1
    raise ValueError("Something went wrong")


analize_token_mechanics()
