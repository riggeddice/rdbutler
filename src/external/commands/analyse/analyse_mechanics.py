from operator import add
import itertools
from textwrap import dedent

from src.technical.file_ops import store_text_in_outputs


def analize_token_mechanics():

    # Mechanics setup:
    # ================
    # Core numbers
    no_influence = [0, 0, 0, "no-mods "]
    scene_bonus_max = [4, 0, 0, "scene "]
    resource_influence = [3, 0, 0, "resources "]
    magic_influence = [3, 0, 0, "magic "]
    weakness_influence = [0, 0, 2, "weakness "]

    # 2 of other params
    weak_prof = [2, 0, 0, "weak-profile "]

    # One skill, 2 of other params
    competent_prof = [6, 0, 0, "competent-profile "]

    # Everything possible applies
    maxed_prof = [9, 0, 0, "maxed-profile "]

    # Red Reduction Step
    red_reduction_step = 3

    # Conflict prototypes
    easy_con = [2, 3, 4, "easy-conflict "]  # aim [60, 30, 10] competent
    typical_con = [0, 3, 6, "typical-conflict "]  # aim [50, 30, 20] competent
    hard_con = [-2, 3, 8, "hard-conflict "]  # aim [20, 30, 50] competent
    heroic_con = [-4, 5, 20, "heroic-conflict "]  # aim [ 0, 20, 80] competent

    # =============
    # Calculations:

    scene_resource = list(map(add, scene_bonus_max, resource_influence))
    resource_magic = list(map(add, resource_influence, magic_influence))
    scene_resource_magic = list(map(add, scene_resource, magic_influence))
    scene_weakness = list(map(add, scene_bonus_max, weakness_influence))
    resource_weakness = list(map(add, resource_influence, weakness_influence))
    scene_resource_weakness = list(map(add, scene_resource, weakness_influence))
    scene_resource_magic_weakness = list(map(add, scene_resource_magic, weakness_influence))

    profiles = [weak_prof, competent_prof, maxed_prof]
    conflicts = [easy_con, typical_con, hard_con, heroic_con]
    modifiers = [
        no_influence, magic_influence, resource_influence, scene_bonus_max, weakness_influence,
        scene_resource, resource_magic, scene_resource_magic,
        scene_weakness, resource_weakness, scene_resource_weakness, scene_resource_magic_weakness
    ]

    permutations = list(itertools.product(profiles, conflicts, modifiers))
    tokenized_conflicts = construct_conflicts(permutations, red_reduction_step)
    ratios = [analyse_mechanics_2(x) for x in tokenized_conflicts]

    # =============
    # Printout:

    output_text = format_text_to_print(ratios, profiles, conflicts, red_reduction_step)
    store_text_in_outputs("mechanics_analysis", output_text)


def format_text_to_print(ratios, profiles, conflicts, red_step):
    text = dedent("""
    ## Dla parametrów

    | Parametr ustawiony    | Wartość |""")

    line_template = "| {param} | {value} |"
    profline = " {g}, {y}, {r} "
    record_len = 21

    for profile in profiles:
        record_padding = record_len - len(profile[3])
        name = profile[3] + " " * record_padding
        formed = profline.format(g=profile[0], y=profile[1], r=profile[2])
        record = line_template.format(param=name, value=formed)
        text = text + "\n" + record

    for conflict in conflicts:
        record_padding = record_len - len(conflict[3])
        name = conflict[3] + " " * record_padding
        formed = profline.format(g=conflict[0], y=conflict[1], r=conflict[2])
        record = line_template.format(param=name, value=formed)
        text = text + "\n" + record

    text = text + "\n| red reduction step    | {val} |".format(val=str(red_step))

    text = text + dedent("""

    ## Wyniki mechaniki tokenowej

    | Profil, konflikt, modyfikatory                                     | P(Sukces) | P(Skonfliktowany) | P(Porażka) |
    |--------------------------------------------------------------------|-----------|-------------------|------------|""")

    record_len = 66
    line_template = "| {record} | {success} | {conflicted} | {failure} |"

    for ratio in ratios:
        record_padding = record_len - len(ratio[3])

        success, conflicted, failure = '{0:.2f}'.format(ratio[0]), '{0:.2f}'.format(ratio[1]), '{0:.2f}'.format(
            ratio[2])
        record = ratio[3] + " " * record_padding

        line = line_template.format(
            record=record,
            success=success, conflicted=conflicted, failure=failure
        )
        text = text + "\n" + line

    return text


def construct_conflicts(permutations, red_reduction_step):
    flattened = []
    for permutation in permutations:
        flat = list(map(add, permutation[0], list(map(add, permutation[1], permutation[2]))))
        flattened.append(flat)

    conflicts = []
    for permutation in flattened:
        conflict = calculate_tokens_for_box(permutation, red_reduction_step)
        conflicts.append(conflict)

    return conflicts


def calculate_tokens_for_box(gyr_vector, red_reduction_step):
    # red reduction
    gyr_vector[2] = gyr_vector[2] - int(gyr_vector[0] / red_reduction_step)

    # zero elimination
    if gyr_vector[0] < 0:
        gyr_vector[0] = 0
    if gyr_vector[2] < 0:
        gyr_vector[2] = 0

    return gyr_vector


def analyse_mechanics_2(conflict):
    tg, ty, tr = conflict[0], conflict[1], conflict[2]  # DISREPANCY: GRY is not GYR

    successes = [[1, 1], [1, 0], [0, 1]]
    conflicted = [[0, 0], [1, -1], [-1, 1]]
    failures = [[-1, -1], [-1, 0], [0, -1]]

    p_s = events_2(successes, tg, ty, tr)
    p_c = events_2(conflicted, tg, ty, tr)
    p_f = events_2(failures, tg, ty, tr)

    return p_s, p_c, p_f, conflict[3]


def events_2(sequences, green, yellow, red):
    p_event = 0
    for sequence in sequences:

        pool = green, yellow, red
        p_prev_event = 1
        for color in sequence:
            picked = pick_1(color, *pool)
            pool = picked[1:]
            p_prev_event = p_prev_event * picked[0]

        p_event = p_event + p_prev_event

    return p_event


def pick_1(color, green, yellow, red):
    if color == 1:
        return green / (green + yellow + red), green - 1, yellow, red
    if color == 0:
        return yellow / (green + yellow + red), green, yellow - 1, red
    if color == -1:
        return red / (green + yellow + red), green, yellow, red - 1
    raise ValueError("Something went wrong")


analize_token_mechanics()
