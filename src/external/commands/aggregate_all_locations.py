from src.application.entities.stories.create import create_stories
from src.application.usecases.aggregate.aggregate_locations import merge_locations_to_common_tree
from src.external.jekyll import config
from src.external.jekyll.read_entities_from_files import read_all_stories
from src.technical.file_ops import save_file, place_file_in_outputs_folder


def aggregate_all_locations():

    read_stories_with_paths = read_all_stories(config)
    stories = create_stories(read_stories_with_paths)

    location_sections = [story.location for story in stories]
    merged_world = merge_locations_to_common_tree(location_sections)

    save_file(merged_world, place_file_in_outputs_folder("invasion_world_map.txt"))


aggregate_all_locations()
