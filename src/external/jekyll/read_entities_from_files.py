import os
import re
from typing import List

from src.technical.file_ops import read_file, EntityTextWithPath


def read_all_actor_autogens(config) -> List[EntityTextWithPath]:

    directory = config.full_path_to_actor_autogen_directory()
    criteria = [
        config.file_matches_actor_names(),
        config.file_matches_professions(),
        config.file_matches_emotions(),
        config.file_matches_traits()
                  ]

    entities_with_paths = []
    for criterion in criteria:
        entities_with_paths.extend(_read_selected_entities_having_path(criterion, directory))

    return entities_with_paths


def read_all_campaigns(config) -> List[EntityTextWithPath]:

    directory = config.full_path_to_stories_directory()
    criterion = config.file_matches_campaign()

    entities_with_paths = _read_selected_entities_having_path(criterion, directory)
    return entities_with_paths


def read_all_stories(config) -> List[EntityTextWithPath]:

    directory = config.full_path_to_stories_directory()
    criterion = config.file_matches_story()

    entities_with_paths = _read_selected_entities_having_path(criterion, directory)
    return entities_with_paths


def read_all_actors(config) -> List[EntityTextWithPath]:

    directory = config.full_path_to_profiles_directory()
    criterion = config.file_matches_actor()

    entities_with_paths = _read_selected_entities_having_path(criterion, directory)
    return entities_with_paths


def read_all_factions(config) -> List[EntityTextWithPath]:

    directory = config.full_path_to_factions_directory()
    criterion = config.file_matches_faction()

    entities_with_paths = _read_selected_entities_having_path(criterion, directory)
    return entities_with_paths


def _read_selected_entities_having_path(criterion, directory):

    all_present_files = os.listdir(directory)
    interesting_files = [f for f in all_present_files if re.search(criterion, f)]
    entities_with_paths = []

    for file in interesting_files:
        read_text = read_file(directory + "/" + file)
        entities_with_paths.append(EntityTextWithPath(entity_text=read_text, path=file))

    return entities_with_paths
