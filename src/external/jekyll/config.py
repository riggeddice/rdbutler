from src.technical import config_helper

config = config_helper.load(__file__)
rd_repository_path = config['rd_repository_path']


def full_path_to_actor_autogen_directory() -> str:
    # Dependent on the Jekyll repo structure; do not change it
    return rd_repository_path + "/" + "inwazja/swiat/ludzki-swiat"


def full_path_to_profiles_directory() -> str:
    # Dependent on the Jekyll repo structure; do not change it
    return rd_repository_path + "/" + "inwazja/opowiesci/karty-postaci"


def full_path_to_stories_directory() -> str:
    # Dependent on the Jekyll repo structure; do not change it
    return rd_repository_path + "/" + "inwazja/opowiesci/konspekty"


def full_path_to_factions_directory() -> str:
    # Dependent on the Jekyll repo structure; do not change it
    return rd_repository_path + "/" + "inwazja/opowiesci/frakcje"


def full_actor_url(actor_uid: str) -> str:
    return "/rpg/inwazja/opowiesci/karty-postaci/" + actor_uid + ".html"


def full_story_url(story_uid:str) -> str:
    return "/rpg/inwazja/opowiesci/konspekty/" + story_uid + ".html"


def full_campagin_url(campaign_uid:str) -> str:
    return "/rpg/inwazja/opowiesci/konspekty/kampania-" + campaign_uid + ".html"


def file_matches_campaign() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"kampania-.+?\.md"


def file_matches_faction() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"frakcja-.+?\.md"


def file_matches_story() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"\d{6}-.+?\.md"


def file_matches_actor() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"(?!9999)\d{4}-.*.md"


def file_matches_actor_names() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"kategoryzowane-imiona\.md"


def file_matches_professions() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"klasyfikacja-zawodow\.md"


def file_matches_emotions() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"emocje\.md"


def file_matches_traits() -> str:
    # Dependent on the Jekyll repo configuration; do not change it
    return r"cechy-charakteru\.md"
