import pytest

from src.rpg_mechanics.structure.aspects.aspect import Aspect


@pytest.mark.parametrize(
    'name1, name2, result', [
        ('aspect1', 'aspect1', True),
        ('aspect1', 'aspect2', False)
    ]
)
def test_aspect_comparison(name1, name2, result):
    # Given
    aspect1 = Aspect(name1)
    aspect2 = Aspect(name2)

    # Then
    assert (aspect1 == aspect2) == result


def test_aspect_representation():
    # Given
    aspect1 = Aspect('aspect1')

    # Then
    assert str(aspect1) == 'Aspect(name="aspect1")'
