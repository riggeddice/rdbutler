import pytest
from textwrap import dedent
from copy import deepcopy


from src.technical.extract_from_text import extract_list_of_links, extract_list_of_unnamed_key_values, \
    extract_headerized_section_body, extract_single_param_from_front_matter, extract_param_list_from_front_matter, \
    extract_dict_of_potentially_emphasised_keys, extract_list_records, extract_list_of_tuples_from_table, \
    extract_list_of_text_to_numbers


def test_extract_list_of_text_to_numbers():

    # Given
    data = dedent("""
                Avocado
                squirrels 20
                quatra 5 10
                sd dasdlkd 4
                      HENRYK 11
                """)

    # Expected
    expected = [('squirrels', '20'), ('quatra', '5')]

    # When
    actual = extract_list_of_text_to_numbers(source=data, default=[])

    # Then
    assert(expected == actual)


@pytest.mark.parametrize('name, source, extracted_expected', [
    ('players', '---\nlayout: inwazja-konspekt\nplayers: kić, dzióbek\n---"', ["kić", "dzióbek"]),
    ('players', '---\nlayout: inwazja-konspekt\nplayers: kić; dzióbek\n---"', ["kić; dzióbek"]),
    ('GMs', '---\nlayout: inwazja-konspekt\ngms: kić\n---"', ["kić"]),
    ('players', '', []),
])
def test_extract_param_list_from_front_matter(name, source, extracted_expected):

    # When
    extracted_actual = extract_param_list_from_front_matter(name=name, source=source, list_separator=",", default=[])

    # Then
    assert extracted_expected == extracted_actual


@pytest.mark.parametrize('name, source, extracted_expected', [
    ('title', '---\nlayout: inwazja-konspekt\ntitle:   "Wspaniały Wieprz Wojtka\n---"', "Wspaniały Wieprz Wojtka"),
    ('layout', '---\nlayout: inwazja-konspekt\ntitle:  "Wspaniały Wieprz Wojtka\n---"', "inwazja-konspekt"),
    ('LaYoUt', '---\nlayout: inwazja-konspekt\n---"', "inwazja-konspekt"),
    ('not_present', '---\nlayout: inwazja-konspekt\ntitle:  "Wspaniały Wieprz Wojtka\n---"', "DEFAULT"),
    ('title', '', "DEFAULT"),
])
def test_extract_single_param_from_front_matter(name, source, extracted_expected):

    # When
    extracted_actual = extract_single_param_from_front_matter(name=name, source=source, default="DEFAULT")

    # Then
    assert extracted_expected == extracted_actual


@pytest.mark.parametrize('name, header_depth, finishing_header_scope, extracted_expected', [
    ('Section 1.1', 2, 3, 'Something here'),
    ('Section 1.1.1', 3, 3, ''),
    ('sEcTiOn 1.1.1', 3, 1, '### Section 1.1.2\ntext'),
    ('Section 1.1', 1, 3, 'cats'),
    ('Section 1.1', 3, 3, 'cats'),
    ('Unknown Header', 2, 3, 'cats')
])
def test_extract_headerized_section(name, header_depth, finishing_header_scope, extracted_expected):

    # Given
    text = dedent("""
            ## Section 1.1
            Something here
            ### Section 1.1.1
            ### Section 1.1.2
            text
            # Section 2
            text
            """)

    # When
    extracted_actual = extract_headerized_section_body(source=text, name=name, default="cats",
                                                       header_depth=header_depth,
                                                       finishing_header_scope=finishing_header_scope)

    # Then
    assert extracted_expected == extracted_actual


def test_extract_list_of_links_absent():

    # Given
    section = dedent("""
        ## Section
        Something else.
        * a [non]-(link) element
        """)

    default = [("Default label", "default-target")]

    # Expecting
    list_expected = deepcopy(default)

    # When
    list_actual = extract_list_of_links(source=section, default=default)

    # Then

    assert list_expected == list_actual


def test_extract_list_of_links_present():

    # Given

    section = dedent("""
        ## Section
        Something else.
        * a [non]-(link) element
        * [Single Link Label](single-link-target.html)
        * Text with [Link Label 2](link-target-2.html) not to be captured
        * [External Link Label](https://en.wikipedia.org/wiki/Hyperlink#Anchor) with text
        * a non-link element
        Something else.
        """)

    # Expecting
    list_expected = [("Single Link Label", "single-link-target.html"),
                     ("Link Label 2", "link-target-2.html"),
                     ("External Link Label", "https://en.wikipedia.org/wiki/Hyperlink#Anchor")]

    # When
    list_actual = extract_list_of_links(source=section, default=[])

    # Then

    assert list_expected == list_actual


def test_extract_list_of_unnamed_key_values_absent():

    # Given

    section = dedent("""
    ## Section
    Something else.
    * a non-KV element
    * a non-KV element
    """)

    default = [("default_key", "default_value")]

    # Expecting
    list_expected = deepcopy(default)

    # When
    list_actual = extract_list_of_unnamed_key_values(source=section, default=default)

    # Then

    assert list_expected == list_actual


def test_extract_dict_of_potentially_emphasised_keys():

    # Given

    section = dedent("""
    ## Section
    Something else.
    * a non-KV element
    * key_1: value_1
    * **key_2**: value_2
    * *key_3*: value_3
    * __key_4__: value_4
    * _key_5_: value_5
    *malformed_key_3: malformed_value_3
    * a non-KV element
    Something else.
    * key_6: value6""")

    # Expecting
    dict_expected = {"key_1": "value_1", "key_2": "value_2", "key_3": "value_3",
                     "key_4": "value_4", "key_5": "value_5", "key_6": "value6"}

    # When
    dict_actual = extract_dict_of_potentially_emphasised_keys(source=section, default={})

    # Then

    assert dict_expected == dict_actual


def test_extract_list_of_unnamed_key_values_present():

    # Given

    section = dedent("""
    ## Section
    Something else.
    * a non-KV element
    * key_1: value_1
    * key_2: value_2
    *malformed_key_3: malformed_value_3
    * a non-KV element
    Something else.
    """)

    # Expecting
    list_expected = [("key_1", "value_1"), ("key_2", "value_2")]

    # When
    list_actual = extract_list_of_unnamed_key_values(source=section, default=[])

    # Then

    assert list_expected == list_actual


def test_extract_list_records():

    # Given

    section = dedent("""
    ## Section
    Something else.
    * a non-KV element
    * key_1: value_1
    * **key_2**: value_2
    * *key_3*: value_3
    * __key_4__: value_4
    * _key_5_: value_5
    *malformed_key_3: malformed_value_3
    * a non-KV element
    Something else.
    """)

    # Expecting
    list_expected = ["a non-KV element", "key_1: value_1", "**key_2**: value_2", "*key_3*: value_3",
                     "__key_4__: value_4", "_key_5_: value_5", "a non-KV element"]

    # When
    list_actual = extract_list_records(source=section, default=[])

    # Then

    assert list_expected == list_actual


def test_extract_list_of_lists_from_table_easy():

    # Given
    values = {
        "header_1": "header_1", "header_2": "header_2",
        "dash_c1": "------------------", "dash_c2": "------------------",
        "r1c1": "Ala; Basia; Celina; ;", "r1c2": "AAA; BB; CC",
        "r2c1": "Daria", "r2c2": "DDDD",
        "r3c1": "Evelyn", "r3c2": ","
    }

    section = dedent("""
        |{header_1}|{header_2}|
        |{dash_c1}|{dash_c2}|
        |{r1c1}|{r1c2}|
        |{r2c1}|{r2c2}|
        |{r3c1}|{r3c2}|
    """).format(**values)

    # Expecting
    list_expected = [
        (values["header_1"], values["header_2"]),
        (values["dash_c1"], values["dash_c2"]),
        (values["r1c1"], values["r1c2"]),
        (values["r2c1"], values["r2c2"]),
        (values["r3c1"], values["r3c2"])
    ]

    # When
    list_actual = extract_list_of_tuples_from_table(section, [])

    # Then
    assert list_expected == list_actual


def test_extract_list_of_lists_from_table_with_whitespaces():

    # Given
    values = {
        "header_1": " header_1 ", "header_2": "     header_2      ",
        "dash_c1": "------------------", "dash_c2": "------------------",
        "r1c1": " Ala; Basia; Celina; ; ", "r1c2": " AAA; BB; CC  ",
        "r2c1": " ", "r2c2": " . ",
        "r3c1": " Evelyn ", "r3c2": "     ,"
    }

    section = dedent("""
        |{header_1}|{header_2}|
        |{dash_c1}|{dash_c2}|
        |{r1c1}|{r1c2}|
        |{r2c1}|{r2c2}|
        |{r3c1}|{r3c2}|
    """).format(**values)

    # Expecting
    list_expected = [
        (values["header_1"], values["header_2"]),
        (values["dash_c1"], values["dash_c2"]),
        (values["r1c1"], values["r1c2"]),
        (values["r2c1"], values["r2c2"]),
        (values["r3c1"], values["r3c2"])
    ]

    # When
    list_actual = extract_list_of_tuples_from_table(section, [])

    # Then
    assert list_expected == list_actual
