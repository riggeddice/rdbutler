import pytest

from src.technical.random_distributions import roulette_breakpoint_list, perform_roulette_selection


@pytest.mark.parametrize("given, randint, expected", [
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 1, "cats"),
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 10, "cats"),
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 11, "dogs"),
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 30, "dogs"),
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 31, "reptiles"),
    ([("cats", 10), ("dogs", 20), ("reptiles", 3)], 33, "reptiles")
])
def test_perform_roulette_selection(given, randint, expected):

    # When
    actual = perform_roulette_selection(given, lambda _1, _2: randint)

    # Then
    assert(expected == actual)


@pytest.mark.parametrize("given, expected", [
    ([("cats", 100), ("dogs", 50)], [("cats", 100), ("dogs", 150)]),
    ([("A", 1), ("B", 3), ("C", 1), ("D", 2)], [("A", 1), ("B", 4), ("C", 5), ("D", 7)])
])
def test_roulette_breakpoint_list__simplest(given, expected):

    # When
    actual = roulette_breakpoint_list(given)

    # Then
    assert(expected == actual)
