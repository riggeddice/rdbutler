import pytest

from src.technical.build_regex import create_regex_extracting_header_section, \
    create_regex_extracting_param_from_front_matter, create_regex_extracting_text_to_numbers_table


@pytest.mark.parametrize('regex_expected', [
    r'^\b(?P<text>\w+)\b\s+\b(?P<number>\d+)\b'
])
def test_extract_text_to_numbers_table(regex_expected):

    # When
    regex_actual = create_regex_extracting_text_to_numbers_table()

    # Then
    assert regex_actual == regex_expected


@pytest.mark.parametrize('name, regex_expected', [
    ('title', r'---.+?^title:\s*\"*(.+?)\"*\s*$.*---')
])
def test_extract_front_matter_param_regex(name, regex_expected):

    # When
    regex_actual = create_regex_extracting_param_from_front_matter(name=name)

    # Then
    assert regex_actual == regex_expected


@pytest.mark.parametrize(
    'name, header_depth, blocking_header_depth, regex_expected', [
        ('Progresja', 1, 3, r"^#\sProgresja[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)"),
        ('progresja', 1, 3, r"^#\sprogresja[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)"),
        ('opis', 2, 1, r"^##\sopis[:|\s]*$(.+?)(\Z|^#{1,1}\s\w+)"),
        ('Zasługi', 1, 1, r"^#\sZasługi[:|\s]*$(.+?)(\Z|^#{1,1}\s\w+)"),
        ('Szkoły magiczne', 3, 3, r"^###\sSzkoły magiczne[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)"),
        ('Motywacje (do czego dąży)', 3, 3, r"^###\sMotywacje \(do czego dąży\)[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)")
    ]
)
def test_extract_entity_section_body_regex(name, header_depth, blocking_header_depth, regex_expected):

    # When
    regex_actual = create_regex_extracting_header_section(header_depth=header_depth,
                                                          blocking_header_depth=blocking_header_depth, name=name)

    # Then
    assert regex_actual == regex_expected
