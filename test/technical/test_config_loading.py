from src.technical import config_helper


def test_load_config_with_local_value_overwrite():
    expected_config = {
        'key1': 'value1',
        'key2': 'value2',
        'key3': 'value3',
        'key4': 'value4',
        'key5': [
            'value5_1',
            'value5_2',
            'value5_3'
        ],
        'key6': {
            'key6_1': 'value6_1',
            'key6_2': 'value6_2'
        },
    }
    actual_config = config_helper.load(__file__)
    assert actual_config == expected_config


def test_load_config_without_local_value_overwrite():
    expected_config = {
        'key1': 1,
        'key2': '2',
        'key3': [
            'value3_1',
            'value3_2',
            'value3_3'
        ],
        'key4': {
            'key4_1': 'value4_1',
            'key4_2': 'value4_2'
        },
    }
    actual_config = config_helper.load(__file__, config_file_name='config2.yml')
    assert actual_config == expected_config
