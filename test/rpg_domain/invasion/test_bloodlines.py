import pytest

from src.application.entities.actors.actor import Actor
from src.application.entities.actors.create import create_actor_with_defaults


from src.rpg_domain.invasion.bloodlines import actor_is_from_bloodline, faction_is_actors_bloodline


@pytest.mark.parametrize("actor,faction,expected", [
    (create_actor_with_defaults(name="Adonis Sowiński"), "Sowińscy", True),
    (create_actor_with_defaults(name="Adonis Sowiński"), "Bankierz", False),
    (create_actor_with_defaults(name="Ola Jurta"), "Zajcew", False),
])
def test_faction_is_actors_bloodline(actor: Actor, faction: str, expected: bool):
    assert (expected == faction_is_actors_bloodline(actor, faction))


@pytest.mark.parametrize("actor,expected", [
    (create_actor_with_defaults(name="Adonis Sowiński"), True),
    (create_actor_with_defaults(name="Kornelia Bankierz-Jagódka"), True),
    (create_actor_with_defaults(name="Ola Jurta"), False),
])
def test_faction_is_actors_bloodline(actor: Actor, expected: bool):
    assert (expected == actor_is_from_bloodline(actor))
