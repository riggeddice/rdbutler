from src.rpg_domain.invasion.entities.magics.create import create_magics_from_actor_text
from src.rpg_domain.invasion.entities.magics.magic import Magic
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.categories.extract import extract_category_section_body


def test_magic_comparison_same():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    magic1 = Magic(name="magic1", aspects=[aspect1, aspect2])
    magic2 = Magic(name="magic1", aspects=[aspect1, aspect2])

    # Then
    assert magic1 == magic2


def test_magic_comparison_different():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    aspect3 = Aspect("aspect3")
    aspect4 = Aspect("aspect4")
    magic1 = Magic(name="magic1", aspects=[aspect1, aspect2])
    magic2 = Magic(name="magic2", aspects=[aspect3, aspect4])

    # Then
    assert magic1 != magic2


def test_magic_representation():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    magic1 = Magic(name="magic1", aspects=[aspect1, aspect2])

    # Then
    assert str(magic1) == 'Magic(name="magic1", aspects="[Aspect(name="aspect1"), Aspect(name="aspect2")]")'


def test_create_magic_from_actor_body_pre_1803():
    actor_body = "\n### Motywacje (do czego dąży)\n\n* opis motywacji" \
                    ".\n\n### Szkoły magiczne\n\n* **Skill1**:\n\n    * _Aspekty_: aspect1, aspect2, interesting aspect 3 \n  " \
                    "* _Opis_: description1\n" \
                    "* **Skill two**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n" \
                    "* **Skill three**: \n\n    * _Aspekty_: Aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n" \
                    "* **Skill4ssadasda**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n\n### Silne i słabe strony:"

    expected_magics = [
        Magic(name="skill1",
              aspects=[Aspect(name="aspect1"), Aspect(name="aspect2"), Aspect(name="interesting aspect 3")]),
        Magic(name="skill two",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Magic(name="skill three",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Magic(name="skill4ssadasda",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")])
    ]

    actual_magics = create_magics_from_actor_text(actor_body, "1709")

    assert actual_magics == expected_magics


def test_extract_magic_body_section():
    given_section = "\n### AAA\n\n* opis motywacji" \
                    ".\n\n### Szkoły magiczne\n\n* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć.\n"
    expected_section = "* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć."

    actual_section = extract_category_section_body(Magic.headers(), given_section)

    assert actual_section == expected_section.strip()
