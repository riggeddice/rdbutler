from textwrap import dedent

from hypothesis import given, strategies

from src.rpg_domain.invasion.entities.motivations.create import create_motivation
from src.rpg_domain.invasion.entities.motivations.create import create_motivations_from_actor_text
from src.rpg_domain.invasion.entities.motivations.motivation import Motivation
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.aspects.create import create_aspects_from_list
from src.rpg_mechanics.structure.categories.extract import extract_category_section_body


def test_motivation_comparison_same():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    motivation1 = Motivation(name="motivation1", aspects=[aspect1, aspect2])
    motivation2 = Motivation(name="motivation1", aspects=[aspect1, aspect2])

    # Then
    assert motivation1 == motivation2


def test_motivation_comparison_different():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    aspect3 = Aspect("aspect3")
    aspect4 = Aspect("aspect4")
    motivation1 = Motivation(name="motivation1", aspects=[aspect1, aspect2])
    motivation2 = Motivation(name="motivation2", aspects=[aspect3, aspect4])

    # Then
    assert motivation1 != motivation2


def test_motivation_representation():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    motivation = Motivation(name="motivation1", aspects=[aspect1, aspect2])

    # Then
    assert 'Motivation(name="motivation1", aspects="[Aspect(name="aspect1"), Aspect(name="aspect2")]")' == str(motivation)


def test_create_motivations_from_actor_body_pre_1803():

    mechanics_version = "1709"

    given_actor_body = "\n### Umiejętności\n\n* opis motywacji" \
                 ".\n\n### Motywacje (do czego dąży)\n\n* **Skill1**:\n\n    * _Aspekty_: aspect1, aspect2, interesting aspect 3 \n  " \
                 "* _Opis_: description1\n" \
                 "* **Skill two**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                 "* _Opis_: description21\n" \
                 "* **Skill three**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                 "* _Opis_: description21\n" \
                 "* **Skill4ssadasda**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                 "* _Opis_: description21\n\n### Silne i słabe strony:"

    expected_motivations = [
        Motivation(name="skill1",
              aspects=[Aspect(name="aspect1"), Aspect(name="aspect2"), Aspect(name="interesting aspect 3")]),
        Motivation(name="skill two",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Motivation(name="skill three",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Motivation(name="skill4ssadasda",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")])
    ]

    actual_motivations = create_motivations_from_actor_text(given_actor_body, mechanics_version)

    assert expected_motivations == actual_motivations


@given(strategies.text().filter(lambda x: "|" not in x and ";" not in x and "\n" not in x))
def test_create_motivations_from_actor_body_1803_hypothesis(aspect):

    # Given
    mechanics_version = "1803"

    categories_aspects = {
        "hdr_cat": "Kategoria", "hdr_asp": aspect,
        "cat_ind": "Indywidualne", "asp_ind_1": aspect, "asp_ind_2": "eksploracja różnorodności",
        "cat_spl": "Społeczne", "asp_spl_1": "autonomia", "asp_spl_2": aspect, "asp_spl_3": "antyniewolnictwo",
        "cat_wrt": "Wartości", "asp_wrt_1": aspect, "asp_wrt_2": aspect, "asp_wrt_3": aspect
    }

    given_actor_body = dedent("""
                                ### Motywacje

                                #### Kategorie i Aspekty

                                | {hdr_cat}         |     {hdr_asp}                              |
                                |-------------------|--------------------------------------------|
                                | {cat_ind}         | {asp_ind_1}; {asp_ind_2}                   |
                                | {cat_spl}         | {asp_spl_1}; {asp_spl_2}; {asp_spl_3}      |
                                | {cat_wrt}         | {asp_wrt_1}; {asp_wrt_2}; {asp_wrt_3}      |

    """).format(**categories_aspects)

    # Expecting
    expected_motivations = [
        create_motivation(name=categories_aspects["cat_ind"].lower(),
                          aspects=create_aspects_from_list(
                                    [categories_aspects["asp_ind_1"],
                                     categories_aspects["asp_ind_2"]]
                               )),
        create_motivation(name=categories_aspects["cat_spl"].lower(),
                          aspects=create_aspects_from_list(
                                   [categories_aspects["asp_spl_1"],
                                    categories_aspects["asp_spl_2"],
                                    categories_aspects["asp_spl_3"]]
                               )),
        create_motivation(name=categories_aspects["cat_wrt"].lower(),
                          aspects=create_aspects_from_list(
                                   [categories_aspects["asp_wrt_1"],
                                    categories_aspects["asp_wrt_2"],
                                    categories_aspects["asp_wrt_3"]]
                               ))
    ]

    # When
    actual_motivations = create_motivations_from_actor_text(given_actor_body, mechanics_version)

    # When
    assert expected_motivations == actual_motivations


def test_create_motivations_from_actor_body_1803():

    mechanics_version = "1803"

    categories_aspects = {
        "hdr_cat": "Kategoria", "hdr_asp": "Aspekty",
        "cat_ind": "Indywidualne", "asp_ind_1": "promocja sztuki", "asp_ind_2":"eksploracja różnorodności",
        "cat_spl": "Społeczne", "asp_spl_1": "autonomia", "asp_spl_2": "praworządność", "asp_spl_3": "antyniewolnictwo",
        "cat_wrt": "Wartości", "asp_wrt_1": "kojenie", "asp_wrt_2":"kolekcjonowanie", "asp_wrt_3": "współdziałanie",
        "hdr_yes": "Co chce by się działo?", "hdr_no": "Co na pewno ma się NIE dziać? Co jest sprzeczne?",
        "yes_01": "promuj różnorodną sztukę", "yes_02": "poznawaj jak najwięcej różnorodnych dzieł",
        "yes_03": "bądź ceniony w Świecy", "yes_04": "odzyskaj pozycję", "yes_05": "odbuduj Rusznicę z ruin",
        "yes_06": "zwalczaj wszelkie formy zniewolenia",
        "no_01": "celem sztuki jest zarobić jak najwięcej", "no_02": "najważniejsze jest to, co przyjemne",
        "no_03": "unikaj innych, są udręką", "no_04": "skreśl tych, co zrobili błąd"
                                    }
    given_actor_body = dedent("""
                                ### Motywacje

                                #### Kategorie i Aspekty

                                | {hdr_cat}         |     {hdr_asp}                              |
                                |-------------------|--------------------------------------------|
                                | {cat_ind}         | {asp_ind_1}; {asp_ind_2}                   |
                                | {cat_spl}         | {asp_spl_1}; {asp_spl_2}; {asp_spl_3}      |
                                | {cat_wrt}         | {asp_wrt_1}; {asp_wrt_2}; {asp_wrt_3}      |

                                #### Szczególnie

                                | {hdr_yes}                         | {hdr_no}                      |
                                |-----------------------------------|-------------------------------|
                                | {yes_01}; {yes_02}                | {no_01}; {no_02}              |
                                | {yes_03};                         | |
                                | {yes_04}; {yes_05}; {yes_06}      | {no_03} ; {no_04} ; ;     |
                                
    """).format(**categories_aspects)

    expected_motivations = [
        Motivation(name=categories_aspects["cat_ind"].lower(),
                   aspects=[Aspect(name=categories_aspects["asp_ind_1"]),
                            Aspect(name=categories_aspects["asp_ind_2"])
                            ]),
        Motivation(name=categories_aspects["cat_spl"].lower(),
                   aspects=[Aspect(name=categories_aspects["asp_spl_1"]),
                            Aspect(name=categories_aspects["asp_spl_2"]),
                            Aspect(name=categories_aspects["asp_spl_3"])
                            ]),
        Motivation(name=categories_aspects["cat_wrt"].lower(),
                   aspects=[Aspect(name=categories_aspects["asp_wrt_1"]),
                            Aspect(name=categories_aspects["asp_wrt_2"]),
                            Aspect(name=categories_aspects["asp_wrt_3"])
                            ])
    ]

    actual_motivations = create_motivations_from_actor_text(given_actor_body, mechanics_version)

    assert expected_motivations == actual_motivations


def test_extract_motivation_body_section_pre_1803():
    given_section = "\n### Motywacje (do czego dąży)\n\n* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć.\n"
    expected_section = "* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć."

    actual_section = extract_category_section_body(Motivation.headers(), given_section)

    assert expected_section.strip() == actual_section


def test_extract_motivation_body_section_1803():

    given_section = dedent("""
                            ### Motywacje
                            
                            #### Kategorie i Aspekty
                            
                            | Kategoria         | Aspekty                                    |
                            |-------------------|--------------------------------------------|
                            | Indywidualne      | promocja sztuki; eksploracja różnorodności |
                            | Społeczne         | autonomia; praworządność; antyniewolnictwo |
                            | Wartości          | kojenie; kolekcjonowanie; współdziałanie   |
                            
                            #### Szczególnie
                            
                            | Co chce by się działo?                                                          | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                        |
                            |---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
                            | promuj różnorodną sztukę; poznawaj jak najwięcej różnorodnych dzieł             | celem sztuki jest zarobić jak najwięcej; najważniejsze jest to, co przyjemne i daje radość              |
                            | bądź ceniony w Świecy, odzyskaj pozycję; odbuduj Rusznicę z ruin                | nie mam za co przepraszać; trzeba pójść dalej i nie patrzeć w przeszłość                                |
                            | zwalczaj wszelkie formy zniewolenia; zatrzymaj corruptorów i deviatorów         | jeśli ofiara CHCE kontroli to jest to OK; silniejszy ma więcej praw; akceptuj różnorodność gildii       |
                            | wolność i samostanowienie to odpowiedzialność; promuj działania zgodne z prawem | wykonywanie rozkazów zwalnia od myślenia; prawo jest ważniejsze niż moralność                           |
                            | ukoj cierpienie przez zniszczenie; daj każdemu szansę - ludzie się zmieniają    | niszczenie dla niszczenia jest fajne; przez konflikty ludzie są silniejsi; skreśl tych, co zrobili błąd |
                            | szukaj towarzystwa; zachowuj się elegancko i uprzejmie; metodycznie, z planem   | unikaj innych, są udręką; zachowuj się tak, by maksymalizować korzyści; impulsywnie, za intuicją        |
""")

    expected_section = dedent("""
                            #### Kategorie i Aspekty
                            
                            | Kategoria         | Aspekty                                    |
                            |-------------------|--------------------------------------------|
                            | Indywidualne      | promocja sztuki; eksploracja różnorodności |
                            | Społeczne         | autonomia; praworządność; antyniewolnictwo |
                            | Wartości          | kojenie; kolekcjonowanie; współdziałanie   |
                            
                            #### Szczególnie
                            
                            | Co chce by się działo?                                                          | Co na pewno ma się NIE dziać? Co jest sprzeczne?                                                        |
                            |---------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
                            | promuj różnorodną sztukę; poznawaj jak najwięcej różnorodnych dzieł             | celem sztuki jest zarobić jak najwięcej; najważniejsze jest to, co przyjemne i daje radość              |
                            | bądź ceniony w Świecy, odzyskaj pozycję; odbuduj Rusznicę z ruin                | nie mam za co przepraszać; trzeba pójść dalej i nie patrzeć w przeszłość                                |
                            | zwalczaj wszelkie formy zniewolenia; zatrzymaj corruptorów i deviatorów         | jeśli ofiara CHCE kontroli to jest to OK; silniejszy ma więcej praw; akceptuj różnorodność gildii       |
                            | wolność i samostanowienie to odpowiedzialność; promuj działania zgodne z prawem | wykonywanie rozkazów zwalnia od myślenia; prawo jest ważniejsze niż moralność                           |
                            | ukoj cierpienie przez zniszczenie; daj każdemu szansę - ludzie się zmieniają    | niszczenie dla niszczenia jest fajne; przez konflikty ludzie są silniejsi; skreśl tych, co zrobili błąd |
                            | szukaj towarzystwa; zachowuj się elegancko i uprzejmie; metodycznie, z planem   | unikaj innych, są udręką; zachowuj się tak, by maksymalizować korzyści; impulsywnie, za intuicją        |
                            """)

    actual_section = extract_category_section_body(Motivation.headers(), given_section)

    assert expected_section.strip() == actual_section.strip()
