from src.rpg_mechanics.invasion.default.extract_mechanics_structure_default import extract_mechanics_structure_default


def test_extract_mechanics_structure_damaged_records_pre_1803():

    # Expecting

    mechanics_structure_expected = [
        (
            "Elektronika i telekomunikacja", [
            "systemy podsłuchowe: transmisja danych", "urządzenia telekomunikacyjne"
        ]),
        ("Domena bezaspektowa", []),
        ("aaaa", []),
        ("Tekst bez wyboldowania", ["miaukotek"])
    ]

    # Given

    mechanics_structure_section = """
* **Elektronika i telekomunikacja**:
    * _Aspekty_: systemy podsłuchowe: transmisja danych, urządzenia telekomunikacyjne
    * Uszkodzony rekord
* **Domena bezaspektowa**
* **a:a:a:a:**: 
    Text 
* Tekst bez wyboldowania
    * Aspekty: miaukotek """

    # When
    mechanics_structure_actual = extract_mechanics_structure_default(mechanics_structure_section, [])

    # Then
    assert mechanics_structure_expected == mechanics_structure_actual


def test_extract_mechanics_structure_all_fine_pre_1803():

    # Expecting

    mechanics_structure_expected = [
        (
            "Elektronika i telekomunikacja",
            ["systemy podsłuchowe", "transmisja danych", "urządzenia telekomunikacyjne"]
         ),
        (
            "Propagandzista najwyższej klasy",
            ["dezinformacja", "wystąpienia publiczne"]
        ),
        (
            "Niebezpieczny regent mafii", [])
    ]

    # Given

    mechanics_structure_section = """
* **Elektronika i telekomunikacja**:
    * _Aspekty_: systemy podsłuchowe, transmisja danych, urządzenia telekomunikacyjne
    * _Opis_: Robert jest specjalistą od sieci i komunikacji idącej
* **Propagandzista najwyższej klasy**: 
    * _Aspekty_: dezinformacja, wystąpienia publiczne,
    * _Opis_: Robert słynie z tego, że potrafi kontrolować 
* **Niebezpieczny regent mafii**:
    * _Aspekty_: 
    * _Opis_: """

    # When
    mechanics_structure_actual = extract_mechanics_structure_default(mechanics_structure_section, [])

    # Then
    assert mechanics_structure_expected == mechanics_structure_actual
