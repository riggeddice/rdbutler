from textwrap import dedent

from hypothesis import given, strategies

from src.rpg_domain.invasion.entities.skills.create import create_skills_from_actor_text, create_skill
from src.rpg_domain.invasion.entities.skills.skill import Skill
from src.rpg_mechanics.structure.aspects.aspect import Aspect
from src.rpg_mechanics.structure.aspects.create import create_aspects_from_list
from src.rpg_mechanics.structure.categories.extract import extract_category_section_body


def test_skill_comparison_same():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    skill1 = Skill(name="skill1", aspects=[aspect1, aspect2])
    skill2 = Skill(name="skill1", aspects=[aspect1, aspect2])

    # Then
    assert skill1 == skill2


def test_skill_comparison_different():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    aspect3 = Aspect("aspect3")
    aspect4 = Aspect("aspect4")
    skill1 = Skill(name="skill1", aspects=[aspect1, aspect2])
    skill2 = Skill(name="skill2", aspects=[aspect3, aspect4])

    # Then
    assert skill1 != skill2


def test_skill_representation():
    # Given
    aspect1 = Aspect("aspect1")
    aspect2 = Aspect("aspect2")
    skill1 = Skill(name="skill1", aspects=[aspect1, aspect2])

    # Then
    assert str(skill1) == 'Skill(name="skill1", aspects="[Aspect(name="aspect1"), Aspect(name="aspect2")]")'


@given(strategies.text().filter(lambda x: "|" not in x and ";" not in x and "\n" not in x))
def test_create_motivations_from_actor_body_1803_hypothesis(aspect):

    # Given
    mechanics_version = "1803"

    categories_aspects = {
        "hdr_cat": "Kategoria", "hdr_asp": aspect,
        "cat_ind": "hodowca", "asp_ind_1": aspect, "asp_ind_2": "koralowce",
        "cat_spl": "naukowiec", "asp_spl_1": "czarne eksperymenty", "asp_spl_2": aspect, "asp_spl_3": "intensyfikacja",
        "cat_wrt": "farmaceuta", "asp_wrt_1": aspect, "asp_wrt_2": aspect, "asp_wrt_3": aspect
    }

    given_actor_body = dedent("""
                                ### Umiejętności

                                #### Kategorie i Aspekty

                                | {hdr_cat}         |     {hdr_asp}                              |
                                |-------------------|--------------------------------------------|
                                | {cat_ind}         | {asp_ind_1}; {asp_ind_2}                   |
                                | {cat_spl}         | {asp_spl_1}; {asp_spl_2}; {asp_spl_3}      |
                                | {cat_wrt}         | {asp_wrt_1}; {asp_wrt_2}; {asp_wrt_3}      |

    """).format(**categories_aspects)

    # Expecting
    expected_skills = [
        create_skill(name=categories_aspects["cat_ind"].lower(),
                     aspects=create_aspects_from_list(
                                    [categories_aspects["asp_ind_1"],
                                     categories_aspects["asp_ind_2"]]
                               )),
        create_skill(name=categories_aspects["cat_spl"].lower(),
                     aspects=create_aspects_from_list(
                                   [categories_aspects["asp_spl_1"],
                                    categories_aspects["asp_spl_2"],
                                    categories_aspects["asp_spl_3"]]
                               )),
        create_skill(name=categories_aspects["cat_wrt"].lower(),
                     aspects=create_aspects_from_list(
                                   [categories_aspects["asp_wrt_1"],
                                    categories_aspects["asp_wrt_2"],
                                    categories_aspects["asp_wrt_3"]]
                               ))
    ]

    # When
    actual_skills = create_skills_from_actor_text(given_actor_body, mechanics_version)

    # When
    assert expected_skills == actual_skills


def test_create_skills_from_actor_body_pre_1803():
    actor_body = "\n### Motywacje (do czego dąży)\n\n* opis motywacji" \
                    ".\n\n### Umiejętności\n\n* **Skill1**:\n\n    * _Aspekty_: aspect1, aspect2, interesting aspect 3 \n  " \
                    "* _Opis_: description1\n" \
                    "* **Skill two**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n" \
                    "* **Skill three**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n" \
                    "* **Skill4ssadasda**: \n\n    * _Aspekty_: aspect21, aspect22, interesting aspect 23 \n  " \
                    "* _Opis_: description21\n\n### Silne i słabe strony:"

    expected_skills = [
        Skill(name="skill1",
              aspects=[Aspect(name="aspect1"), Aspect(name="aspect2"), Aspect(name="interesting aspect 3")]),
        Skill(name="skill two",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Skill(name="skill three",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")]),
        Skill(name="skill4ssadasda",
              aspects=[Aspect(name="aspect21"), Aspect(name="aspect22"), Aspect(name="interesting aspect 23")])
    ]

    actual_skills = create_skills_from_actor_text(actor_body, "1709")

    assert actual_skills == expected_skills


def test_extract_skill_body_section():
    given_section = "\n### Motywacje (do czego dąży)\n\n* opis motywacji" \
                    ".\n\n### Umiejętności\n\n* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć.\n"
    expected_section = "* **Agentka wywiadu**:\n    * _Aspekty_: podkładanie dowodów, " \
                    "ustawianie czujników, gubienie tropu, perfekcyjna maska, wywiad po dokumentach, włażenie " \
                    "gdzie jej nie chcą, ukrywanie czujników, kradzież kieszonkowa, niepozorna myszka\n    " \
                    "* _Opis_: Kaja bardzo często wie, że coś \"nie gra\". Bardzo często potrafi spędzać sporo czasu " \
                    "w bibliotece czy wywiadowni by zdobyć istotne informacje na różne tematy. Trzyma perfekcyjną " \
                    "maskę służki, acz dość bezczelnej, i specjalizuje się w podkładaniu czujników.\n" \
                    "* **Konstruktor czujników**: \n    * _Aspekty_: kalibracja czujników, budowanie czujników, " \
                    "software + hardware, elektronika ludzka\n    * _Opis_: Z zamiłowania konstruktorka, " \
                    "z konieczności szpieg. Kaja bardzo lubi spędzać czas nad kalibracją, doprecyzowywaniem, " \
                    "optymalizacją różnego rodzaju czujników. Szczególnie lubuje się w czujnikach \"full band\" - " \
                    "Zmysły + Kataliza + ludzkie umiejętności konstrukcji. Bardzo cierpliwa, potrafi je świetnie " \
                    "skalibrować i dobrze ukryć."

    actual_section = extract_category_section_body(Skill.headers(), given_section)

    assert actual_section == expected_section.strip()
