import datetime

from textwrap import dedent

from src.application.entities.actors.create import create_actor_with_defaults
from src.application.entities.stories.create import create_story_with_default_params
from src.application.entities.campaigns.create import create_campaign_with_default_params
from src.application.entities.factions.create import create_faction_with_defaults
from src.external.neo4j.queries.queries_modify import *


def test__neo4j_queries__create_actors_with_whole_ecosystem():
    # Given data
    factions = ["Millennium", "Dare Shiver"]
    actor_uid = "ola-iks-1702"
    name = "Ola Iks"
    mechanics_version = "1702"
    actor_type = "NPC"
    mechanics = "This is a place for rpg_mechanics.\nTry it."
    body = "Body here.\nUseful."
    description = "Description here.\nUseful too."
    skills_section = '\n### Umiejętności\n\n' \
                     '* **Skill1**:\n\n    * _Aspekty_: aspect1 \n  ' \
                     '* _Opis_: description1'
    motivations_section = '\n### Motywacje (do czego dąży)\n\n' \
                          '* **Motivation**:\n\n    * _Aspekty_: aspekt1 \n  ' \
                          '* _Opis_: description1'

    body += skills_section
    body += motivations_section

    actor = create_actor_with_defaults(uid=actor_uid,
                                       name=name,
                                       factions=factions,
                                       mechanics=mechanics,
                                       body=body,
                                       actor_type=actor_type,
                                       mechanics_version=mechanics_version,
                                       description=description)

    actors = [actor]

    # Expecting results
    expected_query = dedent("""\
        MERGE (s0:Skill{name:'skill1'})
        MERGE (as0_s0:Aspect{name:'aspect1'})
        MERGE (motivation0:Motivation{name:'motivation'})
        MERGE (as0_motivation0:Aspect{name:'aspekt1'})
        MERGE (f0_0:Faction {name:'Millennium'})
        MERGE (f0_1:Faction {name:'Dare Shiver'})
        MERGE (p0:Player {name:'Publiczny'})
        CREATE
            (a0:Actor{
                uid:"ola-iks-1702",
                name:"Ola Iks",
                mechanics_version:"1702",
                actor_type:"NPC",
                rpg_mechanics:"This is a place for rpg_mechanics.
        Try it.",
                body:"Body here.
        Useful.
        ### Umiejętności
        
        * **Skill1**:
        
            * _Aspekty_: aspect1 
          * _Opis_: description1
        ### Motywacje (do czego dąży)
        
        * **Motivation**:
        
            * _Aspekty_: aspekt1 
          * _Opis_: description1",
                description:"Description here.
        Useful too."
            }),
            (a0)-[:IS_IN]->(f0_0),
            (a0)-[:IS_IN]->(f0_1),
            (s0)<-[:HAS_SKILL]-(a0),
            (s0)-[:HAS_ASPECT]->(as0_s0),
            (a0)-[:HAS_ASPECT]->(as0_s0),
            (motivation0)<-[:HAS_MOTIVATION]-(a0),
            (motivation0)-[:HAS_ASPECT]->(as0_motivation0),
            (a0)-[:HAS_ASPECT]->(as0_motivation0),
            (a0)<-[:OWNS]-(p0)
        """)

    # When
    actual_query = create_actor_ecosystem(actors)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__create_stories_with_whole_ecosystem():
    # Given data
    # # Story data
    story_id = "150313-story"
    title = "Storytime"
    campaign = "dark_campaign"
    summary = "This is a summary. \nTends to work."
    player_name_1 = "Kić"
    player_name_2 = "Dzióbek"
    players = [player_name_1, player_name_2]
    gm_1 = "Żółw"
    gm_2 = "Dust"
    gms = [gm_1, gm_2]

    merits = ["mag", "Anna Diakon", "was happy", "vic", "Xarth", "was himself"]
    actor_progressions = ["Anna Diakon", "gained something", "Xarth", "can do"]
    faction_progression = ["KADEM", "got a thing"]
    actor_plans = ["Anna Diakon", "wants this", "Xarth", "will go"]
    faction_plan = ["KADEM", "plans devious act"]

    body_flavor = "This is a body\n"
    body_merits = "# Zasługi\n* %s: %s, %s\n* %s: %s, %s\n" % (merits[0], merits[1], merits[2],
                                                               merits[3], merits[4], merits[5])
    body_progression = "# Progresja\n* %s: %s\n* %s: %s\n" % (actor_progressions[0], actor_progressions[1],
                                                              actor_progressions[2], actor_progressions[3])
    body_faction_progression = "## Frakcji\n* %s: %s\n" % (faction_progression[0], faction_progression[1])
    body_plan = "# Plany\n* %s: %s\n* %s: %s\n" % (actor_plans[0], actor_plans[1],
                                                       actor_plans[2], actor_plans[3])
    body_faction_plan = "## Frakcji\n* %s: %s\n" % (faction_plan[0], faction_plan[1])

    body = body_flavor + body_merits + body_progression + body_faction_progression + body_plan + body_faction_plan
    # # Story itself
    story = create_story_with_default_params(uid=story_id, title=title, campaign=campaign, summary=summary,
                                             players=players, gms=gms,  body=body)
    story.populated_data[KEY_story_absolute_position] = 16
    story.populated_data[KEY_story_position_in_campaign] = 4
    story.populated_data[KEY_story_start_date] = datetime.date(year=2010, month=1, day=1)
    story.populated_data[KEY_story_end_date] = datetime.date(year=2010, month=2, day=2)

    # Expecting results

    expected_query = dedent("""\
        MERGE (c0:Campaign {uid:'dark_campaign'})
        MERGE (a_m0_in_s0:Actor {name:'Anna Diakon'})
        MERGE (a_m1_in_s0:Actor {name:'Xarth'})
        MERGE (a_p0_in_s0:Actor {name:'Anna Diakon'})
        MERGE (a_p1_in_s0:Actor {name:'Xarth'})
        MERGE (f_p2_in_s0:Faction {name:'KADEM'})
        MERGE (a_plan0_in_s0:Actor {name:'Anna Diakon'})
        MERGE (a_plan1_in_s0:Actor {name:'Xarth'})
        MERGE (f_plan2_in_s0:Faction {name:'KADEM'})
        MERGE (p0_in_s0:Player {name:'Kić'})
        MERGE (p1_in_s0:Player {name:'Dzióbek'})
        MERGE (gm0_in_s0:GameMaster {name:'Żółw'})
        MERGE (gm1_in_s0:GameMaster {name:'Dust'})
        MERGE (sm0:Summary {body:'This is a summary. 
        Tends to work.'})
        MERGE (st0:Story{
            uid:"150313-story",
            title:"Storytime",
            body:"This is a body
        # Zasługi
        * mag: Anna Diakon, was happy
        * vic: Xarth, was himself
        # Progresja
        * Anna Diakon: gained something
        * Xarth: can do
        ## Frakcji
        * KADEM: got a thing
        # Plany
        * Anna Diakon: wants this
        * Xarth: will go
        ## Frakcji
        * KADEM: plans devious act
        ",
            duration:"1",
            delay:"0",
            start_date:"2010-01-01",
            end_date:"2010-02-02",
            absolute_position:16,
            in_campaign_position:4
        })
        CREATE
            (m0_in_s0:Merit{
                story_id:"150313-story",
                actor_name:"Anna Diakon",
                actual_merit:"was happy",
                qualifier:"mag"
            }),
            (m1_in_s0:Merit{
                story_id:"150313-story",
                actor_name:"Xarth",
                actual_merit:"was himself",
                qualifier:"vic"
            }),
            (prog0_in_s0:Progression{
                story_id:"150313-story",
                principal_name:"Anna Diakon",
                actual_progression:"gained something"
            }),
            (prog1_in_s0:Progression{
                story_id:"150313-story",
                principal_name:"Xarth",
                actual_progression:"can do"
            }),
            (prog2_in_s0:Progression{
                story_id:"150313-story",
                principal_name:"KADEM",
                actual_progression:"got a thing"
            }),
            (plan0_in_s0:Plan{
                story_id:"150313-story",
                principal_name:"Anna Diakon",
                actual_plan:"wants this"
            }),
            (plan1_in_s0:Plan{
                story_id:"150313-story",
                principal_name:"Xarth",
                actual_plan:"will go"
            }),
            (plan2_in_s0:Plan{
                story_id:"150313-story",
                principal_name:"KADEM",
                actual_plan:"plans devious act"
            }),
            (p0_in_s0)-[:CO_CREATED]->(st0),
            (p1_in_s0)-[:CO_CREATED]->(st0),
            (gm0_in_s0)-[:CO_CREATED]->(st0),
            (gm1_in_s0)-[:CO_CREATED]->(st0),
            (m0_in_s0)-[:HAPPENED_DURING]->(st0),
            (m1_in_s0)-[:HAPPENED_DURING]->(st0),
            (st0)-[:RESULTED_IN]->(prog0_in_s0),
            (a_p0_in_s0)-[:GAINED]->(prog0_in_s0),
            (st0)-[:RESULTED_IN]->(prog1_in_s0),
            (a_p1_in_s0)-[:GAINED]->(prog1_in_s0),
            (st0)-[:RESULTED_IN]->(prog2_in_s0),
            (f_p2_in_s0)-[:GAINED]->(prog2_in_s0),
            (st0)-[:CAUSED_PLAN]->(plan0_in_s0),
            (a_plan0_in_s0)-[:HAS_PLAN]->(plan0_in_s0),
            (st0)-[:CAUSED_PLAN]->(plan1_in_s0),
            (a_plan1_in_s0)-[:HAS_PLAN]->(plan1_in_s0),
            (st0)-[:CAUSED_PLAN]->(plan2_in_s0),
            (f_plan2_in_s0)-[:HAS_PLAN]->(plan2_in_s0),
            (a_m0_in_s0)-[:DID]->(m0_in_s0),
            (a_m1_in_s0)-[:DID]->(m1_in_s0),
            (st0)-[:SUMMARIZED_AS]->(sm0),
            (st0)-[:BELONGS_TO]->(c0)
        """)

    # When
    actual_query = create_story_ecosystem(story)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__create_campaign_with_whole_ecosystem():

    # Given data
    campaign_id = "nicaretta"
    title = "Nicaretta"
    prev_campaign_id = "ucieczka-do-przodka"
    body = "\n-\n"

    campaign = create_campaign_with_default_params(uid=campaign_id, prev_campaign_uid=prev_campaign_id, title=title,
                                                   body=body)
    campaign.populated_data[KEY_campaign_position_in_index] = 5

    # Expecting results
    expected_query = dedent("""\
        CREATE
            (c0:Campaign{
                uid:"nicaretta",
                title:"Nicaretta",
                body:"
        -
        ",
                index_position: 5,
                description:""
            })
        """)

    # When
    actual_query = create_campaign_ecosystem([campaign])

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_stories_with_chrono_predecessors__two_predecessors():
    """
    Story is an entity containing relevant fields:
    story.previous_chrono_story_ids()
    ASSUMPTION: Will never be fired for empty previous_chrono_story_ids
    CONSTRAINT: This form of query is important, as we don't build a cartesian product of all s with c.
    """

    # Given data
    story_id = "160723-czyj-jest-kompleks-centralny"
    previous_story_ids = ["160713-ukrasc-ze-swiecy-zajcewow", "160707-mala-szara-myszka"]

    story = create_story_with_default_params(uid=story_id, previous_story_ids_chrono=previous_story_ids)

    # Expecting results
    expected_query = dedent("""\
        MATCH(current_story:Story {uid:'160723-czyj-jest-kompleks-centralny'})
        MATCH(story1:Story {uid:'160713-ukrasc-ze-swiecy-zajcewow'})
        MATCH(story2:Story {uid:'160707-mala-szara-myszka'})
        CREATE (current_story)-[:CHRONOLOGICALLY_AFTER]->(story1)
        CREATE (current_story)-[:CHRONOLOGICALLY_AFTER]->(story2)
        """)

    # When
    actual_query = link_single_story_node_with_chronological_predecessors(story)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_stories_with_chrono_predecessors__one_predecessor():
    """
    Story is an entity containing relevant fields:
    story.previous_chrono_story_ids()
    ASSUMPTION: Will never be fired for empty previous_chrono_story_ids
    CONSTRAINT: This form of query is important, as we don't build a cartesian product of all s with s.
    """

    # Given data
    story_id = "141218-portret-boga"
    previous_story_id = "131008-moj-aniol"
    story = create_story_with_default_params(uid=story_id, previous_story_ids_chrono=[previous_story_id])

    # Expecting results
    expected_query = dedent("""\
        MATCH(current_story:Story {uid:'141218-portret-boga'})
        MATCH(story1:Story {uid:'131008-moj-aniol'})
        CREATE (current_story)-[:CHRONOLOGICALLY_AFTER]->(story1)
        """)

    # When
    actual_query = link_single_story_node_with_chronological_predecessors(story)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_story_with_campaign_predecessor():
    """
    Story is an entity containing relevant fields:
    story.previous_campaign_story_id()
    CONSTRAINT: This form of query is important, as we don't build a cartesian product of all s with s.
    """

    # Given data
    story_id = "141218-portret-boga"
    previous_story_id = "131008-moj-aniol"
    story = create_story_with_default_params(uid=story_id, prev_story_id_campgn=previous_story_id)

    # Expecting results
    expected_query = dedent("""\
        MATCH(current_story:Story {uid:'141218-portret-boga'})
        MATCH(previous_story:Story {uid:'131008-moj-aniol'})
        CREATE (current_story)-[:IN_CAMPAIGN_AFTER]->(previous_story)
        """)

    # When
    actual_query = link_single_story_node_with_campaign_predecessor(story)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_campaign_with_campaign_predecessor():
    # Given data
    campaign_id = "nicaretta"
    previous_campaign_id = "ucieczka-do-przodka"
    campaign = create_campaign_with_default_params(uid=campaign_id, prev_campaign_uid=previous_campaign_id)

    # Expecting results
    expected_query = dedent("""\
        MATCH(current_campaign:Campaign {uid:'nicaretta'})
        MATCH(previous_campaign:Campaign {uid:'ucieczka-do-przodka'})
        CREATE (current_campaign)-[:CAMPAIGN_IS_AFTER]->(previous_campaign)
        """)

    # When
    actual_query = link_campaign_with_predecessor(campaign)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__update_missing_faction_uid():

    # Given data
    faction = create_faction_with_defaults(name="abc", body="", uid="")

    expected_query = dedent("""\
        MATCH (f:Faction{name:'abc'})
        SET f.uid = 'abc'
        RETURN f
        """)

    # When
    actual_query = update_missing_faction_uid(faction)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__update_missing_actor_uid():

    # Given data
    actor = create_actor_with_defaults(name="Jan Kowalski", body="", uid="")

    expected_query = dedent("""\
        MATCH (a:Actor{name:'Jan Kowalski'})
        SET a.uid = '9999-jan-kowalski'
        RETURN a
        """)

    # When
    actual_query = update_missing_actor_uid(actor)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__create_faction_with_whole_ecosystem():

    # Given data
    faction1 = create_faction_with_defaults(uid='uid1', name='name1', body='body1 part1\nand part2')
    faction2 = create_faction_with_defaults(uid='uid2', name='name2', body='body2 part1\nand part2')
    factions = [faction1, faction2]

    expected_query = dedent("""\
        CREATE
            (f0:Faction{
                uid:"uid1",
                name:"name1",
                body:"body1 part1
        and part2"
            }),
            (f1:Faction{
                uid:"uid2",
                name:"name2",
                body:"body2 part1
        and part2"
            })
        """)

    # When
    actual_query = create_factions_ecosystem(factions)

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_unlinked_actors_with_default_player():
    # Given
    expected_query = dedent("""\
        MATCH (p:Player), (a:Actor)
        WHERE p.name = 'Publiczny' AND NOT (a)-[:OWNS]-(:Player)
        MERGE (a)<-[:OWNS]-(p)
        """)

    # When
    actual_query = link_unlinked_actors_with_default_player()

    # Then
    assert(expected_query == actual_query)


def test__neo4j_queries__link_actors_with_factions():
    # Given
    surname = 'Wąż'
    faction = 'Powiew Świeżości'
    expected_query = dedent("""\
        MATCH (a:Actor)
        WHERE a.name =~'.*Wąż'
        MERGE (f:Faction {name: 'Powiew Świeżości'})
        MERGE (a)-[:IS_IN]->(f)
        RETURN a, f
        """)

    # When
    actual_query = link_actors_with_factions(surname, faction)

    # Then
    assert(expected_query == actual_query)
    

def test__neo4j_queries__detach_delete_all_nodes():
    # Given
    expected_query = dedent("""\
        MATCH (n) DETACH DELETE n
        """)
    
    # When
    actual_query = detach_delete_all_nodes()
    
    # Then
    assert(expected_query == actual_query)
