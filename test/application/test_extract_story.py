from src.application.entities.stories.extract import TimeRecord
from src.application.entities.stories.extract import extract_time_record_from_text, extract_previous_chrono_story_ids_from_text
from src.application.entities import config


def test_extract_chronologic_predecessors_should_ignore_logic_predecessors():

    # Expecting results
    actual_chrono_id = "151110-chrono-id"

    # Given data
    prev_logic_id, prev_campaign_id = "151110-logic-id", "151110-camp-id"
    chrono_record = "* [previous name](%s.html)\n### Inne\n"
    chrono_body = "\n## Kontynuacja\n### Chronologiczna\n\n" + chrono_record % actual_chrono_id + \
                  chrono_record % prev_logic_id + chrono_record % prev_campaign_id
    story_body = "kasdjadjasdj" + chrono_body + "sadadjaskljdaskjdas + \n # dsdsds "

    # When
    chrono_predecessors = extract_previous_chrono_story_ids_from_text(story_body, actual_chrono_id)

    # Then
    assert (len(chrono_predecessors) == 1)
    assert (actual_chrono_id == chrono_predecessors[0])


def test_extract_time_no_data():

    # Given data
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]

    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign +\
        "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (players[0], players[1], gms[0]) +\
        summary_body + location_body

    # Expecting results
    expected_time = config.default_story_time_record()

    # When
    actual_time = extract_time_record_from_text(story_text)

    # Then
    assert expected_time == actual_time


def test_extract_time_correct_data():

    # Given data
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]
    time_body = "\n# Czas: \n* Opóźnienie: 12\n* Dni: 3\n"

    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign + \
        "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (
        players[0], players[1], gms[0]) + \
        summary_body + location_body + time_body

    # Expecting results
    expected_time = TimeRecord(duration=3, delay=12, is_default=False)

    # When
    actual_time = extract_time_record_from_text(story_text)

    # Then
    assert expected_time == actual_time


def test_extract_time_bad_data():
    # Given data
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]
    time_body = "\n# Czas: \n * Opóźnienie: 12\n * Czas: 3\n"

    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign + \
                 "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (
                     players[0], players[1], gms[0]) + \
                 summary_body + location_body + time_body

    # Expecting results
    expected_time = TimeRecord(duration=1, delay=12, is_default=False)

    # When
    actual_time = extract_time_record_from_text(story_text)

    # Then
    assert expected_time == actual_time


def test_extract_time_no_delay():
    # Given data
    path = "171231-lizard-in-the-darkness.md"
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    previous_id = "151110-previous-id"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]
    time_body = "\n# Czas: \n* Dni: 3\n"

    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign + \
        "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (
        players[0], players[1], gms[0]) + \
        summary_body + location_body + time_body

    # Expecting results
    expected_time = TimeRecord(duration=3, delay=config.default_story_delay(), is_default=False)

    # When
    actual_time = extract_time_record_from_text(story_text)

    # Then
    assert expected_time == actual_time


def test_extract_time_no_duration():
    # Given data
    path = "171231-lizard-in-the-darkness.md"
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    previous_id = "151110-previous-id"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]
    time_body = "\n# Czas: \n * Opóźnienie: 12\n"

    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign + \
        "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (
        players[0], players[1], gms[0]) + \
        summary_body + location_body + time_body

    # Expecting results
    expected_time = TimeRecord(duration=config.default_story_duration(), delay=12, is_default=False)

    # When
    actual_time = extract_time_record_from_text(story_text)

    # Then
    assert expected_time == actual_time
