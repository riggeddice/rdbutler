import datetime

from datetime import timedelta

from src.application.entities.stories.create import create_story_with_default_params
from src.application.entities.stories.story import KEY_story_start_date, KEY_story_end_date
from src.application.usecases.populate.populate_time import populate_time_in_stories_by_chrono_order


def test_populate_time_chrono():
    # Given
    starting_date = datetime.date(year=2010, month=1, day=1)
    story1 = create_story_with_default_params(uid="story1")  # delay 0 duration 1

    story2 = create_story_with_default_params(uid="story2",previous_story_ids_chrono=[story1.unique_id])
    story2.time_record.duration = 2

    story3 = create_story_with_default_params(uid="story3", previous_story_ids_chrono=[story2.unique_id])
    story3.time_record.delay = 2
    story3.time_record.duration = 3

    story4 = create_story_with_default_params(uid="story4")  # delay 2 duration 2
    story4.time_record.delay = 2
    story4.time_record.duration = 3

    stories = [story4, story2, story3, story1]

    # When
    populate_time_in_stories_by_chrono_order(stories, starting_date)

    # Then
    # root
    assert (story1.populated_data[KEY_story_start_date] == starting_date)
    # duration 1
    assert (story1.populated_data[KEY_story_end_date] == (starting_date + timedelta(days=1)))
    # starts next day, so 1+1
    assert (story2.populated_data[KEY_story_start_date] == starting_date + timedelta(days=2))
    # above plus duration of 2
    assert (story2.populated_data[KEY_story_end_date] == (starting_date + timedelta(days=1 + 1 + 2)))
    # above plus starts earliest next day plus delay
    assert (story3.populated_data[KEY_story_start_date] == starting_date + timedelta(days=1 + 1 + 2 + 1 + 2))
    # above plus duration
    assert (story3.populated_data[KEY_story_end_date] == (starting_date + timedelta(days=1 + 1 + 2 + 1 + 2 + 3)))
    # story 4 is its own root, but delayed by 2
    assert (story4.populated_data[KEY_story_start_date] == starting_date + timedelta(days=2))
    # delay 2, duration  3
    assert (story4.populated_data[KEY_story_end_date] == (starting_date + timedelta(days=2+3)))
