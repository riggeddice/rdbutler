import pytest

from src.application.dataclasses.actor_autogen_features import create_actor_autogen_feature_with_defaults
from src.application.dataclasses.emotions import EmotionWheel
from src.application.usecases.randomize.faceless_actor import pick_name, pick_emotion, pick_professions


@pytest.mark.parametrize('randint_list, categorized_names, expected', [
    ([1, 3, 4, 5, 6], {"man": [("Tomek", 3), ("Robert", 1), ("Wojtek", 2)]}, ["Tomek", "Tomek", "Robert", "Wojtek", "Wojtek"])
])
def test_pick_name(randint_list, categorized_names, expected):
    # Setup
    generator = (n for n in randint_list)
    pick_name.test_injected_randint = lambda _1, _2: next(generator)

    # Given
    aaf = create_actor_autogen_feature_with_defaults(categorized_names=categorized_names)
    category = list(categorized_names.keys())[0]

    # When
    actual = [pick_name(aaf=aaf, category=category) for _ in range(len(randint_list))]

    # Then
    assert(expected == actual)

    # Teardown
    pick_name.__dict__.pop("test_injected_randint")


@pytest.mark.parametrize('coded_profession_list, ranchoice_list', [
    ([("121101", "Główny księgowy"), ("121190", "Pozostali kierownicy do spraw finansowych"), ("751501", "Grzyboznawca")],
      ["Główny księgowy", "Grzyboznawca"])
])
def test_pick_profession(coded_profession_list, ranchoice_list):
    # For now this test does not have much to it; however, when the logic starts being interesting
    # we will not test 'ranchoice' function anymore.

    # Setup
    generator = (n for n in ranchoice_list)
    pick_professions.test_injected_choice = lambda _: next(generator)

    # Given
    aaf = create_actor_autogen_feature_with_defaults(professions=coded_profession_list)

    # When
    actual = pick_professions(aaf=aaf)

    # Then
    assert (ranchoice_list == actual)

    # Teardown
    pick_professions.__dict__.pop("test_injected_choice")


def test_pick_emotion():
    # Setup
    ranchoice_list = [1, 13, 5]
    generator = (n for n in ranchoice_list)
    pick_emotion.test_injected_randint = lambda _1, _2: next(generator)

    # Given
    dyads = [("pride", "anger", "joy", "7"), ("delight", "joy", "surprise", "4")]
    base_emotions = [("anger", "10", "annoyance", "4", "rage", "2"), ("joy", "10", "serenity", "4", "extasy", "2")]
    aaf = create_actor_autogen_feature_with_defaults(emotions=EmotionWheel(base_emotions=base_emotions, dyads=dyads))

    # When
    actual = pick_emotion(aaf)

    # Then
    assert ('pride (anger + joy), anger -> annoyance, joy -> joy' == actual)

    # Teardown
    pick_emotion.__dict__.pop("test_injected_randint")


