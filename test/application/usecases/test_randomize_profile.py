from src.application.usecases.randomize.profile import generate_items_with_aspects
from src.external.neo4j.queries.db_query_structures import AspectWithItem
from src.rpg_domain.invasion.entities.skills.skill import Skill


def test_randomize_profile_for_no_duplicates():
    # Given
    skills = [
        Skill(name="one", aspects=[]),
        Skill(name="two", aspects=[]),
        Skill(name="three", aspects=[]),
        Skill(name="four", aspects=[]),
        Skill(name="five", aspects=[]),
        Skill(name="six", aspects=[]),
        Skill(name="seven", aspects=[]),
        Skill(name="eight", aspects=[]),
        Skill(name="nine", aspects=[])
    ]

    aspects = [
        AspectWithItem(aspect="a1", item="one"),
        AspectWithItem(aspect="a2", item="one"),
        AspectWithItem(aspect="a3", item="one"),
        AspectWithItem(aspect="a4", item="one"),
        AspectWithItem(aspect="a5", item="one"),
        AspectWithItem(aspect="a6", item="two"),
        AspectWithItem(aspect="a7", item="two"),
        AspectWithItem(aspect="a8", item="two"),
        AspectWithItem(aspect="a9", item="three"),
        AspectWithItem(aspect="a10", item="three"),
        AspectWithItem(aspect="a11", item="three"),
        AspectWithItem(aspect="a12", item="four"),
        AspectWithItem(aspect="a13", item="three")
    ]

    for _ in range(1000):
        # When
        selected = generate_items_with_aspects(skills, 5, aspects, 3)

        # Then
        assert len(selected) == len(set(selected))


def test_randomize_profile_requested_more_than_had():
    # Given
    skills = [
        Skill(name="one", aspects=[]),
        Skill(name="two", aspects=[]),
        Skill(name="three", aspects=[])
        ]

    aspects = [
        AspectWithItem(aspect="a1", item="one"),
        AspectWithItem(aspect="a2", item="one"),
        AspectWithItem(aspect="a3", item="one"),
        AspectWithItem(aspect="a4", item="one"),
        AspectWithItem(aspect="a5", item="one")
    ]

    # When
    selected = generate_items_with_aspects(skills, 15, aspects, 15)

    # Then
    assert len(selected) == 3
    assert len(selected[0].aspects) == 5
