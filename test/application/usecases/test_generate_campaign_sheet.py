from textwrap import dedent

from datetime import date

from src.application.entities.actors.create import create_actor_with_defaults
from src.application.usecases.generate.generate_campaign_sheet import generate_campaign_sheet
from src.external.neo4j.queries.db_query_structures import CampaignInfo, StoryDataWithCampaignLink, \
    ActualProgressionInContext, ActualPlanInContext

# Common data
_actors = [
    create_actor_with_defaults(name="Test Toon", uid= "1709-test-toon"),
    create_actor_with_defaults(name="Jan Kowalski", uid= "1605-jan-kowalski")
]

_story_data_with_campaigns = [
    StoryDataWithCampaignLink(
        "170801-the-story1",
        "Story1",
        "Story 1 summary",
        date(year=2010, month=1, day=3),
        date(year=2010, month=1, day=5),
        "my-campaign"
    ),
    StoryDataWithCampaignLink(
        "170801-the-story2",
        "Story2",
        "Story 2 summary",
        date(year=2010, month=1, day=6),
        date(year=2010, month=1, day=8),
        "my-campaign"
    ),
    StoryDataWithCampaignLink(
        "170801-the-story21",
        "Story21",
        "Story 1 summary",
        date(year=2010, month=1, day=3),
        date(year=2010, month=1, day=5),
        "not-my-campaign"
    )
]

_progressions = [
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1709-test-toon",
                               "Test Toon",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1605-jan-kowalski",
                               "Jan Kowalski",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Is now powerful",
                               "my-faction",
                               "My Faction",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story21",
                               "Story21",
                               "Gained a thing",
                               "1605-jan-kowalski",
                               "Jan Kowalski",
                               "campaign")
]

_campaign = CampaignInfo(
        "my-campaign",
        "My Campaign",
        "previous-campaign",
        "Previous Campaign",
        "The description of my campaign"
    )

_plans = [
    ActualPlanInContext("170801-the-story1",
                       "Story1",
                       "Will act",
                       "1605-jan-kowalski",
                       "Jan Kowalski",
                       "my-campaign")
]


def test_generate_campaign_sheet_proper_data():
    # Given

    # All default data

    # Expected results
    campaign_text_expected = dedent("""\
        ---
        layout: default
        categories: inwazja, campaign
        title: "My Campaign"
        ---
        
        # Kampania: {{ page.title }}
        
        ## Kontynuacja
        
        * [Previous Campaign](kampania-previous-campaign.html)
        
        ## Opis
        
        The description of my campaign
        
        # Historia
        
        1. [Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html): 10/01/03 - 10/01/05
        (170801-the-story1)
        
        Story 1 summary
        
        1. [Story2](/rpg/inwazja/opowiesci/konspekty/170801-the-story2.html): 10/01/06 - 10/01/08
        (170801-the-story2)
        
        Story 2 summary
        
        ## Progresja
        
        |Misja|Progresja|
        |------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|My Faction Is now powerful|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|[Jan Kowalski](/rpg/inwazja/opowiesci/karty-postaci/1605-jan-kowalski.html) Gained a thing|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|[Test Toon](/rpg/inwazja/opowiesci/karty-postaci/1709-test-toon.html) Gained a thing|
        
        ## Plany

        |Misja|Plan|
        |-----|-----|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|[Jan Kowalski](/rpg/inwazja/opowiesci/karty-postaci/1605-jan-kowalski.html) Will act|
        
    """)

    # When

    campaign_text_actual = generate_campaign_sheet(_campaign, _story_data_with_campaigns, _progressions, _plans, _actors)

    # Then

    assert campaign_text_expected == campaign_text_actual


def test_generate_campaign_sheet_no_progressions_no_plans():
    # Given
    _progressions = []
    _plans = []

    # Expected results
    campaign_text_expected = dedent("""\
        ---
        layout: default
        categories: inwazja, campaign
        title: "My Campaign"
        ---
        
        # Kampania: {{ page.title }}
        
        ## Kontynuacja
        
        * [Previous Campaign](kampania-previous-campaign.html)
        
        ## Opis
        
        The description of my campaign
        
        # Historia
        
        1. [Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html): 10/01/03 - 10/01/05
        (170801-the-story1)
        
        Story 1 summary
        
        1. [Story2](/rpg/inwazja/opowiesci/konspekty/170801-the-story2.html): 10/01/06 - 10/01/08
        (170801-the-story2)
        
        Story 2 summary
        
    """)

    # When

    campaign_text_actual = generate_campaign_sheet(_campaign, _story_data_with_campaigns, _progressions, _plans, _actors)

    # Then
    assert campaign_text_expected == campaign_text_actual


def test_generate_campaign_sheet_no_actors():
    # Given
    _actors = []

    # Expected results
    campaign_text_expected = dedent("""\
        ---
        layout: default
        categories: inwazja, campaign
        title: "My Campaign"
        ---

        # Kampania: {{ page.title }}

        ## Kontynuacja

        * [Previous Campaign](kampania-previous-campaign.html)

        ## Opis

        The description of my campaign

        # Historia

        1. [Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html): 10/01/03 - 10/01/05
        (170801-the-story1)

        Story 1 summary

        1. [Story2](/rpg/inwazja/opowiesci/konspekty/170801-the-story2.html): 10/01/06 - 10/01/08
        (170801-the-story2)

        Story 2 summary

        ## Progresja

        |Misja|Progresja|
        |------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|My Faction Is now powerful|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Jan Kowalski Gained a thing|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Test Toon Gained a thing|

        ## Plany

        |Misja|Plan|
        |-----|-----|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Jan Kowalski Will act|
        
    """)

    # When

    campaign_text_actual = generate_campaign_sheet(_campaign, _story_data_with_campaigns, _progressions, _plans, _actors)

    # Then
    assert campaign_text_expected == campaign_text_actual
