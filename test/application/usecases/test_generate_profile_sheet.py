from textwrap import dedent

from datetime import date

from src.application.entities.actors.create import create_actor_with_defaults
from src.application.usecases.generate.generate_profile_sheet import generate_profile_sheet
from src.external.neo4j.queries.db_query_structures import OwnershipInfo, MeetingOf2Actors, ExtendedMeritInfo, \
    ActorToFactionLink, ActualProgressionInContext, ActualPlanInContext

# Common data
_body = dedent("""\
        This is a body.
        Best ever.
        Never Had better""")

_ownerships = [
    OwnershipInfo("Test Toon", "1709-test-toon", "player1"),
    OwnershipInfo("Jan Kowalski", "1605-jan-kowalski", "player2")
]

_meetings = [
    MeetingOf2Actors("Test Toon",
                     "1709-test-toon",
                     "Jan Kowalski",
                     "1605-jan-kowalski",
                     "170801-the-story1",
                     "Story1",
                     date(year=2010, month=1, day=3),
                     "campaign"),
    MeetingOf2Actors("Jan Kowalski",
                     "1605-jan-kowalski",
                     "Test Toon3",
                     "1709-test-toon3",
                     "170801-the-story1",
                     "Story1",
                     date(year=2010, month=1, day=3),
                     "campaign")
]

_merits = [
    ExtendedMeritInfo("Test Toon",
                      "170801-the-story1",
                      "Story1",
                      date(year=2010, month=1, day=3),
                      date(year=2010, month=1, day=5),
                      "Got what he wanted",
                      "campaign",
                      "Campaign")
]

_actor_to_faction_links = [
    ActorToFactionLink("Test Toon", "1709-test-toon", "Faction1"),
    ActorToFactionLink("Test Toon", "1709-test-toon", "Faction2")
]

_progressions = [
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1709-test-toon",
                               "Test Toon",
                               "campaign"),
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1605-jan-kowalski",
                               "Jan Kowalski",
                               "campaign")
]
_plans =[
    ActualPlanInContext("170801-the-story1",
                        "Story1",
                        "Will act",
                        "1709-test-toon",
                        "Test Toon",
                        "campaign")
]


def test_generate_profile_sheet_proper_data():
    # Given
    actor = create_actor_with_defaults(
        actor_type="NPC",
        name="Test Toon",
        uid="1709-test-toon",
        body=_body
    )

    # Expected results
    expected_profile_text = dedent("""\
        ---
        layout: inwazja-karta-postaci
        categories: profile
        factions: "Faction1, Faction2"
        type: "NPC"
        owner: "player1"
        title: "Test Toon"
        ---
        This is a body.
        Best ever.
        Never Had better
        
        # Historia:
        ## Progresja
        
        |Misja|Progresja|Kampania|
        |-----|------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Gained a thing|campaign|
        
        ## Plany
        
        |Misja|Plan|Kampania|
        |-----|------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Will act|campaign|
        
        ## Dokonania:
        
        |Data|Dokonanie|Misja|Początek|Koniec|Kampania|
        |-----|------|------|-----|------|------|
        |170801|Got what he wanted|[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|10/01/03|10/01/05|[Campaign](/rpg/inwazja/opowiesci/konspekty/kampania-campaign.html)|
        
        
        ## Relacje z postaciami:
        
        |Z kim|Intensywność|Na misjach|
        |-----|------|------|
        |[Jan Kowalski](/rpg/inwazja/opowiesci/karty-postaci/1605-jan-kowalski.html)|1|[170801](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|
    """)

    # When

    actual_profile_text = generate_profile_sheet(_actor_to_faction_links, _meetings, _merits, actor,
                                                 _ownerships, _progressions, _plans)

    # Then

    assert expected_profile_text == actual_profile_text


def test_generate_profile_sheet_empty_body():
    # Given

    actor = create_actor_with_defaults(
        actor_type="NPC",
        name="Test Toon",
        uid="1709-test-toon"
    )

    # Expected results
    expected_profile_text = dedent("""\
        ---
        layout: inwazja-karta-postaci
        categories: profile
        factions: "Faction1, Faction2"
        type: "NPC"
        owner: "player1"
        title: "Test Toon"
        ---
        # {{ page.title }}

        # Historia:
        ## Progresja
        
        |Misja|Progresja|Kampania|
        |-----|------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Gained a thing|campaign|
        
        ## Plany
        
        |Misja|Plan|Kampania|
        |-----|------|------|
        |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Will act|campaign|
        
        ## Dokonania:
        
        |Data|Dokonanie|Misja|Początek|Koniec|Kampania|
        |-----|------|------|-----|------|------|
        |170801|Got what he wanted|[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|10/01/03|10/01/05|[Campaign](/rpg/inwazja/opowiesci/konspekty/kampania-campaign.html)|
        
        
        ## Relacje z postaciami:
        
        |Z kim|Intensywność|Na misjach|
        |-----|------|------|
        |[Jan Kowalski](/rpg/inwazja/opowiesci/karty-postaci/1605-jan-kowalski.html)|1|[170801](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|
    """)

    # When

    actual_profile_text = generate_profile_sheet(_actor_to_faction_links, _meetings, _merits, actor,
                                                 _ownerships, _progressions, _plans)

    # Then

    assert expected_profile_text == actual_profile_text


def test_generate_profile_sheet_no_data():
        # Given
        merits = []
        progressions = []
        plans = []
        meetings = []

        actor = create_actor_with_defaults(
            actor_type="NPC",
            name="Test Toon",
            uid="1709-test-toon"
        )

        # Expected results
        expected_profile_text = dedent("""\
            ---
            layout: inwazja-karta-postaci
            categories: profile
            factions: "Faction1, Faction2"
            type: "NPC"
            owner: "player1"
            title: "Test Toon"
            ---
            # {{ page.title }}
            
            # Historia:
        """)

        # When

        actual_profile_text = generate_profile_sheet(_actor_to_faction_links, meetings, merits, actor,
                                                     _ownerships, progressions, plans)

        # Then

        assert expected_profile_text == actual_profile_text