from src.application.usecases.aggregate.aggregate_locations import merge_locations_only_headers, \
    extract_headers_from_lines, create_location_records_from_headers, LocationRecord, merge_location_records


def test_merge_locations__two_simple_trees():

    # Given data

    location_1 = """1. Świat
    1. Primus
        1. Śląsk, X happened
        1. Mazowsze, Y happened
    1. Faza Daemonica
        1. Mare Vortex"""

    location_2 = """1. Świat
    1. Primus
        1. Mazowsze, A h,appe , ned, ok
        1. Śląsk, B happened
            1. Cat County, C happened
    1. Esuriit
        1. Island of Hunger"""

    # Expecting results

    location_expected = """1. Świat
    1. Esuriit
        1. Island Of Hunger
    1. Faza Daemonica
        1. Mare Vortex
    1. Primus
        1. Mazowsze
        1. Śląsk
            1. Cat County"""

    # When
    location_actual = merge_locations_only_headers(location_1, location_2)

    # Then
    assert (location_expected == location_actual)


def test_merge_location_records():

    # Given data

    tree_1 = [
        LocationRecord(header="1. Świat", path_to_parent="1. Świat", path_to_self="1. Świat"),
        LocationRecord(header="    1. Primus", path_to_parent="1. Świat", path_to_self="1. Świat1. Primus"),
        LocationRecord(header="        1. Śląsk", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Śląsk"),
        LocationRecord(header="        1. Mazowsze", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Mazowsze"),
        LocationRecord(header="    1. Faza Daemonica", path_to_parent="1. Świat", path_to_self="1. Świat1. Faza Daemonica"),
        LocationRecord(header="        1. Mare Vortex", path_to_parent="1. Świat1. Faza Daemonica", path_to_self="1. Świat1. Faza Daemonica1. Mare Vortex"),
    ]

    tree_2 = [
        LocationRecord(header="1. Świat", path_to_parent="1. Świat", path_to_self="1. Świat"),
        LocationRecord(header="    1. Esuriit", path_to_parent="1. Świat", path_to_self="1. Świat1. Esuriit"),
        LocationRecord(header="        1. Island of Hunger", path_to_parent="1. Świat1. Esuriit", path_to_self="1. Świat1. Esuriit1. Island of Hunger"),
        LocationRecord(header="    1. Primus", path_to_parent="1. Świat", path_to_self="1. Świat1. Primus"),
        LocationRecord(header="        1. Śląsk", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Śląsk"),
        LocationRecord(header="            1. Cat County", path_to_parent="1. Świat1. Primus1. Śląsk", path_to_self="1. Świat1. Primus1. Śląsk1. Cat County"),
        LocationRecord(header="        1. Mazowsze", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Mazowsze")
    ]

    # Expecting data

    tree_expected = [
        LocationRecord(header="1. Świat", path_to_parent="1. Świat", path_to_self="1. Świat"),
        LocationRecord(header="    1. Esuriit", path_to_parent="1. Świat", path_to_self="1. Świat1. Esuriit"),
        LocationRecord(header="        1. Island of Hunger", path_to_parent="1. Świat1. Esuriit", path_to_self="1. Świat1. Esuriit1. Island of Hunger"),
        LocationRecord(header="    1. Faza Daemonica", path_to_parent="1. Świat", path_to_self="1. Świat1. Faza Daemonica"),
        LocationRecord(header="        1. Mare Vortex", path_to_parent="1. Świat1. Faza Daemonica", path_to_self="1. Świat1. Faza Daemonica1. Mare Vortex"),
        LocationRecord(header="    1. Primus", path_to_parent="1. Świat", path_to_self="1. Świat1. Primus"),
        LocationRecord(header="        1. Mazowsze", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Mazowsze"),
        LocationRecord(header="        1. Śląsk", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Śląsk"),
        LocationRecord(header="            1. Cat County", path_to_parent="1. Świat1. Primus1. Śląsk", path_to_self="1. Świat1. Primus1. Śląsk1. Cat County"),

    ]

    # When
    tree_actual = merge_location_records(tree_1, tree_2)

    # Then
    for i in range(0, len(tree_expected)):
        assert (tree_expected[i].all_fields() == tree_actual[i].all_fields())


def test_create_location_records_from_headers():

    # Given data

    lines = [
        "1. Świat",
        "    1. Primus",
        "        1. Śląsk",
        "        1. Mazowsze",
        "    1. Faza Daemonica",
        "        1. Mare Vortex"]

    # Expecting data

    records_expected = [
        LocationRecord(header="1. Świat", path_to_parent="1. Świat", path_to_self= "1. Świat"),
        LocationRecord(header="    1. Primus", path_to_parent="1. Świat", path_to_self="1. Świat1. Primus"),
        LocationRecord(header="        1. Śląsk", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Śląsk"),
        LocationRecord(header="        1. Mazowsze", path_to_parent="1. Świat1. Primus", path_to_self="1. Świat1. Primus1. Mazowsze"),
        LocationRecord(header="    1. Faza Daemonica", path_to_parent="1. Świat", path_to_self="1. Świat1. Faza Daemonica"),
        LocationRecord(header="        1. Mare Vortex", path_to_parent="1. Świat1. Faza Daemonica", path_to_self="1. Świat1. Faza Daemonica1. Mare Vortex"),
    ]

    # When

    records_actual = create_location_records_from_headers(lines)

    # Then
    for i in range(0, len(lines)):
        assert (records_expected[i].all_fields() == records_actual[i].all_fields())


def test_extract_headers_from_records():

    # Given data
    tree = """1. Świat
        1. Primus
            1. Śląsk, X happened
            1. Mazowsze, Y, A, B happened
        1. Faza Daemonica, Z happened
            1. Mare Vortex"""

    tree_lines = tree.split("\n")

    # Expecting data
    tree_expected = """1. Świat
        1. Primus
            1. Śląsk
            1. Mazowsze
        1. Faza Daemonica
            1. Mare Vortex"""

    tree_lines_expected = tree_expected.split("\n")

    # When

    tree_lines_actual = extract_headers_from_lines(tree_lines_expected)

    # Then

    for i in range(0, len(tree_lines_expected)):
        assert (tree_lines_expected[i] == tree_lines_actual[i])
