from textwrap import dedent

from src.application.entities.factions.create import create_faction_with_defaults
from src.application.usecases.generate.generate_faction_sheet import generate_faction_sheet
from src.external.neo4j.queries.db_query_structures import ActualProgressionInContext, ActualPlanInContext

# Common data
_progressions = [
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1709-test-toon",
                               "Test Toon",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Gained a thing",
                               "1605-jan-kowalski",
                               "Jan Kowalski",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story1",
                               "Story1",
                               "Is now powerful",
                               "my-faction",
                               "My Faction",
                               "my-campaign"),
    ActualProgressionInContext("170801-the-story21",
                               "Story21",
                               "Gained a thing",
                               "1605-jan-kowalski",
                               "Jan Kowalski",
                               "campaign")
]

_faction_body = dedent("""\
    This is faction body.
    It's awesome.
    """)

_faction = create_faction_with_defaults(name="My Faction", uid="my-faction", body=_faction_body)
_faction_default_body = create_faction_with_defaults(name="My Faction", uid="my-faction")
_plans = [
    ActualPlanInContext("170801-the-story1",
                        "Story1",
                        "has plans",
                        "my-faction",
                        "My Faction",
                        "campaign")
]


def test_generate_faction_sheet_proper_data():
    # Given

    # All default data

    # Expected results
    faction_text_expected = dedent("""\
    ---
    layout: default
    categories: faction
    title: "My Faction"
    ---
    This is faction body.
    It's awesome.
    

    # Historia:
    ## Progresja
    
    |Misja|Progresja|Kampania|
    |-----|------|------|
    |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Is now powerful|my-campaign|

    ## Plany
    
    |Misja|Plan|Kampania|
    |-----|------|------|
    |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|has plans|campaign|
    
    """)

    # When

    faction_text_actual = generate_faction_sheet(_faction, _progressions, _plans)

    # Then

    assert faction_text_expected == faction_text_actual


def test_generate_faction_sheet_no_body():
    # Given

    # All default data

    # Expected results
    faction_text_expected = dedent("""\
    ---
    layout: default
    categories: faction
    title: "My Faction"
    ---
    # {{ page.title }}

    # Historia:
    ## Progresja

    |Misja|Progresja|Kampania|
    |-----|------|------|
    |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|Is now powerful|my-campaign|

    ## Plany
    
    |Misja|Plan|Kampania|
    |-----|------|------|
    |[Story1](/rpg/inwazja/opowiesci/konspekty/170801-the-story1.html)|has plans|campaign|
    
    """)

    # When

    faction_text_actual = generate_faction_sheet(_faction_default_body, _progressions, _plans)

    # Then

    assert faction_text_expected == faction_text_actual


def test_generate_faction_sheet_no_data():
    # Given

    progressions = []
    plans = []

    # Expected results
    faction_text_expected = dedent("""\
    ---
    layout: default
    categories: faction
    title: "My Faction"
    ---
    This is faction body.
    It's awesome.


    # Historia:
    """)

    # When

    faction_text_actual = generate_faction_sheet(_faction, progressions, plans)

    # Then

    assert faction_text_expected == faction_text_actual
