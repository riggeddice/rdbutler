from src.application.usecases.normalize.normalize_story import normalize_story_location_section
from src.application.entities import config


def test_normalize_location__no_root():

    # Given data

    no_depth_zero_root_section = """    1. Świat"""
    misnamed_root_section = """1. NotŚwiat"""

    # Expecting results

    section_expected = config.default_location_in_story()

    # When

    section_actual_1 = normalize_story_location_section(no_depth_zero_root_section)
    section_actual_2 = normalize_story_location_section(misnamed_root_section)

    # Then
    assert (section_expected == section_actual_1 == section_actual_2)


def test_normalize_location__proper_record():

    # Given data

    input_section = """

1. Świat
    1. Allitras
        1. Trimektil
            1. Jezioro Błysków, na stałe sprzężone z Wieżą Wichrów w Powiecie Ciemnowężnym (Gnuśnów Letni).
    1. Primus
        1. Mazowsze
            1. Powiat Ciemnowężny
                1. Gnuśnów Letni, który kiedyś słynął z nadmiernego występowania południc
                    
"""

    # Expecting results

    section_expected = """1. Świat
    1. Allitras
        1. Trimektil
            1. Jezioro Błysków, na stałe sprzężone z Wieżą Wichrów w Powiecie Ciemnowężnym (Gnuśnów Letni).
    1. Primus
        1. Mazowsze
            1. Powiat Ciemnowężny
                1. Gnuśnów Letni, który kiedyś słynął z nadmiernego występowania południc"""

    # When

    section_actual = normalize_story_location_section(input_section)

    # Then
    assert (section_expected == section_actual)
