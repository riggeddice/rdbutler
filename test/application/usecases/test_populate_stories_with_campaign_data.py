from copy import deepcopy

from src.application.entities.campaigns.create import create_campaign_with_default_params
from src.application.entities.stories.create import create_story_with_default_params
from src.application.entities.stories.story import KEY_story_position_in_campaign, KEY_story_absolute_position
from src.application.usecases.populate.populate_stories import populate_stories_with_campaign_data


def test_populate_stories_with_campaign_data__2_camp_5_stories__opposite_campaign_sequence():

    # Given data
    uids_seq = ["100101-first", "100402-second", "100302-third", "100403-fourth", "100105-fifth"]
    campaign_name_1 = "the-campaign-1"
    campaign_name_2 = "the-campaign-2"

    story_a_2 = create_story_with_default_params(uid=uids_seq[0], title="First_Second", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[2])
    story_b_4 = create_story_with_default_params(uid=uids_seq[1], title="Second_Fourth", campaign=campaign_name_2, prev_story_id_campgn=uids_seq[1])
    story_c_1 = create_story_with_default_params(uid=uids_seq[2], title="Third_First", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[2])
    story_d_5 = create_story_with_default_params(uid=uids_seq[3], title="Fourth_Fifth", campaign=campaign_name_2, prev_story_id_campgn=uids_seq[1])
    story_e_3 = create_story_with_default_params(uid=uids_seq[4], title="Fourth_Third", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[0])

    campaign_1 = create_campaign_with_default_params(uid=campaign_name_1, prev_campaign_uid=campaign_name_1)
    campaign_2 = create_campaign_with_default_params(uid=campaign_name_2, prev_campaign_uid=campaign_name_1)

    stories = [story_a_2, story_b_4, story_c_1, story_d_5, story_e_3]

    # Expecting results

    stories_copied = deepcopy(stories)

    key_relative = KEY_story_position_in_campaign
    stories_copied[0].populated_data[key_relative] = 2
    stories_copied[1].populated_data[key_relative] = 1
    stories_copied[2].populated_data[key_relative] = 1
    stories_copied[3].populated_data[key_relative] = 2
    stories_copied[4].populated_data[key_relative] = 3

    key_absolute = KEY_story_absolute_position
    stories_copied[0].populated_data[key_absolute] = 2
    stories_copied[1].populated_data[key_absolute] = 4
    stories_copied[2].populated_data[key_absolute] = 1
    stories_copied[3].populated_data[key_absolute] = 5
    stories_copied[4].populated_data[key_absolute] = 3

    stories_expect = [stories_copied[2], stories_copied[0], stories_copied[4], stories_copied[1], stories_copied[3]]

    # When

    stories_actual, _ = populate_stories_with_campaign_data(stories=stories, campaigns=[campaign_2, campaign_1])

    # Then

    for i in range(len(stories_expect)):
        assert stories_expect[i] == stories_actual[i]
        assert stories_expect[i].populated_data == stories_actual[i].populated_data


def test_populate_stories_with_campaign_data__2_camp_5_stories__simple_campaign_sequence():

    # Given data
    uids_seq = ["100101-first", "100402-second", "100302-third", "100403-fourth", "100105-fifth"]
    campaign_name_1 = "the-campaign-1"
    campaign_name_2 = "the-campaign-2"

    story_a_2 = create_story_with_default_params(uid=uids_seq[0], title="First_Second", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[2])
    story_b_4 = create_story_with_default_params(uid=uids_seq[1], title="Second_Fourth", campaign=campaign_name_2, prev_story_id_campgn=uids_seq[1])
    story_c_1 = create_story_with_default_params(uid=uids_seq[2], title="Third_First", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[2])
    story_d_5 = create_story_with_default_params(uid=uids_seq[3], title="Fourth_Fifth", campaign=campaign_name_2, prev_story_id_campgn=uids_seq[1])
    story_e_3 = create_story_with_default_params(uid=uids_seq[4], title="Fourth_Third", campaign=campaign_name_1, prev_story_id_campgn=uids_seq[0])

    campaign_1 = create_campaign_with_default_params(uid=campaign_name_1, prev_campaign_uid=campaign_name_1)
    campaign_2 = create_campaign_with_default_params(uid=campaign_name_2, prev_campaign_uid=campaign_name_1)

    stories = [story_a_2, story_b_4, story_c_1, story_d_5, story_e_3]

    # Expecting results

    stories_copied = deepcopy(stories)

    key_relative = KEY_story_position_in_campaign
    stories_copied[0].populated_data[key_relative] = 2
    stories_copied[1].populated_data[key_relative] = 1
    stories_copied[2].populated_data[key_relative] = 1
    stories_copied[3].populated_data[key_relative] = 2
    stories_copied[4].populated_data[key_relative] = 3

    key_absolute = KEY_story_absolute_position
    stories_copied[0].populated_data[key_absolute] = 2
    stories_copied[1].populated_data[key_absolute] = 4
    stories_copied[2].populated_data[key_absolute] = 1
    stories_copied[3].populated_data[key_absolute] = 5
    stories_copied[4].populated_data[key_absolute] = 3

    stories_expect = [stories_copied[2], stories_copied[0], stories_copied[4], stories_copied[1], stories_copied[3]]

    # When

    stories_actual, _ = populate_stories_with_campaign_data(stories=stories, campaigns=[campaign_1, campaign_2])

    # Then

    for i in range(len(stories_expect)):
        assert stories_expect[i] == stories_actual[i]
        assert stories_expect[i].populated_data == stories_actual[i].populated_data


def test_populate_stories_with_campaign_data__1_camp_4_stories():

    # Given data
    uids_seq = ["100101-first", "100402-second", "100302-third", "100403-fourth"]
    campaign_uid = "the-campaign"

    story_a_1 = create_story_with_default_params(uid=uids_seq[0], title="First_First", campaign=campaign_uid, prev_story_id_campgn=uids_seq[0])
    story_b_4 = create_story_with_default_params(uid=uids_seq[1], title="Second_Third", campaign=campaign_uid, prev_story_id_campgn=uids_seq[2])
    story_c_2 = create_story_with_default_params(uid=uids_seq[2], title="Third_Second", campaign=campaign_uid, prev_story_id_campgn=uids_seq[0])
    story_d_3 = create_story_with_default_params(uid=uids_seq[3], title="Fourth_Fourth", campaign=campaign_uid, prev_story_id_campgn=uids_seq[1])

    campaign = create_campaign_with_default_params(uid=campaign_uid)
    stories = [story_a_1, story_b_4, story_c_2, story_d_3]

    # Expecting results

    stories_expect = deepcopy(stories)

    key_relative = KEY_story_position_in_campaign
    stories_expect[0].populated_data[key_relative] = 1
    stories_expect[1].populated_data[key_relative] = 3
    stories_expect[2].populated_data[key_relative] = 2
    stories_expect[3].populated_data[key_relative] = 4

    key_absolute = KEY_story_absolute_position
    stories_expect[0].populated_data[key_absolute] = 1
    stories_expect[1].populated_data[key_absolute] = 3
    stories_expect[2].populated_data[key_absolute] = 2
    stories_expect[3].populated_data[key_absolute] = 4

    stories_expect.sort(key=lambda x: x.populated_data[KEY_story_position_in_campaign])

    # When

    stories_actual, _ = populate_stories_with_campaign_data(stories=stories, campaigns=[campaign])

    # Then

    for i in range(len(stories_expect)):
        assert stories_expect[i] == stories_actual[i]
        assert stories_expect[i].populated_data == stories_actual[i].populated_data
