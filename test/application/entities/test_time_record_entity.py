from src.application.entities.stories.time_record import TimeRecord


def test_time_record_equality_operator_same_data():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = TimeRecord(duration=1, delay=2, is_default=False)
    assert t1 == t2


def test_time_record_equality_operator_same_data_with_superfluous_fields():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = TimeRecord(duration=1, delay=2, is_default=False)
    t2.additional_field = "really? why would anyone do this?"
    assert t1 == t2


def test_time_record_equality_operator_different_duration():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = TimeRecord(duration=3, delay=2, is_default=False)
    assert t1 != t2


def test_time_record_equality_operator_different_delay():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = TimeRecord(duration=1, delay=3, is_default=False)
    assert t1 != t2


def test_time_record_equality_operator_different_is_default():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = TimeRecord(duration=1, delay=2, is_default=True)
    assert t1 != t2


def test_time_record_equality_operator_different_object_type():
    t1 = TimeRecord(duration=1, delay=2, is_default=False)
    t2 = "what were you expecting from this comparison?"
    assert t1 != t2
