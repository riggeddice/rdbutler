from hypothesis import given, strategies, assume
from collections import namedtuple

from src.application.entities.actors.actor import Actor
from src.application.entities.actors.create import create_actor
from src.application.entities.actors.create import create_actors
from src.application.entities import config


@given(strategies.text())
def test_no_matter_the_actor_data_it_will_work_somehow(horrible_text):

    assume(horrible_text)

    # Given data
    path = "1705-adonis-sowinski.md"

    ActorWithPath = namedtuple("ActorWithPath", "entity_text path")
    input_data = [ActorWithPath(entity_text=horrible_text, path=path)]
    mech_ver = path[0:4]

    # Expecting results
    expected_actor = Actor(uid=path[:-3], mechanics_version=mech_ver, name=config.default_actor_name(),
                           factions=config.default_actor_factions(), actor_type=config.default_actor_type(), mechanics="",
                           description="", body=horrible_text.strip(), owner=config.default_owner())

    # When
    actual_actor = create_actors(input_data)[0]

    # Then
    assert actual_actor == expected_actor


def test_create_actor_fully_default_values():

    # Given data
    path = "1705-adonis-sowinski.md"
    body = "This is some random \n multiline text"
    history = "\n\n# Historia"
    front_matter = "---\nlayout: inwazja - karta - postaci" \
                 "\ncategories: profile, srebrna_swieca\n---\n"
    mech_ver = path[0:4]

    # Expecting results
    expected_actor = Actor(uid=path[:-3], mechanics_version=mech_ver, name=config.default_actor_name(),
                           factions=config.default_actor_factions(), actor_type=config.default_actor_type(), mechanics="",
                           description="", body=body, owner=config.default_owner())

    # When
    actual_actor = create_actor(path=path, actor_text=front_matter + body + history)

    # Then
    assert actual_actor == expected_actor


def test_create_actor_all_correct_values():
    # Given data
    path = "1705-adonis-sowinski.md"
    body = "## Postać \n" \
           "### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
           "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
           "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
           "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
           "bardziej ryzykownej akcji.\n# Opis\nAdonis - a beautiful and carefree bishounen. " \
           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."
    front_matter = "---\nlayout: inwazja-karta-postaci\ntitle:  \"Adonis Sowiński\"\ncategories: profile\n" \
                   "factions: \"Srebrna Świeca\"\ntype: NPC\nowner: \"Żółw\"\n---\n"
    history = "\n\n# Historia"

    actor_text=front_matter + body + history

    # Expecting results

    expected_description="Adonis - a beautiful and carefree bishounen. " \
           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."

    expected_mechanics="### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
           "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
           "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
           "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
           "bardziej ryzykownej akcji."

    expected_actor = Actor(uid="1705-adonis-sowinski", mechanics_version="1705", name="Adonis Sowiński",
                           factions=["Srebrna Świeca"], actor_type="NPC", mechanics=expected_mechanics,
                           description=expected_description, body=body.strip(), owner="Żółw")

    # When
    actual_actor = create_actor(path=path,actor_text=actor_text)

    # Then
    assert actual_actor == expected_actor


def test_create_actors():
    # Given data
    path = "1705-adonis-sowinski.md"
    body = "## Postać \n" \
           "### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
           "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
           "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
           "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
           "bardziej ryzykownej akcji.\n# Opis\nAdonis - a beautiful and carefree bishounen. " \
           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."
    front_matter = "---\nlayout: inwazja-karta-postaci\ntitle:  \"Adonis Sowiński\"\ncategories: profile\n" \
                   "factions: \"Srebrna Świeca\"\ntype: NPC\n---\n"
    history = "\n\n# Historia"

    actor_text = front_matter + body + history

    ActorWithPath = namedtuple("ActorWithPath", "entity_text path")

    input_data = [ActorWithPath(path=path, entity_text=actor_text)]

    # Expecting results

    expected_description = "Adonis - a beautiful and carefree bishounen. " \
                           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
                           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."

    expected_mechanics = "### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
                         "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
                         "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
                         "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
                         "bardziej ryzykownej akcji."

    expected_actor = Actor(uid="1705-adonis-sowinski", mechanics_version="1705", name="Adonis Sowiński",
                           factions=["Srebrna Świeca"], actor_type="NPC", mechanics=expected_mechanics,
                           description=expected_description, body=body.strip(), owner=config.default_owner())

    # When
    actual_actor = create_actors(input_data)[0]

    # Then
    assert actual_actor == expected_actor


def test_create_actors_with_history_section():

    # Given data
    path = "1705-adonis-sowinski.md"
    body = "## Postać \n" \
           "### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
           "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
           "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
           "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
           "bardziej ryzykownej akcji.\n# Opis\nAdonis - a beautiful and carefree bishounen. " \
           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."
    front_matter = "---\nlayout: inwazja-karta-postaci\ntitle:  \"Adonis Sowiński\"\ncategories: profile\n" \
                   "factions: \"Srebrna Świeca\"\ntype: NPC\n---\n"
    history = "\n\n# Historia"

    actor_text = front_matter + body + history

    ActorWithPath = namedtuple("ActorWithPath", "entity_text path")

    input_data = [ActorWithPath(path=path, entity_text=actor_text)]

    # Expecting results

    expected_description = "Adonis - a beautiful and carefree bishounen. " \
                           "Or rather, what he would like others to think about him. He is extremely beautiful to the point is often " \
                           "considered to be one of Diakon bloodline, however behind the beautiful visage lies a competitive daredevil."

    expected_mechanics = "### Motywacje (do czego dąży)\n* **daredevil**: \"jeśli nic nie czujesz... to nie żyjesz. " \
                         "Zawsze wybieraj najciekawsze rozwiązanie.\"\n### Zachowania (jaka jest)\n* **otwarty**: \"" \
                         "wszystkiego należy spróbować. Nie mnie oceniać. Bądź czym chcesz i rób co chcesz.\"\n### Specjalizacje\n" \
                         "* **skłanianie do ryzyka**: mało kto tak jak Adonis potrafi namówić innych do wybrania " \
                         "bardziej ryzykownej akcji."

    expected_actor = Actor(uid="1705-adonis-sowinski", mechanics_version="1705", name="Adonis Sowiński",
                           factions=["Srebrna Świeca"], actor_type="NPC", mechanics=expected_mechanics,
                           description=expected_description, body=body.strip(), owner=config.default_owner())

    # When
    actual_actor = create_actors(input_data)[0]

    # Then
    assert actual_actor == expected_actor


def test_create_actor_multiple_factions():

    # Given data
    path = "1705-adonis-sowinski.md"
    body = "This is some random \n multiline text"
    history = "\n\n# Historia"
    front_matter = """---
layout: inwazja-karta-postaci 
categories: profile
factions: \"Srebrna Świeca\",\"Dare Shiver\", \"Diakoni\"
---"""
    mech_ver = path[0:4]

    # Expecting results
    expected_actor = Actor(uid=path[:-3], mechanics_version=mech_ver, name=config.default_actor_name(),
                           factions=["Srebrna Świeca", "Dare Shiver", "Diakoni"], actor_type=config.default_actor_type(), mechanics="",
                           description="", body=body, owner=config.default_owner())

    # When
    actual_actor = create_actor(path=path, actor_text=front_matter + body + history)

    # Then
    assert actual_actor == expected_actor
