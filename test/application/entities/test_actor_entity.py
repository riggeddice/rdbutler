from src.application.entities.actors.actor import Actor


def create_test_actor(uid="uid", mechanics_version="version", name="name", factions=None, actor_type="actor type",
                      mechanics="mechanic", description="description", body="body",
                      owner="owner"):
    if factions is None:
        factions = ["faction 1", "faction 2"]
    return Actor(uid, mechanics_version, name, factions, actor_type, mechanics, description, body, owner)


def test_actor_equality_operator_same_data():
    a1 = create_test_actor()
    a2 = create_test_actor()
    assert a1 == a2


def test_actor_equality_operator_same_data_with_superfluous_fields():
    a1 = create_test_actor()
    a2 = create_test_actor()
    a2.additional_field = "really? why would anyone do this?"
    assert a1 == a2


def test_actor_equality_operator_different_body():
    a1 = create_test_actor()
    a2 = create_test_actor(body="different body")
    assert a1 != a2


def test_actor_equality_operator_different_name():
    a1 = create_test_actor()
    a2 = create_test_actor(name="different name")
    assert a1 != a2


def test_actor_equality_operator_different_uid():
    a1 = create_test_actor()
    a2 = create_test_actor(uid="different uid")
    assert a1 != a2


def test_actor_equality_operator_different_factions():
    a1 = create_test_actor()
    a2 = create_test_actor(factions=["faction 3", "faction 4"])
    assert a1 != a2


def test_actor_equality_operator_different_description():
    a1 = create_test_actor()
    a2 = create_test_actor(description="different description")
    assert a1 != a2


def test_actor_equality_operator_different_actor_type():
    a1 = create_test_actor()
    a2 = create_test_actor(actor_type="different actor_type")
    assert a1 != a2


def test_actor_equality_operator_different_mechanics_version():
    a1 = create_test_actor()
    a2 = create_test_actor(mechanics_version="different mechanics_version")
    assert a1 != a2


def test_actor_equality_operator_different_mechanics():
    a1 = create_test_actor()
    a2 = create_test_actor(mechanics="different rpg_mechanics")
    assert a1 != a2


def test_actor_equality_operator_different_owner():
    a1 = create_test_actor()
    a2 = create_test_actor(owner="different owner")
    assert a1 != a2


def test_actor_equality_operator_different_object_type():
    a1 = create_test_actor()
    a2 = "what were you expecting from this comparison?"
    assert a1 != a2
