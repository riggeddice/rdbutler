from src.application.entities.campaigns.campaign import Campaign


def test_campaign_equality_operator_same_data():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    assert c1 == c2


def test_campaign_equality_operator_different_uid():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 2", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    assert c1 != c2


def test_campaign_equality_operator_different_title():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 1", title="title 2", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    assert c1 != c2


def test_campaign_equality_operator_different_prev_campaign_uid():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 2", body="body 1", description="description 1")
    assert c1 != c2


def test_campaign_equality_operator_different_body():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 2", description="description 1")
    assert c1 != c2


def test_campaign_equality_operator_different_description():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 2")
    assert c1 != c2


def test_campaign_equality_operator_different_populated_data():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c1.populated_data["key 1"] = "value 1"
    c2 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2.populated_data["key 1"] = "value 2"
    c2.populated_data["key 2"] = "value 3"
    assert c1 == c2


def test_campaign_equality_operator_different_object_type():
    c1 = Campaign(uid="uid 1", title="title 1", prev_campaign_uid="uid 1", body="body 1", description="description 1")
    c2 = "what were you expecting from this comparison?"
    assert c1 != c2
