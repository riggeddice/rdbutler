from hypothesis import given, strategies

from src.application.entities.merits.create import create_merit
from src.application.entities.merits.create import create_merits
from src.application.entities.merits.merit import Merit
from src.application.entities.merits.extract import extract_merit_section_body
from src.application.entities.merits.extract import extract_merit_record_tuples
from src.application.entities.stories.create import create_story_with_default_params


# Merit creation

# Hypothesis: this checks if nothing is able to damage the merit parser, as hypothesis WILL try to generate
# very, very horrible text data for all function parameters
@given(strategies.text(), strategies.text(), strategies.text(), strategies.text())
def test_no_report_can_damage_story_with_merit_with_exception(story_uid, actor_name, actor_qualifier, merit_text):

    # Given data
    merit_tuple = (actor_qualifier, actor_name, merit_text)

    # Expecting results
    expected_merit = Merit(originating_story=story_uid, actor=actor_name,
                           qualifier=actor_qualifier, actual_merit=merit_text)

    # When
    actual_merit = create_merit(story_uid, merit_tuple)

    # Then
    assert actual_merit == expected_merit


def test_create_merit__proper_data__from_story_only():

    # Given data

    text_before_merit_section = "This is some text before Merit Section\n"
    merit_section_header = "# Zasługi\n"
    merit_section_record_1 = "* mag: Siluria Diakon, jako czarodziejka pokonująca jaszczura\n"
    merit_section_record_2 = "* mag: Aurel Czarko, jako prawie śniadanie jaszczura\n"
    header_different_from_merit_section_with_text = "# Podsumowanie\n\nThis is some text after Merit Section"

    story_uid = "171231-lizard-in-the-darkness"
    story_name = "Lizard in the Darkness"

    story_body = text_before_merit_section + merit_section_header + merit_section_record_1 + merit_section_record_2 +\
        header_different_from_merit_section_with_text

    story = create_story_with_default_params(uid=story_uid, title=story_name, body=story_body)

    # Expecting results
    expected_merits = [create_merit(story_uid, extract_merit_record_tuples(merit_section_record_1)[0]),
                       create_merit(story_uid, extract_merit_record_tuples(merit_section_record_2)[0])]

    # When
    actual_merits = create_merits(story)

    # Then
    for i in range(0, 2):
        assert actual_merits[i] == expected_merits[i]


def test_create_merit__proper_data__from_story_uid_and_tuple():

    # Given data
    story_uid = "171231-lizard-in-the-darkness"
    actor_name = "Siluria Diakon"
    actor_qualifier = "mag"
    merit_text = "has defeated a lizard while being in the darkness"
    merit_tuple = (actor_qualifier, actor_name, merit_text)

    # Expecting results
    expected_merit = Merit(originating_story=story_uid, actor=actor_name,
                           qualifier=actor_qualifier, actual_merit=merit_text)

    # When
    actual_merit = create_merit(story_uid, merit_tuple)

    # Then
    assert actual_merit == expected_merit


# Single Merits

def test_parse_merit__proper_data__is_garbage_or_empty():

    # Given data
    merit_section_body_garbage = "\n\n\na;lsdajajsalkfnalfa\n\n\n"
    merit_section_body_empty = ""

    # Expecting results
    expected_merit_tuple_list = []

    # When
    actual_merit_tuple_list_1 = extract_merit_record_tuples(merit_section_body_garbage)
    actual_merit_tuple_list_2 = extract_merit_record_tuples(merit_section_body_empty)

    # Then

    assert (actual_merit_tuple_list_1 == expected_merit_tuple_list)
    assert (actual_merit_tuple_list_2 == expected_merit_tuple_list)


def test_parse_merit__proper_data__has_mixed_record_structure():

    # Expecting results
    expected_merit_tuple_1 = ("vic", "'GS Aegis' 0003", "który wygrywa internet")
    expected_merit_tuple_2 = ("???", "P", "zwycięzca")
    expected_merit_tuple_3 = ("czł", "Maciek Rozrywka", "bawiący się rozrywkowo")
    expected_merit_tuple_list = [expected_merit_tuple_1, expected_merit_tuple_2, expected_merit_tuple_3]

    # Given data
    garbage_data = "\n\n\na;lsdajajsalkfnalfa\n\n\n"

    record_format_old = "- %s: %s jako %s"
    record_format_new = "* %s: %s, %s"

    merit_record_1 = record_format_old % expected_merit_tuple_1
    merit_record_2 = record_format_new % expected_merit_tuple_2
    merit_record_3 = record_format_new % expected_merit_tuple_3

    merit_section_body = garbage_data + merit_record_1 + "\n" + merit_record_2 + "\n" + merit_record_3 + garbage_data

    # When
    actual_merit_tuple_list = extract_merit_record_tuples(merit_section_body)

    # Then

    for i in range(0, 3):
        assert (expected_merit_tuple_list[i] == actual_merit_tuple_list[i])


# Merit Section

def test_parse_merit_section__proper_data__has_next_header_after_merit_section():

    # Given data
    text_before_merit_section = "This is some text before Merit Section\n"
    merit_section_header = "# Zasługi"
    merit_section_body = "\n\nMerits Record 1\n\nMerits Record 2\n"
    header_different_from_merit_section_with_text = "# Podsumowanie\n\nThis is some text after Merit Section"

    input_data = text_before_merit_section + merit_section_header + merit_section_body +\
        header_different_from_merit_section_with_text

    # When
    actual_merit_section_body = extract_merit_section_body(input_data)

    # Then
    assert (actual_merit_section_body == merit_section_body.strip())


def test_parse_merit_section__proper_data__merit_section_is_last_header():

    # Given data
    text_before_merit_section = "This is some text before Merit Section\n"
    merit_section_header = "# Zasługi"
    merit_section_body = "\n\nMerit Section Record 1\n\nMerit Section Record 2\n"

    input_data = text_before_merit_section + merit_section_header + merit_section_body

    # When
    actual_merit_section_body = extract_merit_section_body(input_data)

    # Then
    assert (actual_merit_section_body == merit_section_body.strip())


def test_parse_merit_section__improper_data__empty_body():

    # Given data
    text_before_merit_section = "This is some text before Merit Section\n"
    merit_section_header = "# Zasługi:"
    header_different_from_merit_section_with_text = "# Podsumowanie\n\nThis is some text after Merit Section"

    input_data = text_before_merit_section + merit_section_header +\
        header_different_from_merit_section_with_text

    # When
    actual_merit_section_body = extract_merit_section_body(input_data)

    # Then
    assert (actual_merit_section_body == "")


def test_parse_merit_section__improper_data__no_personae_section():

    # Given data
    text_before_merit_section = "This is some text before Merit Section\n"
    header_different_from_merit_section_with_text = "# Podsumowanie\n\nThis is some text after Merit Section"

    input_data = text_before_merit_section + header_different_from_merit_section_with_text

    # When
    actual_merit_section_body = extract_merit_section_body(input_data)

    # Then
    assert (actual_merit_section_body == "")
