from hypothesis import given, strategies, assume
from collections import namedtuple

from src.application.entities.stories.time_record import TimeRecord
from src.application.entities.stories.story import Story
from src.application.entities.stories.create import create_stories
from src.application.entities import config


@given(strategies.text())
def test_no_matter_the_story_data_it_will_work_somehow(horrible_text):

    assume(horrible_text)

    # Given data
    path = "171231-lizard-in-the-darkness.md"

    StoryWithPath = namedtuple("StoryWithPath", "entity_text path")
    input_data = [StoryWithPath(entity_text=horrible_text, path=path)]

    # Expecting results
    expected_story = Story(uid=path[:-3], title=config.default_story_title_in_story(),
                           campaign=config.default_campaign_title(),
                           prev_story_ids_chrono=["171231-lizard-in-the-darkness"],
                           prev_story_id_campgn=path[:-3], body=horrible_text.strip(),
                           players=config.default_players_in_story(), gms=config.default_gms_in_story(),
                           summary=config.default_summary_in_story(), location=config.default_location_in_story(),
                           time_record=config.default_story_time_record())

    # When
    actual_story = create_stories(input_data)[0]

    # Then
    assert actual_story == expected_story


def test_create_stories__malformed_data__single_story_no_title():

    # Given data
    path = "171231-lizard-in-the-darkness.md"
    campaign = "lizard_strikes_back"
    prev_chrono_id = "151110-previous-id"
    prev_camp_id = "151111-previous-id2"
    body = "\n## Kontynuacja\n" + "### Kampanijna\n\n* [prev camp story name](" + prev_camp_id + ".html)\n\n" + \
                  "### Chronologiczna\n\n* [prev chrono story name](" + prev_chrono_id + ".html)\n### Inne\n"
    story_text = "---\nlayout: inwazja-konspekt\n" + "\ncampaign: " + campaign + \
                 "\ncategories: inwazja, konspekt\n---" + body

    StoryWithPath = namedtuple("StoryWithPath", "entity_text path")
    input_data = [StoryWithPath(entity_text=story_text, path=path)]

    # Expecting results
    expected_story = Story(uid=path[:-3], title=config.default_story_title_in_story(),
                           prev_story_ids_chrono=[prev_chrono_id], prev_story_id_campgn=prev_camp_id, campaign=campaign,
                           body=body.strip(), players=config.default_players_in_story(),
                           gms=config.default_gms_in_story(), summary=config.default_summary_in_story(),
                           location=config.default_location_in_story(), time_record=config.default_story_time_record())

    # When
    actual_story = create_stories(input_data)[0]

    # Then
    assert actual_story == expected_story


def test_create_stories__malformed_data__single_story_no_campaign():

    # Given data
    path = "171231-lizard-in-the-darkness.md"
    title = "Lizard in the Darkness"
    prev_chrono_id = "151110-previous-id"
    prev_story_id_campgn = "151111-previous-story-id-campaign"
    chrono_body = "\n## Kontynuacja\n" + "### Kampanijna\n\n* [prev camp story name](" + prev_story_id_campgn + ".html)\n\n" + \
                  "### Chronologiczna\n\n* [prev chrono story name](" + prev_chrono_id + ".html)\n### Inne\n"
    body = chrono_body + "\n# Streszczenie\\n### Inne\n"

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\n" + \
                 "\ncategories: inwazja, konspekt\n---" + body

    StoryWithPath = namedtuple("StoryWithPath", "entity_text path")
    input_data = [StoryWithPath(entity_text=story_text, path=path)]

    # Expecting results
    expected_story = Story(uid=path[:-3], title=title, campaign=config.default_campaign_title(),
                           prev_story_ids_chrono=[prev_chrono_id], prev_story_id_campgn=prev_story_id_campgn, body=body.strip(),
                           players=config.default_players_in_story(), gms=config.default_gms_in_story(),
                           summary=config.default_summary_in_story(),
                           location=config.default_location_in_story(), time_record=config.default_story_time_record())

    # When
    actual_story = create_stories(input_data)[0]

    # Then
    assert actual_story == expected_story


def test_create_stories__proper_data__single_story():

    # Given data
    path = "171231-lizard-in-the-darkness.md"
    title = "Lizard in the Darkness"
    campaign = "lizard_strikes_back"
    players = ["Kić", "Dust"]
    gms = ["Żółw"]
    prev_chrono_id = "151110-previous-id"
    prev_story_id_campgn = "151111-previous-campaign-id"
    chrono_body = "\n## Kontynuacja\n" + "### Kampanijna\n\n* [prev camp story name](" + prev_story_id_campgn + ".html)\n\n" +\
                  "### Chronologiczna\n\n* [prev chrono story name](" + prev_chrono_id + ".html)\n### Inne\n"
    time_body = "\n# Czas: \n * Opóźnienie: 12\n* Dni: 3\n"
    summary_section_header = "\n# Streszczenie\n"
    summary_section = "Summary line1 \n+Summary line2"
    summary_body = summary_section_header + summary_section

    location_section_header = "\n# Lokalizacje\n"
    location_section = "1. Świat\n    1.Śląsk"
    location_body = location_section_header + location_section

    story_text = "---\nlayout: inwazja-konspekt\ntitle: \"" + title + "\"\ncampaign: " + campaign +\
                 "\ncategories: inwazja, konspekt\nplayers: %s, %s\ngm: %s\n---" % (players[0], players[1], gms[0]) +\
                 chrono_body + summary_body + location_body + time_body

    StoryWithPath = namedtuple("StoryWithPath", "entity_text path")
    input_data = [StoryWithPath(entity_text=story_text, path=path)]

    # Expecting results
    stripped_body = (chrono_body + summary_body + location_body + time_body).strip()
    expected_story = Story(uid=path[:-3],
                           title=title,
                           campaign=campaign,
                           prev_story_ids_chrono=[prev_chrono_id],
                           prev_story_id_campgn=prev_story_id_campgn,
                           body=stripped_body,
                           players=players,
                           gms=gms,
                           summary=summary_section.strip(),
                           location=location_section.strip(),
                           time_record=TimeRecord(duration=3, delay=12, is_default=False))

    # When
    actual_story = create_stories(input_data)[0]

    # Then
    assert actual_story == expected_story
