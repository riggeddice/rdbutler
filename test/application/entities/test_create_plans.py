from textwrap import dedent

from src.application.entities.plans.create import create_plans_from_story, create_plan_with_default_params
from src.application.entities.stories.create import create_story_with_default_params

story_body_no_plan = dedent("""\
    ## Kontynuacja
    ### Kampanijna
    
    * [prev camp story name](151111-previous-story-id-campaign.html)
    
    ### Chronologiczna
    
    * [prev chrono story name](151110-previous-id.html)
    ### Inne
    
    # Streszczenie\\n### Inne
    """)

story_body = dedent("""\
    ## Kontynuacja
    ### Kampanijna
    
    * [prev camp story name](151111-previous-story-id-campaign.html)
    
    ### Chronologiczna
    
    * [prev chrono story name](151110-previous-id.html)
    ### Inne
    
    # Streszczenie\\n### Inne
    
    # Plany
    * Maja Kola: już nie chce
    
    ## Frakcji
    * Misie: idą tu
    
    """)

story_no_plan = create_story_with_default_params(body=story_body_no_plan)
story = create_story_with_default_params(body=story_body)
plan1 = create_plan_with_default_params(story_uid="101214-default", actual_plan="już nie chce",
                                        principal="Maja Kola")
plan2 = create_plan_with_default_params(story_uid="101214-default", actual_plan="idą tu",
                                        principal="Misie", is_faction=True)


def test_create_progression__proper_data__from_story():
    plans_expected = [plan1, plan2]
    plans_actual = create_plans_from_story(story)

    assert len(plans_actual) == len(plans_expected)
    for i in range(0, len(plans_expected)):
        assert plans_actual[i] == plans_expected[i]


def test_create_progression__no_data__from_story():
    plans_actual = create_plans_from_story(story_no_plan)

    assert not plans_actual
    