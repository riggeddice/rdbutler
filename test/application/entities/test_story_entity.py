from src.application.entities.stories.story import Story
from src.application.entities.stories.time_record import TimeRecord


def create_test_story(uid="uid", title="title", campaign="campaign", previous_story_ids_chrono=("previous story id 1",),
                      previous_story_id_campgn="previous story id campaign", gms=("gm1",), players=("p1", "p2"),
                      body="body", summary="summary", location="location",
                      time_record=TimeRecord(duration=1, delay=2, is_default=False)):
    return Story(uid, title, campaign, previous_story_ids_chrono, previous_story_id_campgn, gms, players, body,
                 summary, location, time_record)


def test_story_equality_operator_same_data():
    s1 = create_test_story()
    s2 = create_test_story()
    assert s1 == s2


def test_story_equality_operator_different_unique_id():
    s1 = create_test_story()
    s2 = create_test_story(uid="different uid")
    assert s1 != s2


def test_story_equality_operator_different_title():
    s1 = create_test_story()
    s2 = create_test_story(title="different title")
    assert s1 != s2


def test_story_equality_operator_different_campaign():
    s1 = create_test_story()
    s2 = create_test_story(campaign="different campaign")
    assert s1 != s2


def test_story_equality_operator_different_body():
    s1 = create_test_story()
    s2 = create_test_story(body="different body")
    assert s1 != s2


def test_story_equality_operator_different_previous_story_ids_chrono():
    s1 = create_test_story()
    s2 = create_test_story(previous_story_ids_chrono=("different id",))
    assert s1 != s2


def test_story_equality_operator_different_previous_story_id_campaign():
    s1 = create_test_story()
    s2 = create_test_story(previous_story_id_campgn="different id")
    assert s1 != s2


def test_story_equality_operator_different_gms():
    s1 = create_test_story()
    s2 = create_test_story(gms=("different gm",))
    assert s1 != s2


def test_story_equality_operator_different_players():
    s1 = create_test_story()
    s2 = create_test_story(players=("player 3", "player 4"))
    assert s1 != s2


def test_story_equality_operator_different_summary():
    s1 = create_test_story()
    s2 = create_test_story(summary="different summary")
    assert s1 != s2


def test_story_equality_operator_different_location():
    s1 = create_test_story()
    s2 = create_test_story(location="different location")
    assert s1 != s2


def test_story_equality_operator_different_time_record():
    s1 = create_test_story()
    s2 = create_test_story(time_record=TimeRecord(duration=3, delay=4, is_default=True))
    assert s1 != s2


def test_story_equality_operator_different_populated_data():
    s1 = create_test_story()
    s1.populated_data["key 1"] = "value 1"
    s2 = create_test_story()
    s2.populated_data["key 1"] = "value 2"
    s2.populated_data["key 2"] = "value 3"
    assert s1 == s2


def test_story_equality_operator_different_object_type():
    s1 = create_test_story()
    s2 = "what were you expecting from this comparison?"
    assert s1 != s2
