from src.application.entities.progressions.progression import Progression


def create_test_progression(originating_story="1234-story-uid", principal="Jan Kowalski",
                            actual_progression="got ABC",is_faction=False)-> Progression:
    return Progression(originating_story=originating_story, principal=principal,
                       actual_progression=actual_progression, is_faction=is_faction)


def test_progression_equality_operator_same_data():
    s1 = create_test_progression()
    s2 = create_test_progression()
    assert s1 == s2


def test_progression_equality_operator_different_orig_story():
    s1 = create_test_progression()
    s2 = create_test_progression(originating_story="4321-another-story")
    assert s1 != s2


def test_progression_equality_operator_different_principal():
    s1 = create_test_progression()
    s2 = create_test_progression(principal="different principal")
    assert s1 != s2


def test_progression_equality_operator_different_progression():
    s1 = create_test_progression()
    s2 = create_test_progression(actual_progression="something else")
    assert s1 != s2


def test_progression_equality_operator_different_faction():
    s1 = create_test_progression()
    s2 = create_test_progression(is_faction=True)
    assert s1 != s2


def test_progression_equality_operator_different_object_type():
    s1 = create_test_progression()
    s2 = "what were you expecting from this comparison?"
    assert s1 != s2
