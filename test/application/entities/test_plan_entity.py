from src.application.entities.plans.plan import Plan


def create_test_plan(originating_story="1234-story-uid", principal="Jan Kowalski",
                     actual_plan="got ABC", is_faction=False)-> Plan:
    return Plan(originating_story=originating_story, principal=principal,
                actual_plan=actual_plan, is_faction=is_faction)


def test_plan_equality_operator_same_data():
    s1 = create_test_plan()
    s2 = create_test_plan()
    assert s1 == s2


def test_plan_equality_operator_different_orig_story():
    s1 = create_test_plan()
    s2 = create_test_plan(originating_story="4321-another-story")
    assert s1 != s2


def test_plan_equality_operator_different_principal():
    s1 = create_test_plan()
    s2 = create_test_plan(principal="different principal")
    assert s1 != s2


def test_plan_equality_operator_different_progression():
    s1 = create_test_plan()
    s2 = create_test_plan(actual_plan="something else")
    assert s1 != s2


def test_plan_equality_operator_different_faction():
    s1 = create_test_plan()
    s2 = create_test_plan(is_faction=True)
    assert s1 != s2


def test_plan_equality_operator_different_object_type():
    s1 = create_test_plan()
    s2 = "what were you expecting from this comparison?"
    assert s1 != s2
