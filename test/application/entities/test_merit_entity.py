from src.application.entities.merits.merit import Merit


def test_merit_equality_operator_same_data():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    assert m1 == m2


def test_merit_equality_operator_same_data_with_superfluous_fields():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2.additional_field = "really? why would anyone do this?"
    assert m1 == m2


def test_merit_equality_operator_different_originating_story():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 2", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    assert m1 != m2


def test_merit_equality_operator_different_actor():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 1", actor="actor 2", qualifier="qualifier 1", actual_merit="merit 1")
    assert m1 != m2


def test_merit_equality_operator_different_qualifier():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 2", actual_merit="merit 1")
    assert m1 != m2
    

def test_merit_equality_operator_different_actual_merit():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 2")
    assert m1 != m2


def test_merit_equality_operator_different_object_type():
    m1 = Merit(originating_story="story 1", actor="actor 1", qualifier="qualifier 1", actual_merit="merit 1")
    m2 = "what were you expecting from this comparison?"
    assert m1 != m2