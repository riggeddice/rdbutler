from src.application.entities.consequences.consequence import Consequence


def test_consequence_equality_operator_same_data():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    assert c1 == c2


def test_consequence_equality_operator_same_data_with_superfluous_fields():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2.additional_field = "really? why would anyone do this?"
    assert c1 == c2


def test_consequence_equality_operator_different_originating_story():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = Consequence(originating_story="story 2", actor="actor 1", actual_consequence="consequence 1")
    assert c1 != c2


def test_consequence_equality_operator_different_actor():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = Consequence(originating_story="story 1", actor="actor 2", actual_consequence="consequence 1")
    assert c1 != c2


def test_consequence_equality_operator_different_actual_consequence():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 2")
    assert c1 != c2


def test_consequence_equality_operator_different_object_type():
    c1 = Consequence(originating_story="story 1", actor="actor 1", actual_consequence="consequence 1")
    c2 = "what were you expecting from this comparison?"
    assert c1 != c2
