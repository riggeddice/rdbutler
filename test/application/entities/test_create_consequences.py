from hypothesis import given, strategies

from src.application.entities.consequences.create import create_consequence
from src.application.entities.consequences.create import create_consequences
from src.application.entities.consequences.consequence import Consequence
from src.application.entities.consequences.extract import extract_consequence_section_body
from src.application.entities.consequences.extract import extract_consequence_record_tuples
from src.application.entities.stories.create import create_story_with_default_params


# consequence creation

# Hypothesis: this checks if nothing is able to damage the consequence parser, as hypothesis WILL try to generate
# very, very horrible text data for all function parameters
@given(strategies.text(), strategies.text(), strategies.text())
def test_no_report_can_damage_story_with_consequence_with_exception(story_uid, actor_name, consequence_text):

    # Given data
    consequence_tuple = (actor_name, consequence_text)

    # Expecting results
    expected_consequence = Consequence(originating_story=story_uid, actor=actor_name, actual_consequence=consequence_text)

    # When
    actual_consequence = create_consequence(story_uid, consequence_tuple)

    # Then
    assert actual_consequence == expected_consequence


def test_create_consequence__proper_data__from_story_only():

    # Given data

    text_before_consequence_section = "This is some text before Consequence Section\n"
    consequence_section_header = "# Progresja\n"
    consequence_section_record_1 = "* Siluria Diakon : uzyskała duży wpływ na Wiktora (+2 do testów)\n"
    consequence_section_record_2 = "* Aurel Czarko:wpadł w niełaskę\n"
    header_different_from_consequence_section_with_text = "# Podsumowanie\n\nThis is some text after Consequence Section"

    story_uid = "171231-lizard-in-the-darkness"
    story_name = "Lizard in the Darkness"

    story_body = text_before_consequence_section + consequence_section_header + consequence_section_record_1 + consequence_section_record_2 +\
        header_different_from_consequence_section_with_text

    story = create_story_with_default_params(uid=story_uid, title=story_name, body=story_body)

    # Expecting results
    consequence_actor_1 = extract_consequence_record_tuples(consequence_section_record_1)[0][0]
    consequence_text_1 = extract_consequence_record_tuples(consequence_section_record_1)[0][1]
    consequence_actor_2 = extract_consequence_record_tuples(consequence_section_record_2)[0][0]
    consequence_text_2 = extract_consequence_record_tuples(consequence_section_record_2)[0][1]
    consequence_tuple_1=(consequence_actor_1,consequence_text_1)
    consequence_tuple_2=(consequence_actor_2,consequence_text_2)
    expected_consequences = [create_consequence(story_uid,consequence_tuple_1),
                             create_consequence(story_uid,consequence_tuple_2)]

    # When
    actual_consequences = create_consequences(story)

    # Then
    for i in range(0, 2):
        assert actual_consequences[i] == expected_consequences[i]


def test_create_consequence__proper_data__from_story_uid_and_tuple():

    # Given data
    story_uid = "171231-lizard-in-the-darkness"
    actor_name = "Siluria Diakon"
    consequence_text = "uzyskała duży wpływ na Wiktora (+2 do testów)"
    consequence_tuple = (actor_name, consequence_text)

    # Expecting results
    expected_consequence = Consequence(originating_story=story_uid, actor=actor_name,
                           actual_consequence=consequence_text)

    # When
    actual_consequence = create_consequence(story_uid, consequence_tuple)

    # Then
    assert actual_consequence == expected_consequence


# Single consequences

def test_parse_consequence__proper_data__is_garbage_or_empty():

    # Given data
    consequence_section_body_garbage = "\n\n\na;lsdajajsalkfnalfa\n\n\n"
    consequence_section_body_empty = ""

    # Expecting results
    expected_consequence_tuple_list = []

    # When
    actual_consequence_tuple_list_1 = extract_consequence_record_tuples(consequence_section_body_garbage)
    actual_consequence_tuple_list_2 = extract_consequence_record_tuples(consequence_section_body_empty)

    # Then

    assert (actual_consequence_tuple_list_1 == expected_consequence_tuple_list)
    assert (actual_consequence_tuple_list_2 == expected_consequence_tuple_list)


# consequence Section

def test_parse_consequence_section__proper_data__has_next_header_after_consequence_section():

    # Given data
    text_before_consequence_section = "This is some text before consequence Section\n"
    consequence_section_header = "# Progresja"
    consequence_section_body = "\n\nActor1: Record 1\n\nActor2: Record 2\n"
    header_different_from_consequence_section_with_text = "# Podsumowanie\n\nThis is some text after consequence Section"

    input_data = text_before_consequence_section + consequence_section_header + consequence_section_body +\
        header_different_from_consequence_section_with_text

    # When
    actual_consequence_section_body = extract_consequence_section_body(input_data)

    # Then
    assert (actual_consequence_section_body == consequence_section_body.strip())


def test_parse_consequence_section__proper_data__consequence_section_is_last_header():

    # Given data
    text_before_consequence_section = "This is some text before Consequence Section\n"
    consequence_section_header = "# Progresja"
    consequence_section_body = "\n\nConsequence Section Record 1\n\nConsequence Section Record 2\n"

    input_data = text_before_consequence_section + consequence_section_header + consequence_section_body

    # When
    actual_consequence_section_body = extract_consequence_section_body(input_data)

    # Then
    assert (actual_consequence_section_body == consequence_section_body.strip())


def test_parse_consequence_section__improper_data__empty_body():

    # Given data
    text_before_consequence_section = "This is some text before Consequence Section\n"
    consequence_section_header = "# Progresja:"
    header_different_from_consequence_section_with_text = "\n\n# Podsumowanie\n\nThis is some text after Consequence Section"

    input_data = text_before_consequence_section + consequence_section_header +\
        header_different_from_consequence_section_with_text

    # When
    actual_consequence_section_body = extract_consequence_section_body(input_data)

    # Then
    assert (actual_consequence_section_body == "")


def test_parse_consequence_section__improper_data__no_personae_section():

    # Given data
    text_before_consequence_section = "This is some text before Consequence Section\n"
    header_different_from_consequence_section_with_text = "# Podsumowanie\n\nThis is some text after Consequence Section"

    input_data = text_before_consequence_section + header_different_from_consequence_section_with_text

    # When
    actual_consequence_section_body = extract_consequence_section_body(input_data)

    # Then
    assert (actual_consequence_section_body == "")
