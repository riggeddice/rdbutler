from textwrap import dedent

from src.application.entities.progressions.create import create_progressions_from_story, \
    create_progression_with_default_params
from src.application.entities.stories.create import create_story_with_default_params

story_body_no_progression = dedent("""\
    ## Kontynuacja
    ### Kampanijna
    
    * [prev camp story name](151111-previous-story-id-campaign.html)
    
    ### Chronologiczna
    
    * [prev chrono story name](151110-previous-id.html)
    ### Inne
    
    # Streszczenie\\n### Inne
    """)

story_body = dedent("""\
    ## Kontynuacja
    ### Kampanijna
    
    * [prev camp story name](151111-previous-story-id-campaign.html)
    
    ### Chronologiczna
    
    * [prev chrono story name](151110-previous-id.html)
    ### Inne
    
    # Streszczenie\\n### Inne
    
    # Progresja
    * Maja Kola: ma coś nowego
    
    ## Frakcji
    * Misie: idą tu
    
    """)

story_no_progression = create_story_with_default_params(body=story_body_no_progression)
story = create_story_with_default_params(body=story_body)
progression1 = create_progression_with_default_params(story_uid="101214-default", actual_progression="ma coś nowego",
                                                      principal="Maja Kola")
progression2 = create_progression_with_default_params(story_uid="101214-default", actual_progression="idą tu",
                                                      principal="Misie", is_faction=True)


def test_create_progression__proper_data__from_story():
    progressions_expected = [progression1, progression2]
    progressions_actual = create_progressions_from_story(story)

    assert len(progressions_actual) == len(progressions_expected)
    for i in range(0, len(progressions_expected)):
        assert progressions_actual[i] == progressions_expected[i]


def test_create_progression__no_data__from_story():
    progressions_actual = create_progressions_from_story(story_no_progression)

    assert not progressions_actual
