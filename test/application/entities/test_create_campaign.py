from collections import namedtuple

from hypothesis import given, strategies, assume

from src.application.entities import config
from src.application.entities.campaigns.campaign import Campaign
from src.application.entities.campaigns.create import create_campaigns, create_campaign_with_default_params
from src.application.usecases.generate.helpers.campaign_sheets_helper import campaign_front_matter


@given(strategies.text())
def test_no_matter_the_campaign_data_it_will_work_somehow(horrible_body):

    assume(horrible_body)

    # Given data
    path = "kampania-test-campaign.md"
    uid = path[:-3].replace("kampania-", "")
    title = "Any title"
    front_matter = campaign_front_matter % title

    full_text = front_matter + horrible_body

    EntityWithPath = namedtuple("EntityWithPath", "entity_text path")
    input_data = [EntityWithPath(entity_text=full_text, path=path)]

    # Expecting results
    expected_campaign = Campaign(uid=uid,
                                 title=title,
                                 prev_campaign_uid=uid,
                                 body=horrible_body.strip(),
                                 description=config.default_campaign_description())

    # When
    actual_campaign = create_campaigns(input_data)[0]

    # Then
    assert actual_campaign == expected_campaign


def test_create_campaigns__default_data__single_campaign():

    # Given data

    # Expecting results
    expected_campaign = Campaign(uid=config.default_campaign_uid().replace("kampania-", ""),
                                 title=config.default_campaign_title(),
                                 prev_campaign_uid=config.default_campaign_uid().replace("kampania-", ""),
                                 body=config.default_body(),
                                 description=config.default_campaign_description())

    # When
    actual_campaign = create_campaign_with_default_params()

    # Then
    assert actual_campaign == expected_campaign


def test_create_campaigns__proper_data__single_campaign():

    # Given data
    path = "kampania-test-campaign.md"
    uid = path[:-3]

    title = "Test campaign"
    front_matter = campaign_front_matter % title

    prev_campaign = "kampania-previous-campaign"
    body = "# Kampania: {{ page.title }}\n\n## Kontynuacja\n\n* [Test](%s.html)\n\n## Opis\nTu jest opis\n\n" % prev_campaign
    history_header = "# Historia"
    full_text = front_matter + body + history_header

    EntityWithPath = namedtuple("EntityWithPath", "entity_text path")
    input_data = [EntityWithPath(entity_text=full_text, path=path)]

    # Expecting results
    expected_campaign = Campaign(uid=uid.replace("kampania-", ""), title=title,
                                 prev_campaign_uid=prev_campaign.replace("kampania-", ""),
                                 body=body.strip(),
                                 description="Tu jest opis")

    # When
    actual_campaign = create_campaigns(input_data)[0]

    # Then
    assert actual_campaign == expected_campaign


